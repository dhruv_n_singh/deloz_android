package io.deloz.dhruv.deloz.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import io.deloz.dhruv.deloz.R;

public class GettingStartedActivity extends AppCompatActivity {

    //Variables

    //Compoments

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getting_started);
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
    }

    @Override
    protected void onStart() {
        super.onStart();
    }



    public void btnActionGettingStartedNow(View v) {
        Intent intent = new Intent(getApplicationContext(), AccountValidationActivity.class);
        startActivity(intent);
    }




}
