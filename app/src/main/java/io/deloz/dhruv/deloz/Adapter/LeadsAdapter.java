package io.deloz.dhruv.deloz.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.deloz.dhruv.deloz.Helper.ImageCircleTransformation;
import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.Helper.swipelayout.adapters.SwipeLayout;
import io.deloz.dhruv.deloz.Interfaces.RecyclerViewClickListener;
import io.deloz.dhruv.deloz.Modal.LeadModal;
import io.deloz.dhruv.deloz.R;

/**
 * Created by Dhruv on 12/21/17.
 */

/**
 * Provide views to RecyclerView with data from leadsListData.
 */
public class LeadsAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private static final String TAG = "LeadsAdapter";
    public Context mContext;
    public List<LeadModal> leadsListData;
    public static final int ITEM = 0;
    public static final int LOADING = 1;
    public boolean isLoadingAdded = false;
    public boolean isPeople = true;
    private SwipeLayout swipeLayout;
    private ImageView imgDelete;
    private ImageView imgLeadToContact;
    private ImageView imgArchiveContact;
    private RecyclerViewClickListener mListener;

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param leadsListData ArrayList<LeadModal> containing the data to populate views to be used by RecyclerView.
     */

    public LeadsAdapter( Context mContext
            , List<LeadModal> leadsListData,RecyclerViewClickListener listener) {
        this.leadsListData = leadsListData;
        this.mContext = mContext;
        this.mListener = listener;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.row_item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.row_lead_view, parent, false);
        viewHolder = new LeadVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        switch (getItemViewType(position)) {
            case ITEM:
                LeadVH viewHolder = (LeadVH) holder;
                LeadModal modal = leadsListData.get(position);
                String username = "";
                if (isPeople){
                    if (modal.getFirstName() != "null" && modal.getFirstName() != null){
                        username = username + modal.getFirstName();
                    }
                    if (modal.getLastName() != "null" && modal.getLastName() != null){
                        username =   username + " " + modal.getLastName();
                    }
                    if (username.length() > 0){
                        viewHolder.textViewLeadsTitle.setText(username);
                    }
                }else{
                    viewHolder.textViewLeadsTitle.setText("");
                    if (modal.getCompanyName() != null && modal.getCompanyName() != "null"){
                        username = username + modal.getCompanyName();
                        viewHolder.textViewLeadsTitle.setText(username);
                    }
                }

                if (modal.getLeadWorkPhone() != null){
                    if (modal.getLeadWorkPhone().length() > 0){
                        viewHolder.textViewLeadSubtitle.setText(modal.getLeadWorkPhone());
                    }
                } else if (modal.getLeadPrimaryEmail() != null){
                    if (modal.getLeadPrimaryEmail().length() > 0){
                        viewHolder.textViewLeadSubtitle.setText(modal.getLeadPrimaryEmail());
                    }
                }else if (modal.getLeadSecondaryEmail() != null){
                    if (modal.getLeadSecondaryEmail().length() > 0){
                        viewHolder.textViewLeadSubtitle.setText(modal.getLeadSecondaryEmail());
                    }
                }
                viewHolder.textViewTime.setText(Utility.getDaysAgo(modal.getCreatedTime()));

                if (modal.getCreatedUserProfilePic() != null && modal.getCreatedUserProfilePic() != "null"){
                    if (!modal.getCreatedUserProfilePic().isEmpty()){
                        Log.d(TAG + position + " url: ",modal.getCreatedUserProfilePic());
                        Picasso
                                .with(mContext)
                                .load(modal.getCreatedUserProfilePic())
                                .transform(new ImageCircleTransformation())
                                .resize(100, 100)
                                // .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .into(viewHolder.imageViewCreatedUser);
                    }
                }else {
                    String startsWithName = "";

                    int color = Color.parseColor("#3F51B5");;
                    if (modal.getLeadColor() != null){
                        if (modal.getLeadColor().length() > 0){
                            if (modal.getLeadColor().contains("#"))
                                color = Color.parseColor(modal.getLeadColor());
                        }
                    }

                    if (!isPeople){
                        if (modal.getCompanyName() != null && modal.getCompanyName() != "null"){
                            if (modal.getCompanyName().length() > 0){
                                startsWithName = startsWithName + modal.getCompanyName().charAt(0);
                            }
                        }
                    }else{
                        if (modal.getFirstName() != null){
                            if (modal.getFirstName().length() > 0){
                                startsWithName = startsWithName + modal.getFirstName().charAt(0);
                            }
                        }
                        if (modal.getLastName() != null){
                            if (modal.getLastName().length() > 0){
                                startsWithName = startsWithName + "" + modal.getLastName().charAt(0);
                            }
                        }
                    }

                    TextDrawable drawable = TextDrawable.builder()
                            .beginConfig()
                            .textColor(Color.WHITE)
                            .bold()
                            .withBorder(2)
                            .useFont(Typeface.DEFAULT)
                            .fontSize(24) /* size in px */
                            .toUpperCase()
                            .width(100)
                            .height(100)
                            .endConfig()
                            .buildRoundRect(startsWithName, color, 50);
                    viewHolder.imageViewCreatedUser.setImageDrawable(drawable);

                }

                if (modal.getContactsPeople() != null){
                        Picasso
                                .with(mContext)
                                .load(R.drawable.ic_people)
                                .resize(15, 15)
                                // .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .into(viewHolder.imageViewLeadUser);
                }else {
                    Picasso
                            .with(mContext)
                            .load(R.drawable.ic_lead_add_user)
                            // .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .into(viewHolder.imageViewLeadUser);

                }

                if (modal.getContactsPeople() != null && modal.getContactsPeople() != "null"){
                    if (modal.getContactsPeople().length() > 0){
                        imgLeadToContact.getLayoutParams().width = 0;
                    }else{
                        imgLeadToContact.getLayoutParams().width = 90;
                    }
                }else {
                    imgLeadToContact.getLayoutParams().width = 90;
                }
                break;
            case LOADING:
                break;
        }


    }


    @Override
    public int getItemCount() {
        return leadsListData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == leadsListData.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        leadsListData.add(new LeadModal());
        notifyItemInserted(leadsListData.size() - 1);
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = leadsListData.size() - 1;
        LeadModal item = leadsListData.get(position);

        if (item != null) {
            leadsListData.remove(position);
            notifyItemRemoved(position);
        }
    }

     /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    protected class LeadVH extends RecyclerView.ViewHolder {
        private TextView
                textViewLeadsTitle,
                textViewTime,
                textViewLeadSubtitle;
        private ImageView
                imageViewCreatedUser,
                imageViewLeadUser;

        public LeadVH(View v) {
            super(v);

            textViewLeadsTitle = (TextView) v.findViewById(R.id.textViewLeadTitle);
            textViewLeadSubtitle = (TextView) v.findViewById(R.id.textViewLeadSubtitle);
            textViewTime = (TextView) v.findViewById(R.id.textViewTime);
            imageViewCreatedUser = (ImageView) v.findViewById(R.id.imageViewCreatedUser);
            imageViewLeadUser = (ImageView) v.findViewById(R.id.imageViewLeadUser);
            swipeLayout = (SwipeLayout) v.findViewById(R.id.sample1);
            swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
            View starBottView = swipeLayout.findViewById(R.id.starbott);
            swipeLayout.addDrag(SwipeLayout.DragEdge.Left, swipeLayout.findViewById(R.id.bottom_wrapper));
            swipeLayout.addDrag(SwipeLayout.DragEdge.Right, swipeLayout.findViewById(R.id.bottom_wrapper_2));
            imgDelete = swipeLayout.findViewById(R.id.delete);
            imgLeadToContact = swipeLayout.findViewById(R.id.leadToContact);
            imgArchiveContact = swipeLayout.findViewById(R.id.leadArchive);
            imgDelete.setTag(this);
            imgLeadToContact.setTag(this);
            imgArchiveContact.setTag(this);
            swipeLayout.requestLayout();
            swipeLayout.setClickToClose(true);
            swipeLayout.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(v,getAdapterPosition(),0);
                }
            });

            swipeLayout.findViewById(R.id.leadToContact).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(v,getAdapterPosition(),1);
                }
            });

            swipeLayout.findViewById(R.id.leadArchive).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(v,getAdapterPosition(),2);
                }
            });
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }
}
