package io.deloz.dhruv.deloz.Modal;

/**
 * Created by Dhruv on 1/11/18.
 */

public class Contact {

    private int status;
    private String createdAt;
    private String projectId;
    private String firstName;
    private String lastName;
    private String key;
    private String rev;
    private String socialFacebook;
    private String socialLinkedIn;
    private String color;
    private String createdBy;
    private String updatedAt;
    private String primaryPhone;
    private String address;
    private String id;
    private String primaryEmail;
    private String accountId;
    private String profileImage;
    private String thumbnailImage;
    private String organizationEmail;
    private String headQuater;
    private String organizationName;
    private String middleName;
    private String workPhone;


    //Getter


    public String getWorkPhone() {
        return workPhone;
    }

    public String getMiddleName() {
        return middleName;
    }

    public int getStatus() {
        return status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getKey() {
        return key;
    }

    public String getRev() {
        return rev;
    }

    public String getSocialFacebook() {
        return socialFacebook;
    }

    public String getSocialLinkedIn() {
        return socialLinkedIn;
    }

    public String getColor() {
        return color;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public String getAddress() {
        return address;
    }

    public String getId() {
        return id;
    }

    public String getPrimaryEmail() {
        return primaryEmail;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public String getOrganizationEmail() {
        return organizationEmail;
    }

    public String getHeadQuater() {
        return headQuater;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    //Setter

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setRev(String rev) {
        this.rev = rev;
    }

    public void setSocialFacebook(String socialFacebook) {
        this.socialFacebook = socialFacebook;
    }

    public void setSocialLinkedIn(String socialLinkedIn) {
        this.socialLinkedIn = socialLinkedIn;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPrimaryEmail(String primaryEmail) {
        this.primaryEmail = primaryEmail;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public void setOrganizationEmail(String organizationEmail) {
        this.organizationEmail = organizationEmail;
    }

    public void setHeadQuater(String headQuater) {
        this.headQuater = headQuater;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }
}
