package io.deloz.dhruv.deloz.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

import io.deloz.dhruv.deloz.Interfaces.RecycleViewSharedInboxClickListener;
import io.deloz.dhruv.deloz.Modal.SharedInboxList;
import io.deloz.dhruv.deloz.R;

/**
 * Created by Dhruv on 2/15/18.
 */

public class SharedInboxListAdapter extends RecyclerView.Adapter<SharedInboxListAdapter.ViewHolder>{


    private static final String TAG = "SharedInboxListAdapter";
    private Context mContext;
    private List<SharedInboxList> sharedInboxLists;
    private RecycleViewSharedInboxClickListener mRecycleViewSharedInboxToolClickListener;

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView
                textViewInboxToolItem;
        private ImageView
                imageViewInboxToolItem;


        public ViewHolder(View v) {
            super(v);
            imageViewInboxToolItem = (ImageView) v.findViewById(R.id.imageViewInboxToolItem);
            textViewInboxToolItem = (TextView) v.findViewById(R.id.textViewInboxToolItem);
        }

    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param sharedInboxLists ArrayList<LeadModal> containing the data to populate views to be used by RecyclerView.
     */
    public SharedInboxListAdapter( Context mContext
            , List<SharedInboxList> sharedInboxLists,RecycleViewSharedInboxClickListener mRecycleViewSharedInboxToolClickListener) {
        this.sharedInboxLists = sharedInboxLists;
        this.mContext = mContext;
        this.mRecycleViewSharedInboxToolClickListener = mRecycleViewSharedInboxToolClickListener;
    }



    // Create new views (invoked by the layout manager)
    @Override
    public SharedInboxListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_item_inbox_message_tools, viewGroup, false);

        return new SharedInboxListAdapter.ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(SharedInboxListAdapter.ViewHolder viewHolder, final int position) {
        final SharedInboxList modal = sharedInboxLists.get(position);
        viewHolder.textViewInboxToolItem.setText(modal.getInbox_name());

        int color = Color.parseColor("#3F51B5");;
        if (modal.getColour() != null){
            if (modal.getColour().length() > 0){
                if (modal.getColour().contains("#"))
                    color = Color.parseColor(modal.getColour());
            }
        }
        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(color)
                .borderWidthDp(2)
                .cornerRadiusDp(22)
                .oval(false)
                .build();
        Picasso.with(mContext)
                .load(R.mipmap.ic_fiber_manual_record_white_48dp)
                .fit()
                .transform(transformation)
                .into(viewHolder.imageViewInboxToolItem);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRecycleViewSharedInboxToolClickListener.onClickSharedInbox(modal);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return sharedInboxLists.size();
    }

}
