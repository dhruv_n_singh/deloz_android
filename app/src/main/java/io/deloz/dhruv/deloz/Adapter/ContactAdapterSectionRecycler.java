package io.deloz.dhruv.deloz.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amulyakhare.textdrawable.TextDrawable;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.deloz.dhruv.deloz.Helper.ImageCircleTransformation;
import io.deloz.dhruv.deloz.Helper.SectionedRecycleView.SectionRecyclerViewAdapter;
import io.deloz.dhruv.deloz.Modal.Contact;
import io.deloz.dhruv.deloz.R;

/**
 * Created by Dhruv on 1/11/18.
 */

public class ContactAdapterSectionRecycler extends SectionRecyclerViewAdapter<ContactSectionHeader,
        Contact, ContactSectionViewHolder, ContactViewHolder> {

   public static Context context;
   public static boolean isPeople = true;
   private static final String TAG = ContactAdapterSectionRecycler.class.getName();

    public ContactAdapterSectionRecycler(Context context, List<ContactSectionHeader> sectionHeaderItemList) {
        super(context, sectionHeaderItemList);
        this.context = context;
    }

    @Override
    public ContactSectionViewHolder onCreateSectionViewHolder(ViewGroup sectionViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.contact_row_section_header, sectionViewGroup, false);
        return new ContactSectionViewHolder(view);
    }

    @Override
    public ContactViewHolder onCreateChildViewHolder(ViewGroup childViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.contact_row_item, childViewGroup, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindSectionViewHolder(ContactSectionViewHolder sectionViewHolder, int sectionPosition, ContactSectionHeader sectionHeader) {
        sectionViewHolder.textViewContactHeaderStartsWith.setText(sectionHeader.sectionText);
    }

    @Override
    public void onBindChildViewHolder(ContactViewHolder childViewHolder, int sectionPosition, int childPosition, Contact child) {
        String name = "";

        if (isPeople){
            if (child.getFirstName() != null && child.getFirstName() != "null"){
                name = name + child.getFirstName();
            }
            if (child.getLastName() != null && child.getLastName() != "null"){
                name = name + " "+  child.getLastName();
            }
            if (name.length() > 0){
                TextDrawable drawabledummy = TextDrawable.builder()
                        .beginConfig()
                        .textColor(Color.WHITE)
                        .bold()
                        .useFont(Typeface.DEFAULT)
                        .fontSize(24) /* size in px */
                        .toUpperCase()
                        .width(100)
                        .height(100)
                        .endConfig()
                        .buildRoundRect(String.format("%c",name.charAt(0)), R.color.colorLightLayout, 50);
                childViewHolder.imgLogo.setImageDrawable(drawabledummy);
            }else {
                TextDrawable drawabledummy = TextDrawable.builder()
                        .beginConfig()
                        .textColor(Color.WHITE)
                        .bold()
                        .useFont(Typeface.DEFAULT)
                        .fontSize(24) /* size in px */
                        .toUpperCase()
                        .width(100)
                        .height(100)
                        .endConfig()
                        .buildRoundRect("", R.color.colorLightLayout, 50);
                childViewHolder.imgLogo.setImageDrawable(drawabledummy);
            }

            if (child.getProfileImage() != null && child.getProfileImage() != "null"){
                if ((child.getProfileImage().length() > 0) && (child.getProfileImage().startsWith("http") == true)){
                    Picasso
                            .with(context)
                            .load(child.getProfileImage())
                            .transform(new ImageCircleTransformation())
                            .resize(100, 100)
                            .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .into(childViewHolder.imgLogo);
                }else {
                    String startsWithName = "";
                    if (name.length() > 0){
                        startsWithName = startsWithName + name.charAt(0);
                    }

                    if (child.getColor() != null && child.getColor() != "null"){
                        if (child.getColor().length() > 0){
                            Log.d(TAG + " onBindChildViewHolder "," isPeople " + name + " " + startsWithName.toUpperCase());
                            int color = Color.parseColor(child.getColor());
                            TextDrawable drawable = TextDrawable.builder()
                                    .beginConfig()
                                    .textColor(Color.WHITE)
                                    .bold()
                                    .useFont(Typeface.DEFAULT)
                                    .fontSize(24) /* size in px */
                                    .toUpperCase()
                                    .width(100)
                                    .height(100)
                                    .endConfig()
                                    .buildRoundRect(startsWithName.toUpperCase(), color, 50);
                            childViewHolder.imgLogo.setImageDrawable(drawable);
                        }
                    }

                }
            }else {

                String startsWithName = "";
                if (name.length() > 0){
                    startsWithName = startsWithName + name.charAt(0);
                }

                if (child.getColor() != null && child.getColor() != "null"){
                    if (child.getColor().length() > 0){
                        Log.d(TAG + " onBindChildViewHolder "," isPeople " + name + " " + startsWithName.toUpperCase());
                        int color = Color.parseColor(child.getColor());
                        TextDrawable drawable = TextDrawable.builder()
                                .beginConfig()
                                .textColor(Color.WHITE)
                                .bold()
                                .useFont(Typeface.DEFAULT)
                                .fontSize(24) /* size in px */
                                .toUpperCase()
                                .width(100)
                                .height(100)
                                .endConfig()
                                .buildRoundRect(startsWithName.toUpperCase(), color, 50);
                        childViewHolder.imgLogo.setImageDrawable(drawable);
                    }
                }
            }
        }else {

            name = "";
            String startsWithName = "";

            if (child.getOrganizationName() != null && child.getOrganizationName() != "null"){
                name = name + child.getOrganizationName();
            }
            if (name.length() > 0){
                startsWithName = startsWithName + name.charAt(0);
            }
            if (name.length() > 0){
                TextDrawable drawabledummy = TextDrawable.builder()
                        .beginConfig()
                        .textColor(Color.WHITE)
                        .bold()
                        .useFont(Typeface.DEFAULT)
                        .fontSize(24) /* size in px */
                        .toUpperCase()
                        .width(100)
                        .height(100)
                        .endConfig()
                        .buildRoundRect(String.format("%c",name.charAt(0)), R.color.colorLightLayout, 50);
                childViewHolder.imgLogo.setImageDrawable(drawabledummy);
            }else {
                TextDrawable drawabledummy = TextDrawable.builder()
                        .beginConfig()
                        .textColor(Color.WHITE)
                        .bold()
                        .useFont(Typeface.DEFAULT)
                        .fontSize(24) /* size in px */
                        .toUpperCase()
                        .width(100)
                        .height(100)
                        .endConfig()
                        .buildRoundRect("", R.color.colorLightLayout, 50);
                childViewHolder.imgLogo.setImageDrawable(drawabledummy);
            }


            if (child.getColor() != null && child.getColor() != "null"){
                if (child.getColor().length() > 0){
                    Log.d(TAG + " onBindChildViewHolder "," isOrganization " + name + " " + startsWithName.toUpperCase());
                    int color = Color.parseColor(child.getColor());
                    TextDrawable drawable = TextDrawable.builder()
                            .beginConfig()
                            .textColor(Color.WHITE)
                            .bold()
                            .useFont(Typeface.DEFAULT)
                            .fontSize(24) /* size in px */
                            .toUpperCase()
                            .width(100)
                            .height(100)
                            .endConfig()
                            .buildRoundRect(startsWithName.toUpperCase(), color, 50);
                    childViewHolder.imgLogo.setImageDrawable(drawable);
                }
            }
        }
        childViewHolder.textViewContactName.setText(name);


      }

    }
