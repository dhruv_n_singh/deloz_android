package io.deloz.dhruv.deloz.Adapter;

import java.util.List;

import io.deloz.dhruv.deloz.Helper.SectionedRecycleView.Section;
import io.deloz.dhruv.deloz.Modal.Contact;

/**
 * Created by Dhruv on 1/11/18.
 */

public class ContactSectionHeader implements Section<Contact>, Comparable<ContactSectionHeader>  {

    List<Contact> childList;
    String sectionText;
    int index;

    public ContactSectionHeader(List<Contact> childList, String sectionText, int index) {
        this.childList = childList;
        this.sectionText = sectionText;
        this.index = index;
    }

    @Override
    public List<Contact> getChildItems() {
        return childList;
    }

    public String getSectionText() {
        return sectionText;
    }

    @Override
    public int compareTo(ContactSectionHeader another) {
        if (this.index > another.index) {
            return -1;
        } else {
            return 1;
        }
    }

}
