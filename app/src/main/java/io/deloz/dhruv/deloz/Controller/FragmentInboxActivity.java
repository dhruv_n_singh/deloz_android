package io.deloz.dhruv.deloz.Controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jaychang.srv.OnLoadMoreListener;
import com.jaychang.srv.SimpleCell;
import com.jaychang.srv.SimpleRecyclerView;
import com.jaychang.srv.decoration.SectionHeaderProvider;
import com.jaychang.srv.decoration.SimpleSectionHeaderProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import io.deloz.dhruv.deloz.Adapter.InboxCell;
import io.deloz.dhruv.deloz.Helper.API;
import io.deloz.dhruv.deloz.Helper.HttpUtility;
import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.Interfaces.RecycleViewPipelineListClickListener;
import io.deloz.dhruv.deloz.Modal.Inbox;
import io.deloz.dhruv.deloz.Modal.InboxCategory;
import io.deloz.dhruv.deloz.R;
import okhttp3.Response;

/**
 * Created by Dhruv on 12/19/17.
 */

public class FragmentInboxActivity  extends Fragment {

     /**
     * UI Components
     */

     SimpleRecyclerView simpleRecyclerView;
     private ProgressDialog mProgressDialog;
     Toolbar mToolbar;
     Spinner mSpinner;
     MenuItem editMenuItem;
     private ActionMode mActionMode;

     /**
     * Variables
     */

     private boolean multiSelect = false;
     String project_id ;
     String accountKey;
     String deviceId ;
     String auth_code ;
     String currentIndex;
     private FragmentInboxActivity.OnFragmentInteractionListener mListenerFragment;
     public FragmentInboxActivity() {}
     private static final String TAG = FragmentInboxActivity.class.getName();
     int currentPageNumber = 0;
     List<Inbox> listInboxData;
     private boolean hasMoreData = true;


    /**
     * Activity Lifecycle
     * @param savedInstanceState
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_inbox, container, false);
        setRetainInstance(true);
        if (mListenerFragment != null) {
            mListenerFragment.onFragmentInteraction("Inbox");

        }
        setHasOptionsMenu(true);
        currentPageNumber = 0;
        currentIndex = "0";
        hasMoreData = true;
        setupInitial(view);
        this.addRecyclerHeaders();
        this.loadSelectedMenuData(true,currentIndex,false);
        setDropDownToolBar(view);
        return view;
    }


    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item_inbox_toolbar, menu);
        editMenuItem = menu.findItem(R.id.edit_item_select_inbox);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case R.id.edit_item_select_inbox:
                Log.e(TAG," Clicked on ActionBar Select Inbox!");
                Intent intent = new Intent(getActivity(), InboxMessagesToolsActivity.class);
                startActivityForResult(intent,Utility.RequestResultConst.PICK_INBOX_REQUEST);
                return true;
            case R.id.edit_item_inbox_search:
                Log.e(TAG," Clicked on ActionBar Selected Search");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,"onActivityResult:"+ requestCode + resultCode);
        if (requestCode == Utility.RequestResultConst.PICK_INBOX_REQUEST) {
            if(resultCode == Activity.RESULT_OK){
                if (data.hasExtra("key")){
                    currentPageNumber = 0;
                    String sharedInboxKey = data.getStringExtra("key");
                    String sharedInboxName = data.getStringExtra("name");
                    editMenuItem.setTitle(sharedInboxName);
                    Log.d(TAG,"Wow getting something back: " + sharedInboxKey );
                    String url = API.KBaseUrlDeloz + "/" + accountKey + API.kGetSharedInboxMessages  + "/" +  sharedInboxKey + "?page=" + currentPageNumber;
                    loadMenu(true, url,false);
                }
                if (data.hasExtra("position")){
                    String position = data.getStringExtra("position");
                    Log.d(TAG,"Selected position: " + position );
                    currentPageNumber = 0;
                    currentIndex = position;
                    hasMoreData = true;
                    changeMenuItemTitle(position);
                    loadSelectedMenuData(true,position,false);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.d(TAG,"There's no result");
            }
        }
    }

    private void changeMenuItemTitle(String position){
        switch (position){
            case "0":
                editMenuItem.setTitle(R.string.inbox_all);
            break;
            case "1":
                editMenuItem.setTitle(R.string.inbox_favourites);
                break;
            case "2":
                editMenuItem.setTitle(R.string.inbox_assigned);
                break;
            case "3":
                editMenuItem.setTitle(R.string.inbox_send);
                break;
            case "4":
                editMenuItem.setTitle(R.string.inbox_draft);
                break;
            case "5":
                editMenuItem.setTitle(R.string.inbox_trash);
                break;
            default:
                break;
        }
    }

    public void setDropDownToolBar(View view){
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        //you can also set the style with the constructor
        mSpinner = new Spinner(getActivity());
        mSpinner.getBackground().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);

        mSpinner.setGravity(Gravity.LEFT);
        final String[] frags = new String[]{"Inbox"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,frags);
        mSpinner.setAdapter(arrayAdapter);
        mSpinner.setSelection(0, true);
        View v = mSpinner.getSelectedView();
        TextView SpinnerText = (TextView)v;
        ((TextView)v).setTextSize(Utility.SPINNER_TEXT_SIZE);
        if (SpinnerText == null) {
            Log.d(TAG,"Not found");
        } else {
            SpinnerText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        mSpinner.cancelPendingInputEvents();
                    }
                    //Creating the instance of PopupMenu
                    PopupMenu popup = new PopupMenu(getContext(), arg0);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater().inflate(R.menu.menu_item_inbox_filter, popup.getMenu());

                    //registering popup with OnMenuItemClickListener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                           // Toast.makeText(getContext(),"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                            return true;
                        }
                    });

                    popup.show();//showing popup menu
                }
            });
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            ((TextView)v).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
        }
        ((TextView)v).setTextColor(getResources().getColor(R.color.colorWhite));
        mToolbar.addView(mSpinner);




        //Set the listener for when each option is clicked.
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    mSpinner.cancelPendingInputEvents();
                }
                //Change the selected item's text color
                ((TextView) view).setTextColor(getResources().getColor(R.color.colorWhite));
                ((TextView)view).setTextSize(Utility.SPINNER_TEXT_SIZE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

        });
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentDashboardActivity.OnFragmentInteractionListener) {
            mListenerFragment = (FragmentInboxActivity.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }

        // Makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListenerFragment = null;
        Log.d(TAG," onDetach ");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mToolbar != null && mSpinner != null) {
            mToolbar.removeView(mSpinner);
        }
    }

    /**
     * Functions
     */

    public void setupInitial(View view){

        listInboxData = new ArrayList<>();
        project_id = PreferenceManager.getDefaultSharedPreferences(getContext())
                .getString(API.SharedPreferenceKeys.project_id, "");
        accountKey = PreferenceManager.getDefaultSharedPreferences(getContext())
                .getString(API.SharedPreferenceKeys.account_key, "");
        deviceId = Utility.getDeviceId(getContext());
        auth_code = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.auth_code, "");
        simpleRecyclerView = view.findViewById(R.id.recyclerViewInbox);
        simpleRecyclerView.setLoadMoreToTop(false);
        simpleRecyclerView.setLoadMoreView(R.layout.row_item_progress);
        simpleRecyclerView.setOnLoadMoreListener(new OnLoadMoreListener() {

            @Override
            public boolean shouldLoadMore() {
                return hasMoreData;
            }

            @Override
            public void onLoadMore() {
                loadSelectedMenuData(false,currentIndex,true);
            }

        });

    }

    private ActionMode.Callback actionModeCallbacks = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            Log.d(TAG," onCreateActionMode ");
            mode.setTitle("0 item selected");
            mode.getMenuInflater().inflate(R.menu.inbox_context_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            Log.d(TAG," onPrepareActionMode ");
            menu.findItem(R.id.inbox_delete_selected_msg).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            menu.findItem(R.id.edit_inbox_msgs).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            Log.d(TAG," onActionItemClicked ");
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            Log.d(TAG," onDestroyActionMode ");
            mActionMode = null;
            multiSelect = false;
        }

    };

    private void loadInbox() {
        setupInbox(false);
    }

    private void bindBooks(List<Inbox> inboxes) {

        //CUSTOM SORT ACCORDING TO CATEGORIES
        Collections.sort(inboxes, new Comparator<Inbox>() {
            public int compare(Inbox o1, Inbox o2) {
                return o2.getDateTime().compareTo(o1.getDateTime());
            }
        });

        List<InboxCell> cells = new ArrayList<>();

        //LOOP THROUGH GALAXIES INSTANTIATING THEIR CELLS AND ADDING TO CELLS COLLECTION
        for (Inbox inbox : inboxes) {

            final RecycleViewPipelineListClickListener mListener = new RecycleViewPipelineListClickListener() {
                @Override
                public void onClickView(int position) {
                    Log.e("onCellClicked "," position: " + position);
                }
            };
            io.deloz.dhruv.deloz.Interfaces.InboxCellLongPressListner  mLongPressListner = new io.deloz.dhruv.deloz.Interfaces.InboxCellLongPressListner(){

                @Override
                public void onLongPress(int position){
                    multiSelect = true;
                    if (mListenerFragment != null){
                        Log.e(TAG + " longPress "," position: " + position);
                        mListenerFragment.onLongPressClickedToEditMessage("0 selected");
                        multiSelect = true;
                    }
                }
            };
            InboxCell cell = new InboxCell(inbox,mLongPressListner,mListener,getContext(),listInboxData);
            cells.add(cell);
        }
        simpleRecyclerView.addCells(cells);
    }

    /*
     - Create RecyclerViewHeaders
    */
    private void addRecyclerHeaders(){

        SectionHeaderProvider<Inbox> sh=new SimpleSectionHeaderProvider<Inbox>() {

            @NonNull
            @Override
            public View getSectionHeaderView(@NonNull Inbox inbox, int i) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.row_item_inbox_header, null, false);
                TextView textView =  view.findViewById(R.id.headerTxt);
                int numberOfDays = Utility.daysBtwDates(inbox.getDateTime());
                if (numberOfDays <= 6){
                    switch (numberOfDays){
                        case 0:
                            textView.setText(R.string.today);
                            break;
                        case 1:
                            textView.setText(R.string.yesterday);
                            break;
                        default:
                            textView.setText(Utility.getDayOfWeek(inbox.getDateTime()));
                            break;
                    }
                }else {
                    textView.setText(Utility.getFormattedDate(inbox.getDateTime(),"dd-MMM-yyyy"));
                }
                return view;
            }

            @Override
            public boolean isSameSection(@NonNull Inbox inbox, @NonNull Inbox nextInbox) {
                return inbox.getInboxCategory().getName().equalsIgnoreCase(nextInbox.getInboxCategory().getName()) ;
            }
            // Optional, whether the header is sticky, default false
            @Override
            public boolean isSticky() {
                return true;
            }

        };
        simpleRecyclerView.setSectionHeader(sh);
    }


    /*
  - Bind data to our RecyclerView
   */
    private void bindData(List<Inbox> inboxList) {
        simpleRecyclerView.removeAllCells(true);
        //CUSTOM SORT ACCORDING TO CATEGORIES
        Collections.sort(inboxList, new Comparator<Inbox>() {
            public int compare(Inbox o1, Inbox o2) {
                return o2.getDateTime().compareTo(o1.getDateTime());
            }
        });

        List<InboxCell> cells = new ArrayList<>();

        //LOOP THROUGH GALAXIES INSTANTIATING THEIR CELLS AND ADDING TO CELLS COLLECTION
        for (Inbox inbox : inboxList) {
            RecycleViewPipelineListClickListener mListener = new RecycleViewPipelineListClickListener() {
                @Override
                public void onClickView(int position) {
                    Log.e("onCellClicked "," position: " + position);
                }
            };

            io.deloz.dhruv.deloz.Interfaces.InboxCellLongPressListner  mLongPressListner = new io.deloz.dhruv.deloz.Interfaces.InboxCellLongPressListner(){

                @Override
                public void onLongPress(int position){
                    Log.e(TAG + " ongPressListner"," position: " + position);
                    mListenerFragment.onLongPressClickedToEditMessage("0 selected");
                    multiSelect = true;
                }

            };

            InboxCell cell = new InboxCell(inbox,mLongPressListner,mListener,getContext(),listInboxData);
            cells.add(cell);
        }

        simpleRecyclerView.addCells(cells);
        synchronized (simpleRecyclerView){
           simpleRecyclerView.notifyAll();
        }

    }

    /**
     * Webservice Response
     */
    public void setupInbox(Boolean isShowActivity){
        if (Utility.isOnline(getContext())){
            String url = API.KBaseUrlDeloz + "/" + accountKey + API.kGetMails + "?page=" + currentPageNumber;
            getInboxDetails(url,isShowActivity,false,currentPageNumber);

        }else {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_no_internet_connection)
                    .setMessage(getString(R.string.error_internet_connection_message))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert).show();
        }

    }

    public void getInboxDetails(final String url, final Boolean isShowActivity, final Boolean isPaginated, final int index){

        if (Utility.isOnline(getContext())){
            if (isShowActivity){
                mProgressDialog = new ProgressDialog(getContext());
                mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }

            String deviceId = Utility.getDeviceId(getContext());
            String auth_code = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.auth_code, "");
            try {
                JSONObject headerData = new JSONObject();
                headerData.put(API.SharedPreferenceKeys.device_key,deviceId);
                headerData.put(API.SharedPreferenceKeys.auth_code,auth_code);
                HttpUtility.getData(url,HttpUtility.getHeaders(headerData),getContext(),new HttpUtility.HttpCallback(){

                    @Override
                    public void onFailure(Response response, IOException e) {
                        if (mProgressDialog != null) {
                            mProgressDialog.dismiss();
                        }
                        currentPageNumber = currentPageNumber - 1;
                        if (currentPageNumber < 0){
                            currentPageNumber = 0;
                        }
                        hasMoreData = false;
                        bindData(listInboxData);
                        Log.d(TAG ," onFailure: currentPage: " + currentPageNumber);
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                List<SimpleCell> cells = new ArrayList<>();
                                simpleRecyclerView.addCells(cells);
                                new AlertDialog.Builder(getActivity())
                                        .setTitle("")
                                        .setMessage(R.string.error_unable_to_get_response)
                                        .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                getInboxDetails(url,isShowActivity,isPaginated,index);
                                            }
                                        }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                }).setIcon(android.R.drawable.ic_dialog_alert).show();

                            }
                        });


                    }

                    @Override
                    public void onSuccess(Response response) {


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (mProgressDialog != null) {
                                    mProgressDialog.dismiss();
                                }
                            }
                        });


                        final String result;
                        try {
                            result = response.body().string();
                            if (response.isSuccessful()){
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (Utility.isRedirectFound(result)){
                                            Utility.sessionLogout(result,getActivity(),getActivity());
                                        }else {
                                            try{
                                                JSONObject jsonResponse = new JSONObject(result);
                                                if (jsonResponse.has("payload")){
                                                    Log.d(TAG + " payload: ",jsonResponse.toString());
                                                        JSONArray jsonList = jsonResponse.getJSONArray("payload");
                                                    if (jsonList.length() > 0){
                                                            currentPageNumber = currentPageNumber + 1;
                                                            Log.d(TAG ," onSuccess: updated currentPage: " + currentPageNumber);
                                                        }else {
                                                            hasMoreData = false;
                                                            currentPageNumber = currentPageNumber - 1;
                                                            if (currentPageNumber < 0){
                                                                currentPageNumber = 0;
                                                            }
                                                            bindData(listInboxData);
                                                            Log.d(TAG ," onSuccess: no data currentPage: " + currentPageNumber);
                                                        }
                                                        if (isPaginated){
                                                            List<Inbox> inboxList = parseInboxDetails(jsonList);
                                                            for (int i = 0;i< inboxList.size();i++){
                                                                listInboxData.add(inboxList.get(i));
                                                            }
                                                            bindBooks(listInboxData);
                                                        }else {
                                                           // simpleRecyclerView.removeAllCells();
                                                            listInboxData =  parseInboxDetails(jsonList);
                                                            bindData(listInboxData);
                                                        }

                                                }
                                            }catch (JSONException e){
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });

                            }else {
                                Toast.makeText(getContext(), R.string.error_unable_to_get_response, Toast.LENGTH_SHORT).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                });
            } catch (JSONException e){
                e.printStackTrace();
            }

        }else {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_no_internet_connection)
                    .setMessage(getString(R.string.error_internet_connection_message))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert).show();
        }

    }

    /**
     * Json Parsing
     */

    public List<Inbox> parseInboxDetails(JSONArray jsonArrayData){
        Log.d(TAG,"parseInboxDetails");
        List<Inbox> inboxList = new ArrayList<>();
        for (int i = 0;i < jsonArrayData.length();i++){
            Inbox modal = new Inbox();
            try{
                JSONObject jsonObjectData = jsonArrayData.getJSONObject(i);
                if (jsonObjectData.has("replied")){
                    Boolean replied  = jsonObjectData.getBoolean("replied");
                    modal.setReplied(replied);
                }

                if (jsonObjectData.has("status")){
                    int status = jsonObjectData.getInt("status");
                    modal.setStatus(status);
                }

                if (jsonObjectData.optBoolean("attachments")){
                    modal.setAttachments(jsonObjectData.optBoolean("attachments"));
                }

                if (jsonObjectData.has("profile_pic")){
                    modal.setProfile_pic(jsonObjectData.getString("profile_pic"));
                }
                if (jsonObjectData.has("updated_at")){
                    modal.setUpdated_at(jsonObjectData.getString("updated_at"));
                }
                if (jsonObjectData.has("message")){
                   modal.setMessage(jsonObjectData.getString("message"));
                }

                if (jsonObjectData.has("subject")){
                    modal.setSubject(jsonObjectData.getString("subject"));
                }

                if (jsonObjectData.has("from")){
                    modal.setFrom(jsonObjectData.getString("from"));
                }

                if (jsonObjectData.has("contact_color")){
                    modal.setContact_color(jsonObjectData.getString("contact_color"));
                }

                if (jsonObjectData.has("to")){
                    JSONArray jsonArrayTo = jsonObjectData.getJSONArray("to");
                    List<String>stringListTo = new ArrayList<>();
                    for (int to_index = 0; to_index < jsonArrayTo.length();to_index++){
                        String stringTo = jsonArrayTo.getString(to_index);
                        stringListTo.add(stringTo);
                        }
                        modal.setToList(stringListTo);
                }

                if (jsonObjectData.has("draft")){
                    Boolean draft = jsonObjectData.getBoolean("draft");
                    modal.setDraft(draft);
                }

                if (jsonObjectData.has("project_id")){
                    modal.setProject_id(jsonObjectData.getString("project_id"));
                }

                if (jsonObjectData.has("body")){
                    modal.setBody(jsonObjectData.getString("body"));
                }

                if (jsonObjectData.has("handle")){
                    modal.setHandle(jsonObjectData.getString("handle"));
                }

                if (jsonObjectData.has("account_id")){
                    modal.setAccount_id(jsonObjectData.getString("account_id"));
                }

                if (jsonObjectData.has("read")){
                    Boolean read = jsonObjectData.getBoolean("read");
                    modal.setRead(read);
                }

                if (jsonObjectData.has("sharedinbox")){
                    modal.setSharedinbox(jsonObjectData.getString("sharedinbox"));
                }

                if (jsonObjectData.has("members")){
                    JSONArray jsonArrayMembers = jsonObjectData.getJSONArray("members");
                    List<String> stringListMembers = new ArrayList<>();
                    for (int index_members = 0 ; index_members < jsonArrayMembers.length();index_members++){
                            String member = jsonArrayMembers.getString(index_members);
                            stringListMembers.add(member);
                        }
                        modal.setMembersList(stringListMembers);
                }

                if (jsonObjectData.has("date")){
                    String date = jsonObjectData.getString("date");
                    modal.setDate(date);
                    if (date != null && date != "null"){
                        if (date.length() > 0){
                            Date convertedDate = Utility.getDate(date);
                            modal.setDateTime(convertedDate);
                            modal.setDuration(Utility.getFormattedTime(date));
                            InboxCategory inboxCategory = new InboxCategory(Utility.getDateString(date));
                            modal.setInboxCategory(inboxCategory);
                        }
                    }
                }

                if (jsonObjectData.has("placeholder")){
                    modal.setPlaceholder(jsonObjectData.getString("placeholder"));
                }

                if (jsonObjectData.has("_key")){
                    modal.set_key(jsonObjectData.getString("_key"));
                }

                if (jsonObjectData.has("created_at")){
                    modal.setCreated_at(jsonObjectData.getString("created_at"));
                }

                if (jsonObjectData.has("starred")){
                    Boolean starred = jsonObjectData.getBoolean("starred");
                    modal.setStarred(starred);
                }

                if (jsonObjectData.has("_id")){
                    modal.set_id(jsonObjectData.getString("_id"));
                }
                if (jsonObjectData.has("colour")){
                    modal.setColour(jsonObjectData.getString("colour"));
                }

                modal.setLoadingIndicator(false);

                inboxList.add(modal);
            }catch (JSONException e){
                e.printStackTrace();
            }

        }
        return inboxList;
    }

    public void loadSelectedMenuData(Boolean isShowIndicator,String index,Boolean isPaginated){
        Log.d(TAG,"loadSelectedMenuData " + index);
        switch (index){
            case "0": {
                String url = API.KBaseUrlDeloz + "/" + accountKey + API.kGetMails + "?page=" + currentPageNumber;
                loadMenu(isShowIndicator, url,isPaginated);
                break;
            }
            case "1": {
                String url = API.KBaseUrlDeloz + "/" + accountKey + API.kGetFavourites + "?page=" + currentPageNumber;
                loadMenu(isShowIndicator, url,isPaginated);
                break;
            }
            case "2": {
                String url = API.KBaseUrlDeloz + "/" + accountKey + API.kGetAssignedToMe + "?page=" + currentPageNumber;
                loadMenu(isShowIndicator, url,isPaginated);
            }
                break;
            case "3": {
                String url = API.KBaseUrlDeloz + "/" + accountKey + API.kGetSendMails + "?page=" + currentPageNumber;
                loadMenu(isShowIndicator, url,isPaginated);
            }
                break;
            case "4":{
                String url = API.KBaseUrlDeloz + "/" + accountKey + API.kGetDrafts + "?page=" + currentPageNumber;
                loadMenu(isShowIndicator, url,isPaginated);
            }
                break;
            case "5":{
                String url = API.KBaseUrlDeloz + "/" + accountKey + API.kGetTrash + "?page=" + currentPageNumber;
                loadMenu(isShowIndicator, url,isPaginated);
            }
                break;
        }

    }

    public void loadMenu(Boolean isShowIndicator,String url,Boolean isPaginated){
        if (Utility.isOnline(getContext())){
            getInboxDetails(url,isShowIndicator,isPaginated,currentPageNumber);
        }else {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_no_internet_connection)
                    .setMessage(getString(R.string.error_internet_connection_message))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert).show();
        }
    }


    /**
     * Interface
     */


    public interface OnFragmentInteractionListener {

        // NOTE : We changed the Uri to String.
        void onFragmentInteraction(String title);
        void onLongPressClickedToEditMessage(String title);
    }


}
