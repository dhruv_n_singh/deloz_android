package io.deloz.dhruv.deloz.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import io.deloz.dhruv.deloz.Interfaces.RecycleViewInboxToolClickListener;
import io.deloz.dhruv.deloz.Modal.InboxTool;
import io.deloz.dhruv.deloz.R;

/**
 * Created by Dhruv on 2/14/18.
 */

public class InboxToolAdapter extends RecyclerView.Adapter<InboxToolAdapter.ViewHolder> {

    private static final String TAG = "InboxToolAdapter";
    private Context mContext;
    private List<InboxTool> inboxToolList;
    private RecycleViewInboxToolClickListener mRecycleViewInboxToolClickListener;

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView
                textViewInboxToolItem;
        private ImageView
                imageViewInboxToolItem;


        public ViewHolder(View v) {
            super(v);
            imageViewInboxToolItem = (ImageView) v.findViewById(R.id.imageViewInboxToolItem);
            textViewInboxToolItem = (TextView) v.findViewById(R.id.textViewInboxToolItem);
        }

    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param inboxToolList ArrayList<LeadModal> containing the data to populate views to be used by RecyclerView.
     */
    public InboxToolAdapter( Context mContext
            , List<InboxTool> inboxToolList,RecycleViewInboxToolClickListener recycleViewInboxToolClickListener) {
        this.inboxToolList = inboxToolList;
        this.mContext = mContext;
        this.mRecycleViewInboxToolClickListener = recycleViewInboxToolClickListener;
    }



    // Create new views (invoked by the layout manager)
    @Override
    public InboxToolAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_item_inbox_message_tools, viewGroup, false);

        return new InboxToolAdapter.ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(InboxToolAdapter.ViewHolder viewHolder, final int position) {
        InboxTool modal = inboxToolList.get(position);
        viewHolder.textViewInboxToolItem.setText(modal.getTitle());

        Picasso
                .with(mContext)
                .load(modal.getImage())
                .into(viewHolder.imageViewInboxToolItem);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRecycleViewInboxToolClickListener.onClickInboxToolView(position);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return inboxToolList.size();
    }
}
