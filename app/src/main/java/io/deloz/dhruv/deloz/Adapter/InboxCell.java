package io.deloz.dhruv.deloz.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.jaychang.srv.SimpleCell;
import com.jaychang.srv.SimpleViewHolder;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.deloz.dhruv.deloz.Helper.ImageCircleTransformation;
import io.deloz.dhruv.deloz.Interfaces.RecycleViewPipelineListClickListener;
import io.deloz.dhruv.deloz.Modal.Inbox;
import io.deloz.dhruv.deloz.R;

/**
 * Created by Dhruv on 2/6/18.
 */

public class InboxCell extends SimpleCell<Inbox,InboxCell.ViewHolder>  {

    public static final int ITEM = 0;
    public static final int LOADING = 1;
    public boolean isLoadingAdded = false;
    List<Inbox> listInboxData;
    private RecycleViewPipelineListClickListener mListener;
    public Context mContext;
    public Inbox itemModal ;
    private io.deloz.dhruv.deloz.Interfaces.InboxCellLongPressListner mLongPressListner;

    public InboxCell(@NonNull Inbox item ,io.deloz.dhruv.deloz.Interfaces.InboxCellLongPressListner mLongPressListner, RecycleViewPipelineListClickListener listener, Context mContext, List<Inbox> listInboxData) {
        super(item);
        this.mListener = listener;
        this.mContext = mContext;
        this.listInboxData = listInboxData;
        this.itemModal = item;
        this.mLongPressListner = mLongPressListner;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.row_item_inbox;
    }

    /*
    - Return a ViewHolder instance
     */
    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(ViewGroup parent, View cellView) {
            return new ViewHolder(cellView);
    }

    /*
    - Bind data to widgets in our viewholder.
     */
    @Override
    protected void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i, @NonNull Context context, Object o) {

        Inbox modal = getItem();
        if (modal.getHandle() != null && modal.getHandle() != "null") {
            if (modal.getHandle().length() > 0) {
                String strHandle = modal.getHandle();
                if (strHandle.contains("<")) {
                    int index = strHandle.lastIndexOf('<');
                    viewHolder.textViewInboxTitle.setText(strHandle.substring(0, index));
                } else {
                    viewHolder.textViewInboxTitle.setText(modal.getHandle());
                }
            }
        }

        if (modal.getSubject() != null && modal.getSubject() != "null") {
            viewHolder.textViewInboxSubtitle.setText(modal.getSubject());
        }

        if (modal.getDuration() != null && modal.getDuration() != "null") {
            viewHolder.textViewInboxDate.setText(modal.getDuration());
        }

        if (modal.getBody() != null && modal.getBody() != "null") {
            viewHolder.textViewInboxMessage.setText(modal.getBody());
        }

        if (modal.getAttachments() != null) {
            if (modal.getAttachments()) {
                Picasso
                        .with(mContext)
                        .load(R.drawable.ic_attachment)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .into(viewHolder.imageViewInboxAttachment);
            } else {
                viewHolder.imageViewInboxAttachment.setImageDrawable(null);
            }
        }

        if (modal.getProfile_pic() != null && modal.getProfile_pic() != "null") {
            if (!modal.getProfile_pic().isEmpty()) {
                Picasso
                        .with(mContext)
                        .load(modal.getProfile_pic())
                        .transform(new ImageCircleTransformation())
                        .resize(100, 100)
                        // .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .into(viewHolder.imageViewInboxStartsWith);
            }
        } else {
            String startsWithName = "";
            if (modal.getHandle() != null) {
                if (modal.getHandle().length() > 0) {
                    int color = Color.parseColor("#3F51B5");
                    ;
                    if (modal.getColour() != null) {
                        if (modal.getColour().length() > 0) {
                            if (modal.getColour().contains("#"))
                                color = Color.parseColor(modal.getColour());
                        }
                    }
                    startsWithName = startsWithName + "" + modal.getHandle().charAt(0);
                    TextDrawable drawable = TextDrawable.builder()
                            .beginConfig()
                            .textColor(Color.WHITE)
                            .bold()
                            .useFont(Typeface.DEFAULT)
                            .fontSize(24) /* size in px */
                            .toUpperCase()
                            .width(100)
                            .height(100)
                            .endConfig()
                            .buildRoundRect(startsWithName, color, 50);
                    viewHolder.imageViewInboxStartsWith.setImageDrawable(drawable);
                }
            }
        }

        if (modal.getStarred() != null) {
            if (modal.getStarred()) {
                Picasso
                        .with(mContext)
                        .load(R.drawable.ic_inbox_unstar)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .into(viewHolder.imageViewInboxBookmark);
            } else {
                Picasso
                        .with(mContext)

                        .load(R.drawable.ic_inbox_star)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .into(viewHolder.imageViewInboxBookmark);
            }
        }




        viewHolder.getContainerView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onClickView(viewHolder.getAdapterPosition());
            }
        });

          viewHolder.getContainerView().setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                android.util.Log.d("InboxCell "," onLongClick");
                mLongPressListner.onLongPress(viewHolder.getAdapterPosition());

                return true;
            }

        });



    }


    /**
     - Our ViewHolder class.
     - Inner static class.
     * Define your view holder, which must extend SimpleViewHolder.
     * */
    static class ViewHolder extends SimpleViewHolder {

        TextView textViewInboxTitle,textViewInboxDate,textViewInboxSubtitle,textViewInboxMessage;
        ImageView imageViewInboxStartsWith,imageViewInboxBookmark,imageViewInboxAttachment;

        ViewHolder(View itemView) {
            super(itemView);
          textViewInboxTitle = itemView.findViewById(R.id.textViewInboxTitle);
          textViewInboxDate = itemView.findViewById(R.id.textViewInboxDate);
          textViewInboxSubtitle = itemView.findViewById(R.id.textViewInboxSubtitle);
          textViewInboxMessage = itemView.findViewById(R.id.textViewInboxMessage);
          imageViewInboxStartsWith = itemView.findViewById(R.id.imageViewInboxStartsWith);
          imageViewInboxBookmark = itemView.findViewById(R.id.imageViewInboxBookmark);
          imageViewInboxAttachment = itemView.findViewById(R.id.imageViewInboxAttachment);
        }

    }


}
