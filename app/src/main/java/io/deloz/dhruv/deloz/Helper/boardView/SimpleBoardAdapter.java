package io.deloz.dhruv.deloz.Helper.boardView;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.ArrayList;
import java.util.List;

import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.Modal.PipeLineDeal;
import io.deloz.dhruv.deloz.Modal.PipeLineStage;
import io.deloz.dhruv.deloz.R;

/**
 * Created by Dhruv on 1/27/18.
 */

public class SimpleBoardAdapter extends BoardAdapter{
    List<SimpleColumn> data = new ArrayList<>();
    int header_resource;
    int item_resource;

    public SimpleBoardAdapter(Context context, ArrayList<SimpleColumn> data) {
        super(context);
        this.data = data;
        this.header_resource = R.layout.column_header;
        this.item_resource = R.layout.column_item;
    }

    @Override
    public int getColumnCount() {
        return data.size();
    }

    @Override
    public int getItemCount(int column_position) {
        return data.get(column_position).items.size();
    }

    @Override
    public Object createHeaderObject(int column_position) {
        return data.get(column_position).header;
    }

    @Override
    public Object createFooterObject(int column_position) {
        return "Footer "+ String.valueOf(column_position);
    }

    @Override
    public Object createItemObject(int column_position, int item_position) {
        return data.get(column_position).items.get(item_position);
    }

    @Override
    public boolean isColumnLocked(int column_position) {
        return false;
    }

    @Override
    public boolean isItemLocked(int column_position) {
        // if(column_position == 2)
        //    return true;
        return false;
    }

    @Override
    public View createItemView(Context context, Object header_object, Object item_object, int column_position, int item_position) {
        View item = View.inflate(context, item_resource, null);
        TextView textViewPipelineDealName = (TextView)item.findViewById(R.id.textViewPipelineDealName);
        TextView textViewPipelineDealValue = (TextView)item.findViewById(R.id.textViewPipelineDealValue);
        TextView textViewPipelineUserName = (TextView)item.findViewById(R.id.textViewPipelineUserName);
        TextView textViewPipelineDealCreatedByName = (TextView)item.findViewById(R.id.textViewPipelineDealCreatedByName);
        TextView textViewPipelineDeal_Date = (TextView)item.findViewById(R.id.textViewPipelineDeal_Date);
        ImageView imageViewStartsWith = (ImageView)item.findViewById(R.id.imageViewPipelinePlaceholder);

        PipeLineDeal modal = data.get(column_position).items.get(item_position);
        textViewPipelineDealName.setText(modal.getName());
        textViewPipelineDealValue.setText("" + modal.getValue());
        if (modal.getCreatedByUsername() != null && modal.getCreatedByUsername() != "null"){
            textViewPipelineDealCreatedByName.setText(modal.getCreatedByUsername());
        }

        if (modal.getCreatedByUsername() != null && modal.getCreatedByUsername() != "null"){
            textViewPipelineUserName.setText(modal.getCreatedByUsername());
        }

        String assignedToName = "";
        String assignedToStartsWith = "";
        if (modal.getAssignedToFirstName() != null && modal.getAssignedToFirstName() != "null"){
            assignedToName = assignedToName + modal.getAssignedToFirstName();
            if (modal.getAssignedToFirstName().length() > 0){
                assignedToStartsWith = assignedToStartsWith + modal.getAssignedToFirstName().charAt(0);
            }
        }
        if (modal.getAssignedToLastName() != null && modal.getAssignedToLastName() != "null"){
            assignedToName = assignedToName + " " + modal.getAssignedToLastName();
            if (modal.getAssignedToLastName().length() > 0){
                assignedToStartsWith =  assignedToStartsWith + modal.getAssignedToLastName().charAt(0);
            }
        }
        int color = Color.parseColor("#54B9E8");;

        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .textColor(Color.WHITE)
                .bold()
                .useFont(Typeface.DEFAULT)
                .fontSize(12) /* size in px */
                .toUpperCase()
                .endConfig()
                .buildRoundRect(assignedToStartsWith, color, 40);
        imageViewStartsWith.setImageDrawable(drawable);

        textViewPipelineDealCreatedByName.setText(assignedToName);
        if (modal.getDue_date() != null && modal.getDue_date() != "null"){
            if (modal.getDue_date().length() > 0){
                String date = Utility.getFormattedDate(modal.getDue_date());
                if (date.trim().length() > 0){
                    textViewPipelineDeal_Date.setText(date);
                }else{
                    textViewPipelineDeal_Date.setText(R.string.error_no_due_date);
                }
            }

        }else{
            textViewPipelineDeal_Date.setText(R.string.error_no_due_date);

        }

        return item;
    }

    @Override
    public void createColumns() {
        super.createColumns();
    }

    @Override
    public View createHeaderView(Context context,Object header_object,int column_position) {
        View column = View.inflate(context, header_resource, null);
        TextView textView = (TextView)column.findViewById(R.id.textViewPipelineStageName);
        textView.setText(data.get(column_position).header.getStage_name());
        TextView textViewPipelineHeaderCount = (TextView)column.findViewById(R.id.textViewPipelineStageCount);
        if (data.get(column_position).header.getLineDealList() != null){
            String dealsCount = "";
            dealsCount = data.get(column_position).header.getLineDealList().size()+"";
            textViewPipelineHeaderCount.setText(dealsCount);
        }
        return column;
    }

    @Override
    public View createFooterView(Context context, Object footer_object, int column_position) {
        View footer = View.inflate(context, header_resource, null);
        return null;
    }

    public static class SimpleColumn{
        public List<PipeLineDeal> items;
        public PipeLineStage header;
        public SimpleColumn(PipeLineStage header, List<PipeLineDeal> items){
            this.header = header;
            this.items = items;
        }
    }

}
