package io.deloz.dhruv.deloz.Adapter;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;

import io.deloz.dhruv.deloz.R;

/**
 * Created by Dhruv on 1/11/18.
 */

public class ContactSectionViewHolder extends ViewHolder{

    TextView textViewContactHeaderStartsWith;


    public ContactSectionViewHolder(View itemView) {
        super(itemView);
        textViewContactHeaderStartsWith = (TextView) itemView.findViewById(R.id.textViewContactHeader);
    }

}
