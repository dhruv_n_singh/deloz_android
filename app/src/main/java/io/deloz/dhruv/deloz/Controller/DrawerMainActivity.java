package io.deloz.dhruv.deloz.Controller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.deloz.dhruv.deloz.Helper.API;
import io.deloz.dhruv.deloz.Helper.HttpUtility;
import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.R;
import okhttp3.Response;

public class DrawerMainActivity extends AppCompatActivity
        implements
        FragmentDashboardActivity.OnFragmentInteractionListener,
        FragmentInboxActivity.OnFragmentInteractionListener,
        FragmentLeadsActivity.OnFragmentInteractionListener,
        FragmentContactsActivity.OnFragmentInteractionListener,
        FragmentPipelineActivity.OnFragmentInteractionListener,
        FragmentInsightsActivity.OnFragmentInteractionListener,
        FragmentSettingsActivity.OnFragmentInteractionListener,
        NavigationView.OnNavigationItemSelectedListener{

    private ProgressDialog mProgressDialog;
    private static final String TAG = DrawerMainActivity.class.getName();
    private static TabLayout tabLayout;
    private ActionMode mActionMode;


    private static FragmentDashboardActivity fragmentDashboardActivity ;
    private static FragmentLeadsActivity fragmentLeadsActivity;
    private static FragmentPipelineActivity fragmentPipelineActivity;
    private static FragmentInboxActivity fragmentInboxActivity;
    private static FragmentInsightsActivity fragmentInsightsActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //NOTE:  Open fragment1 initially.
         FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        fragmentDashboardActivity = new FragmentDashboardActivity();
        ft.replace(R.id.mainFrame, fragmentDashboardActivity);
        ft.commit();

        tabLayout = findViewById(R.id.tabLayoutMainDrawer);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.d(TAG," tab selected position " + tab.getPosition());
                //NOTE: creating fragment object
                Fragment fragment = null;
                switch (tab.getPosition()){
                    case 0:
                        if (fragmentDashboardActivity == null){
                            fragmentDashboardActivity = new FragmentDashboardActivity();
                            fragment = fragmentDashboardActivity;
                        }else {
                            fragment = fragmentDashboardActivity;
                        }
                        break;
                    case 1:
                        if (fragmentLeadsActivity == null){
                            fragmentLeadsActivity = new FragmentLeadsActivity();
                            fragment = fragmentLeadsActivity;
                        }else {
                            fragment = fragmentLeadsActivity;
                        }
                        break;
                    case 2:
                        if (fragmentPipelineActivity == null){
                            fragmentPipelineActivity = new FragmentPipelineActivity();
                            fragment = fragmentPipelineActivity;
                        }else {
                            fragment = fragmentPipelineActivity;
                        }
                        break;
                    case 3:
                        if (fragmentInboxActivity == null){
                            fragmentInboxActivity = new FragmentInboxActivity();
                            fragment = fragmentInboxActivity;
                        }else {
                            fragment = fragmentInboxActivity;
                        }
                        break;
                    case 4:
                        if (fragmentInsightsActivity == null){
                            fragmentInsightsActivity = new FragmentInsightsActivity();
                            fragment = fragmentInsightsActivity;
                        }else {
                            fragment = fragmentInsightsActivity;
                        }
                        break;
                }

                //NOTE: Fragment changing code
                if (fragment != null) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.mainFrame, fragment);
                    ft.commit();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLongPressClickedToEditMessage(String title) {
         Log.d(TAG," onLongPressClickedToEditMessage ");
         mActionMode = this.startActionMode(actionModeCallbacks);
         mActionMode.setTitle(title);
        ActionBarContextView actionBar = getWindow().getDecorView().findViewById(R.id.action_mode_bar);
        actionBar.setBackgroundColor(getResources().getColor(R.color.colorInboxToolbarBackground));
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        //NOTE: creating fragment object
        Fragment fragment = null;

        int id = item.getItemId();
        if (id == R.id.nav_dashboard) {
            fragment = new FragmentDashboardActivity();
            tabLayout.setScrollPosition(0,0,true);
        } else if (id == R.id.nav_inbox) {
            fragment = new FragmentInboxActivity();
            tabLayout.setScrollPosition(3,0,true);
        } else if (id == R.id.nav_leads) {
            fragment = new FragmentLeadsActivity();
            tabLayout.setScrollPosition(1,0,true);
        } else if (id == R.id.nav_contacts) {
            fragment = new FragmentContactsActivity();
            tabLayout.setScrollPosition(1,0,true);
        } else if (id == R.id.nav_pipeline) {
            fragment = new FragmentPipelineActivity();
            tabLayout.setScrollPosition(2,0,true);
        } else if (id == R.id.nav_insights) {
            fragment = new FragmentInsightsActivity();
            tabLayout.setScrollPosition(4,0,true);
        }else if (id == R.id.nav_settings){
            fragment = new FragmentSettingsActivity();
        }else if (id == R.id.nav_logout){
            new AlertDialog.Builder(DrawerMainActivity.this)
                    .setTitle("")
                    .setMessage(R.string.logout_alert_message)
                    .setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            logoutAction();
                        }
                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            }).setIcon(android.R.drawable.ic_dialog_alert).show();

        }

        //NOTE: Fragment changing code
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.mainFrame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    Toolbar mToolbar;

    public void logoutAction(){
        if (Utility.isOnline(getApplicationContext())){
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            logout();
        }else {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.error_no_internet_connection)
                    .setMessage(R.string.error_internet_connection_message)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
        }
    }


    @Override
    public void onFragmentInteraction(String title) {
        if (!title.equalsIgnoreCase("Pipeline") && !title.equalsIgnoreCase("Leads")
                && !title.equalsIgnoreCase("Contacts") && !title.equalsIgnoreCase("Inbox")){
            getSupportActionBar().setTitle(title);

        }else{
            getSupportActionBar().setTitle("");

        }
    }

    @Override
    public void onFragmentSwitch(String selectedFragment) {
        Fragment fragment = null;

        if (selectedFragment.equalsIgnoreCase("Leads")){
            fragment = new FragmentLeadsActivity();
        }else {
            fragment = new FragmentContactsActivity();
        }

        //NOTE: Fragment changing code
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.mainFrame, fragment);
            ft.commit();
        }
    }

    private ActionMode.Callback actionModeCallbacks = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            Log.d(TAG," onCreateActionMode ");
            mode.setTitle("0 item selected");
            mode.getMenuInflater().inflate(R.menu.inbox_context_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            Log.d(TAG," onPrepareActionMode ");
            //if (multiSelect){
                menu.findItem(R.id.inbox_delete_selected_msg).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                menu.findItem(R.id.edit_inbox_msgs).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
           // }
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            Log.d(TAG," onActionItemClicked ");
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            Log.d(TAG," onDestroyActionMode ");
            mActionMode = null;

        }
    };

    public void logout(){
        String url = API.kBaseUrl + API.kLogout;
        String deviceId = Utility.getDeviceId(getApplicationContext());
        String auth_code = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(API.SharedPreferenceKeys.auth_code, "");

        try{
            JSONObject headerData = new JSONObject();
            headerData.put("Device-Key",deviceId);
            headerData.put(API.SharedPreferenceKeys.auth_code ,auth_code);
            HttpUtility.getData(url,HttpUtility.getHeaders(headerData),DrawerMainActivity.this,new HttpUtility.HttpCallback(){

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    try {
                        downloadComplete(response.body().string());
                    } catch (IOException s){
                        s.printStackTrace();
                    }

                }

            });

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void downloadComplete(String response) {
        Log.d(TAG + "logout: ",response);
        try {
            JSONObject json = new JSONObject(response);
            if (json.has("redirect_url")){
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(API.SharedPreferenceKeys.account_key, "").apply();
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(API.SharedPreferenceKeys.auth_code, "").apply();
                startActivity(new Intent(getApplicationContext(), AccountValidationActivity.class));
                finish();
            }

        }catch (JSONException e){
            e.printStackTrace();
        }

    }
}
