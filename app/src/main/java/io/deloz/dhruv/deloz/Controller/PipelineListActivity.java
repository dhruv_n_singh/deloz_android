package io.deloz.dhruv.deloz.Controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.deloz.dhruv.deloz.Adapter.PipelineListAdapter;
import io.deloz.dhruv.deloz.Helper.API;
import io.deloz.dhruv.deloz.Helper.HttpUtility;
import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.Interfaces.PipelineListSelectedListener;
import io.deloz.dhruv.deloz.Interfaces.RecycleViewPipelineListClickListener;
import io.deloz.dhruv.deloz.Modal.PipelineList;
import io.deloz.dhruv.deloz.R;
import okhttp3.Response;

public class PipelineListActivity extends AppCompatActivity {

    //UI Components
    private ProgressDialog mProgressDialog;
    private RecyclerView recycleViewPipelineList;
    private PipelineListAdapter pipelineListAdapter;
    private List<PipelineList> pipelineLists;
    private PipelineListSelectedListener mListner;


    //Variables
    private static final String TAG = PipelineListActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pipeline_list);
        setupUIComponents();
        initialSetup();
    }

    public void initialSetup(){
        if (Utility.isOnline(getApplicationContext())){
            mProgressDialog = new ProgressDialog(PipelineListActivity.this);
            mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            getPipeLineList();
        }else {
            Toast.makeText(PipelineListActivity.this, R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }

    public void setupUIComponents(){
        pipelineLists = new ArrayList<>();
        recycleViewPipelineList = findViewById(R.id.recycleViewPipelineList);
        final LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        RecycleViewPipelineListClickListener listClickListener = new RecycleViewPipelineListClickListener() {
            @Override
            public void onClickView(int position) {
                PipelineList modal = pipelineLists.get(position);
                Intent returnIntent = new Intent();
                returnIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                returnIntent.putExtra("key",modal.get_key());
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        };
        pipelineListAdapter = new PipelineListAdapter(this, pipelineLists,listClickListener);
        recycleViewPipelineList.setLayoutManager(llm);
        recycleViewPipelineList.setAdapter(pipelineListAdapter);

    }

    public void getPipeLineList(){
        final String project_id = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(API.SharedPreferenceKeys.project_id, "");
        final String accountKey = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(API.SharedPreferenceKeys.account_key, "");
        String url = API.KBaseUrlDeloz + "/" + accountKey + "/" + project_id + API.kGetPipeLineList;
        String deviceId = Utility.getDeviceId(this);
        String auth_code = PreferenceManager.getDefaultSharedPreferences(this).getString(API.SharedPreferenceKeys.auth_code, "");
        JSONObject postdata = new JSONObject();

        try {
            JSONObject headerData = new JSONObject();
            headerData.put(API.SharedPreferenceKeys.device_key,deviceId);
            headerData.put(API.SharedPreferenceKeys.auth_code,auth_code);
            HttpUtility.postData(url,postdata,HttpUtility
                    .getHeaders(headerData),PipelineListActivity.this, new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    PipelineListActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(PipelineListActivity.this)
                                    .setTitle("")
                                    .setMessage(R.string.error_unable_to_get_response)
                                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            getPipeLineList();
                                        }
                                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();
                        }
                    });

                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    try {
                        final String mMessage = response.body().string();
                        Log.d(TAG + " onSuccess: ",mMessage);
                        Log.d(TAG,mMessage);
                        if (response.isSuccessful()){
                            PipelineListActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (Utility.isRedirectFound(mMessage)){
                                        Utility.sessionLogout(mMessage,getApplicationContext(),PipelineListActivity.this);
                                    }else {
                                        try {
                                            JSONArray jsonArray = new JSONArray(mMessage);
                                            Log.d(TAG," json array "+ jsonArray.toString());
                                            parsePipelinelist(jsonArray);
                                        } catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }

                                }
                            });

                        }else {
                            Toast.makeText(PipelineListActivity.this, R.string.error_account_not_found, Toast.LENGTH_SHORT).show();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void parsePipelinelist(JSONArray jsonArrayData){

        for (int i = 0;i< jsonArrayData.length();i++){
            try {
                JSONObject jsonObjectPipeline = jsonArrayData.getJSONObject(i);
                PipelineList pipelineListModal = new PipelineList();
                if (jsonObjectPipeline.has("account_id")){
                    pipelineListModal.setAccount_id(jsonObjectPipeline.getString("account_id"));
                }
                if (jsonObjectPipeline.has("_key")){
                    pipelineListModal.set_key(jsonObjectPipeline.getString("_key"));
                }
                if (jsonObjectPipeline.has("created_at")){
                    pipelineListModal.setCreated_at(jsonObjectPipeline.getString("created_at"));
                }
                if (jsonObjectPipeline.has("description")){
                    pipelineListModal.setDescription(jsonObjectPipeline.getString("description"));

                }
                if (jsonObjectPipeline.has("updated_at")){
                    pipelineListModal.setUpdated_at("updated_at");
                }
                if (jsonObjectPipeline.has("created_by")){
                    pipelineListModal.setCreated_by(jsonObjectPipeline.getString("created_by"));
                }
                if (jsonObjectPipeline.has("pipeline_name")){
                    pipelineListModal.setPipeline_name(jsonObjectPipeline.getString("pipeline_name"));
                }
                if (jsonObjectPipeline.has("project_id")){
                    pipelineListModal.setProject_id(jsonObjectPipeline.getString("project_id"));
                }
                if (jsonObjectPipeline.has("colour")){
                    pipelineListModal.setColour(jsonObjectPipeline.getString("colour"));
                }
                if (jsonObjectPipeline.has("updated_by")){
                    pipelineListModal.setUpdated_by(jsonObjectPipeline.getString("updated_by"));
                }
                pipelineLists.add(pipelineListModal);

            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        pipelineListAdapter.notifyDataSetChanged();

    }



}
