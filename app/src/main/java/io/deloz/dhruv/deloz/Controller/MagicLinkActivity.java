package io.deloz.dhruv.deloz.Controller;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Timer;

import io.deloz.dhruv.deloz.Helper.API;
import io.deloz.dhruv.deloz.Helper.HttpUtility;
import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.R;
import okhttp3.MediaType;
import okhttp3.Response;

public class MagicLinkActivity extends AppCompatActivity {


    //VARIABLES
    private static final String TAG = AccountValidationActivity.class.getName();
    private ProgressDialog mProgressDialog;
    public static final MediaType MEDIA_TYPE =
            MediaType.parse("application/json");
    private String accountKey;
    private Timer splashTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_magic_link);
        accountKey = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(API.SharedPreferenceKeys.account_key, "");
        onNewIntent(getIntent());
    }

    public void btnActionGetMagicLink(View v){
        LayoutInflater inflater = getLayoutInflater();
       final View alertLayout = inflater.inflate(R.layout.magic_link_dialog,null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(false);
        alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText editText = (EditText) alertLayout.findViewById(R.id.edit_text_magic_link_email);
                final String email = editText.getText().toString().trim();
                if (email.isEmpty()){
                    Toast.makeText(MagicLinkActivity.this, R.string.error_enter_email, Toast.LENGTH_SHORT).show();
                }else  if (!Utility.validateEmail(email)){
                    Toast.makeText(MagicLinkActivity.this, R.string.error_invalid_email, Toast.LENGTH_SHORT).show();
                }else {
                   if (Utility.isOnline(getApplicationContext())){
                       mProgressDialog = new ProgressDialog(MagicLinkActivity.this);
                       mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
                       mProgressDialog.setCancelable(false);
                       mProgressDialog.show();
                       getMagicLink(email);
                   }else {
                       Toast.makeText(MagicLinkActivity.this, R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show();
                   }
                }
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();

    }

    public void getMagicLink(final String email){
        String baseURL = API.kBaseUrl   + API.kGenerateMagicLink;
        String deviceId = Utility.getDeviceId(getApplicationContext());
        Log.d(TAG + "url: ",baseURL);
        JSONObject postdata = new JSONObject();
        try {
            postdata.put("email", email);
            postdata.put("account_id", accountKey);
        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d(TAG + "Param:",postdata.toString());

        try {
            JSONObject headerData = new JSONObject();
            headerData.put("Device-Key",deviceId);
            Log.d(TAG+ "Header",headerData.toString());
            HttpUtility.postData(baseURL,postdata,HttpUtility.getHeaders(headerData),MagicLinkActivity.this, new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                    MagicLinkActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new android.app.AlertDialog.Builder(MagicLinkActivity.this)
                                    .setTitle("")
                                    .setMessage(R.string.error_unable_to_get_response)
                                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            getMagicLink(email);
                                        }
                                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();
                        }
                    });
                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    String mMessage = null;
                    try {
                        mMessage = response.body().string();
                        Log.d(TAG,mMessage);

                        if (response.isSuccessful()){
                            try {
                                JSONObject json = new JSONObject(mMessage);

                                if (json.has("message")){
                                   final String message = json.getString("message");
                                    MagicLinkActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(MagicLinkActivity.this, message, Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(MagicLinkActivity.this, R.string.error_account_not_found, Toast.LENGTH_SHORT).show();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    protected void onNewIntent(Intent intent) {
        String action = intent.getAction();
        String data = intent.getDataString();
        if (Intent.ACTION_VIEW.equals(action) && data != null) {
            Log.d(TAG + " deeplink: ",data);
            String verifyId = data.substring(data.lastIndexOf("/") + 1);

            if (Utility.isOnline(getApplicationContext())){
                mProgressDialog = new ProgressDialog(MagicLinkActivity.this);
                mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
                verifyMagicLink(verifyId);

            }else {
                Toast.makeText(MagicLinkActivity.this, R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void verifyMagicLink(final String verifyId){
        String baseURL = API.kBaseUrl   + API.kGenerateMagicLink;
        String deviceId = Utility.getDeviceId(getApplicationContext());
        Log.d(TAG + "url: ",baseURL);
        JSONObject postdata = new JSONObject();
        Log.d(TAG + "Param:",postdata.toString());

        try {
            JSONObject headerData = new JSONObject();
            headerData.put("Device-Key",deviceId);
            Log.d(TAG+ "Header",headerData.toString());
            HttpUtility.postData(baseURL,postdata,HttpUtility.getHeaders(headerData),MagicLinkActivity.this, new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    new android.app.AlertDialog.Builder(MagicLinkActivity.this)
                            .setTitle("")
                            .setMessage(R.string.error_unable_to_get_response)
                            .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    verifyMagicLink(verifyId);
                                }
                            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    String mMessage = null;
                    try {
                        mMessage = response.body().string();
                        Log.d(TAG,mMessage);
                        if (response.isSuccessful()){
                            try {
                                JSONObject json = new JSONObject(mMessage);
                                if (json.has("Authorization-Key")){
                                    String authorizationKey = json.getString("Authorization-Key");
                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(API.SharedPreferenceKeys.auth_code, authorizationKey).apply();
                                    Log.d(TAG,authorizationKey);
                                    String strURL = API.KBaseUrlDeloz + "/" + accountKey + API.kGetProjectList;
                                    getProjectId(strURL);

                                }
                            } catch (Exception e){
                                e.printStackTrace();
                            }

                        }else {
                            Toast.makeText(MagicLinkActivity.this, R.string.error_account_not_found, Toast.LENGTH_SHORT).show();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void getProjectId(final String url){

        try{
            String deviceId = Utility.getDeviceId(getApplicationContext());
            String auth_code = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(API.SharedPreferenceKeys.auth_code, "");
            JSONObject headerData = new JSONObject();
            headerData.put(API.SharedPreferenceKeys.device_key,deviceId);
            headerData.put(API.SharedPreferenceKeys.auth_code,auth_code);
            HttpUtility.getData(url,HttpUtility.getHeaders(headerData),MagicLinkActivity.this,new HttpUtility.HttpCallback(){

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    MagicLinkActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new android.app.AlertDialog.Builder(MagicLinkActivity.this)
                                    .setTitle("")
                                    .setMessage(R.string.error_unable_to_get_response)
                                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            getProjectId(url);
                                        }
                                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();
                        }
                    });
                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    // <-------- Do some view manipulation here
                    final String result;  // 4
                    try {
                        result = response.body().string();
                        Log.d(TAG, response.code() + " " +result);
                        if (response.isSuccessful()){
                            try{
                                JSONObject jsonResponse = new JSONObject(result);
                                if (jsonResponse.has("payload")){
                                    Log.d(TAG + " payload: ",jsonResponse.toString());
                                        JSONObject jsonPayload = jsonResponse.getJSONObject("payload");
                                        if (jsonPayload.has("list")){
                                            JSONArray jsonList = jsonPayload.getJSONArray("list");
                                            if (jsonList.length() > 0){
                                                JSONObject jsonProject  = jsonList.getJSONObject(0);
                                                if (jsonProject.has("id")){
                                                    String projectId = jsonProject.getString("id");
                                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                                                            .edit().putString(API.SharedPreferenceKeys.project_id, projectId).apply();
                                                    Intent intent = new Intent(MagicLinkActivity.this, DrawerMainActivity.class);
                                                    startActivity(intent);
                                                    killActivity();
                                                }
                                            }
                                        }
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                            }


                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            });

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void killActivity(){
        MagicLinkActivity.this.finishAffinity();
    }

}
