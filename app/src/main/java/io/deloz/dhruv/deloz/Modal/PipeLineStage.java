package io.deloz.dhruv.deloz.Modal;

import java.util.List;

/**
 * Created by Dhruv on 1/30/18.
 */

public class PipeLineStage {

    private int status;
    private String created_at;
    private String stage_name;
    private String project_id;
    private String account_id;
    private String _key;
    private String pipeline;
    private String created_by;
    private String colour;
    private String description;
    private List<PipeLineDeal> lineDealList;

    //Getter

    public int getStatus() {
        return status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getStage_name() {
        return stage_name;
    }

    public String getProject_id() {
        return project_id;
    }

    public String getAccount_id() {
        return account_id;
    }

    public String get_key() {
        return _key;
    }

    public String getPipeline() {
        return pipeline;
    }

    public String getCreated_by() {
        return created_by;
    }

    public String getColour() {
        return colour;
    }

    public String getDescription() {
        return description;
    }

    public List<PipeLineDeal> getLineDealList() {
        return lineDealList;
    }

    //Setter


    public void setStatus(int status) {
        this.status = status;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setStage_name(String stage_name) {
        this.stage_name = stage_name;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public void set_key(String _key) {
        this._key = _key;
    }

    public void setPipeline(String pipeline) {
        this.pipeline = pipeline;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLineDealList(List<PipeLineDeal> lineDealList) {
        this.lineDealList = lineDealList;
    }
}
