package io.deloz.dhruv.deloz.Controller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.deloz.dhruv.deloz.Helper.API;
import io.deloz.dhruv.deloz.Helper.HttpUtility;
import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.R;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public class DefaultLoginActivity extends AppCompatActivity {

    //Variables
    private ProgressDialog mProgressDialog;
    private static final String TAG = AccountValidationActivity.class.getName();
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 9002;
    //Components
    private TextInputLayout textInputEditTextEmail;
    private TextInputLayout textInputEditTextPassword;
    private ImageButton imageButtonShow;
    private String accountKey;
    private boolean isSelected ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default_login);
        Log.d(TAG,"onCreate");
        setupUI();
        setupGoogleSignIn();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void setupUI(){
        imageButtonShow = findViewById(R.id.imageButtonShow);
        isSelected = false;
        textInputEditTextEmail = findViewById(R.id.textInputEditTextEmail);
        textInputEditTextPassword = findViewById(R.id.textInputEditTextPassword);
        accountKey = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(API.SharedPreferenceKeys.account_key, "");
        Log.d(TAG + " Account-Key: ",accountKey);

        imageButtonShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSelected){
                    isSelected = false;
                    textInputEditTextPassword.getEditText().setTransformationMethod(PasswordTransformationMethod.getInstance());
                    imageButtonShow.setImageDrawable(getResources().getDrawable(R.mipmap.ic_visibility_off_black_24dp));
                }else {
                    isSelected = true;
                    textInputEditTextPassword.getEditText().setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    imageButtonShow.setImageDrawable(getResources().getDrawable(R.mipmap.ic_visibility_black_24dp));
                }
            }
        });
    }

    public void btnOpenMagicLink(View v) {
        Intent intent = new Intent(getApplicationContext(), MagicLinkActivity.class);
        startActivity(intent);
    }

    public void btnOpenQRCode(View v) {
        Intent intent = new Intent(getApplicationContext(), QRCodeScannerActivity.class);
        startActivity(intent);
    }

    public void btnLogin(View v){
        String email = textInputEditTextEmail.getEditText().getText().toString().trim();
        String password = textInputEditTextPassword.getEditText().getText().toString().trim();
        if (email.isEmpty()){
            Toast.makeText(DefaultLoginActivity.this, R.string.error_enter_email, Toast.LENGTH_SHORT).show();
        }else if (password.isEmpty()){
            Toast.makeText(DefaultLoginActivity.this, R.string.error_enter_password, Toast.LENGTH_SHORT).show();
        }else {
            if (Utility.isOnline(getApplicationContext())){
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
                loginToAccount(email,password);
            }else {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.error_no_internet_connection)
                        .setMessage(getString(R.string.error_internet_connection_message))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).setIcon(android.R.drawable.ic_dialog_alert).show();
            }
        }
    }

    public void setupGoogleSignIn(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(API.outh_client_id)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
    }

    private void updateUI(@Nullable GoogleSignInAccount account) {
        if (account != null) {
            Log.d(TAG + "user details: " , "Email: " + account.getEmail() + "idToken: " + account.getIdToken());
            if (Utility.isOnline(getApplicationContext())){
                loginWithGooglePlus(account.getIdToken());
            }else {
                new AlertDialog.Builder(getApplicationContext())
                        .setTitle(R.string.error_no_internet_connection)
                        .setMessage(R.string.error_internet_connection_message)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).setIcon(android.R.drawable.ic_dialog_alert).show();
            }
        }
    }

    // [START onActivityResult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }


    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            updateUI(account);
        } catch (ApiException e) {
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    public void signIn(View v) {
        signOut();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void loginToAccount(final String accountName, final String password){

        String baseURL = API.kBaseUrl + API.kDefaultLogin + "?account_name=" + accountName;
        String deviceId = Utility.getDeviceId(getApplicationContext());
        Log.d(TAG,baseURL);
        JSONObject postdata = new JSONObject();
        try {
            postdata.put("username", accountName);
            postdata.put("password", password);
            postdata.put("account_id",accountKey);
        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d(TAG,postdata.toString());

        try {
            JSONObject headerData = new JSONObject();
            headerData.put("Device-Key",deviceId);
            Log.d(TAG+ "Header",headerData.toString());
            HttpUtility.postData(baseURL,postdata,HttpUtility
                    .getHeaders(headerData),DefaultLoginActivity.this, new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                    new AlertDialog.Builder(DefaultLoginActivity.this)
                            .setTitle("")
                            .setMessage("Error: Unable to get response from server!")
                            .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    loginToAccount(accountName,password);
                                }
                            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();

                }

                @Override
                public void onSuccess(Response response) {

                    String mMessage = null;
                    try {
                        mMessage = response.body().string();
                        Log.d(TAG + " onSuccess: ",mMessage);
                        Log.d(TAG,mMessage);
                        if (response.isSuccessful()){
                            try {
                                JSONObject json = new JSONObject(mMessage);
                                if (json.has("Authorization-Key")){
                                    String authorizationKey = json.getString("Authorization-Key");
                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(API.SharedPreferenceKeys.auth_code, authorizationKey).apply();
                                    Log.d(TAG,authorizationKey);

                                    String strURL = API.KBaseUrlDeloz + "/" + accountKey + API.kGetProjectList;
                                    getProjectId(strURL);

                                }
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(DefaultLoginActivity.this, R.string.error_account_not_found, Toast.LENGTH_SHORT).show();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void getProjectId(final String url){

        try{
            String deviceId = Utility.getDeviceId(getApplicationContext());
            String auth_code = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                    .getString(API.SharedPreferenceKeys.auth_code, "");
            JSONObject headerData = new JSONObject();
            headerData.put(API.SharedPreferenceKeys.device_key,deviceId);
            headerData.put(API.SharedPreferenceKeys.auth_code,auth_code);
            HttpUtility.getData(url,HttpUtility.getHeaders(headerData),DefaultLoginActivity.this,new HttpUtility.HttpCallback(){

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                    new AlertDialog.Builder(DefaultLoginActivity.this)
                            .setTitle("")
                            .setMessage("Error: Unable to get response from server!")
                            .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    getProjectId(url);
                                }
                            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    // <-------- Do some view manipulation here
                    final String result;  // 4
                    try {
                        result = response.body().string();
                        Log.d(TAG, response.code() + " " +result);
                        if (response.isSuccessful()){
                            try{
                                JSONObject jsonResponse = new JSONObject(result);
                                if (jsonResponse.has("payload")){
                                    Log.d(TAG + " payload: ",jsonResponse.toString());
                                        JSONObject jsonPayload = jsonResponse.getJSONObject("payload");
                                        if (jsonPayload.has("list")){
                                            JSONArray jsonList = jsonPayload.getJSONArray("list");
                                            if (jsonList.length() > 0){
                                                JSONObject jsonProject  = jsonList.getJSONObject(0);
                                                if (jsonProject.has("id")){
                                                    String projectId = jsonProject.getString("id");
                                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                                                            .edit().putString(API.SharedPreferenceKeys.project_id, projectId).apply();
                                                    Intent intent = new Intent(DefaultLoginActivity.this, DrawerMainActivity.class);
                                                    startActivity(intent);
                                                    killActivity();
                                                }
                                            }
                                        }
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(DefaultLoginActivity.this, R.string.error_account_not_found, Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            });

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void killActivity(){
        DefaultLoginActivity.this.finishAffinity();

    }

    public void loginWithGooglePlus(String accessToken){
        String baseURL = API.kBaseUrl + API.kGConnect;
        String deviceId = Utility.getDeviceId(getApplicationContext());
        Log.d(TAG,baseURL);
        final OkHttpClient client = new OkHttpClient();
        JSONObject postdata = new JSONObject();
        try {
            postdata.put("id_token", accessToken);
            postdata.put("account_id",accountKey);
        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d(TAG,postdata.toString());

        try {
            JSONObject headerData = new JSONObject();
            headerData.put("Device-Key",deviceId);
            Log.d(TAG+ "Header",headerData.toString());
            HttpUtility.postData(baseURL,postdata,HttpUtility
                    .getHeaders(headerData),DefaultLoginActivity.this, new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }


                   if (response != null){
                       if (response.code() != 200){
                           DefaultLoginActivity.this.runOnUiThread(new Runnable() {
                               @Override
                               public void run() {
                                   if (mProgressDialog != null) {
                                       mProgressDialog.dismiss();
                                   }
                                   Toast.makeText(DefaultLoginActivity.this, R.string.error_account_not_found, Toast.LENGTH_SHORT).show();
                               }
                           });
                       }
                   }

                }

                @Override
                public void onSuccess(Response response) {
                    String mMessage = null;
                    try {
                        mMessage = response.body().string();
                        Log.d(TAG + " onSuccess: ",mMessage);
                        if (response.isSuccessful()){
                            try {
                                JSONObject json = new JSONObject(mMessage);
                                if (json.has("Authorization-Key")){
                                    String authorizationKey = json.getString("Authorization-Key");
                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(API.SharedPreferenceKeys.auth_code, authorizationKey).apply();
                                    Log.d(TAG,authorizationKey);

                                    String strURL = API.KBaseUrlDeloz + "/" + accountKey + API.kGetProjectList;
                                    getProjectId(strURL);

                                }
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(DefaultLoginActivity.this, R.string.error_account_not_found, Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void btnActionForgotPassword(View view) {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.magic_link_dialog,null);
        final EditText editText = (EditText) alertLayout.findViewById(R.id.edit_text_magic_link_email);
        TextView textViewTitle = (TextView) alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setText(R.string.enter_email_for_magic_link);
        android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(false);
        alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String email = editText.getText().toString().trim();
                if (email.isEmpty()){
                    Toast.makeText(DefaultLoginActivity.this, R.string.error_enter_email, Toast.LENGTH_SHORT).show();
                }else if (!Utility.validateEmail(email)){
                    Toast.makeText(DefaultLoginActivity.this, R.string.error_invalid_email, Toast.LENGTH_SHORT).show();
                } else{
                    if (Utility.isOnline(getApplicationContext())){
                        mProgressDialog = new ProgressDialog(DefaultLoginActivity.this);
                        mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
                        mProgressDialog.setCancelable(false);
                        mProgressDialog.show();
                        forgotPassword(editText.getText().toString());
                    }else {
                        Toast.makeText(DefaultLoginActivity.this, R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        android.support.v7.app.AlertDialog dialog = alert.create();
        dialog.show();
    }



    public void forgotPassword(final String email){
        String baseURL = API.kBaseUrl   + API.kForgotPassword;
        String deviceId = Utility.getDeviceId(getApplicationContext());
        Log.d(TAG + "url: ",baseURL);
        JSONObject postdata = new JSONObject();
        try {
            postdata.put("email", email);
        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d(TAG + "Param:",postdata.toString());

        try {
            JSONObject headerData = new JSONObject();
            headerData.put("Device-Key",deviceId);
            Log.d(TAG+ "Header",headerData.toString());
            HttpUtility.postData(baseURL,postdata,HttpUtility.getHeaders(headerData),DefaultLoginActivity.this, new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                    new AlertDialog.Builder(DefaultLoginActivity.this)
                            .setTitle("")
                            .setMessage("Error: Unable to get response from server!")
                            .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    forgotPassword(email);
                                }
                            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
                    DefaultLoginActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utility.hideKeyboardFrom(DefaultLoginActivity.this);
                        }
                    });
                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    String mMessage = null;
                    try {
                        mMessage = response.body().string();
                        Log.d(TAG,mMessage);
                        if (response.isSuccessful()){
                            try {
                                JSONObject json = new JSONObject(mMessage);
                                if (json.has("message")){
                                   final String message = json.getString("message");
                                    DefaultLoginActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Utility.hideKeyboardFrom(DefaultLoginActivity.this);
                                            Toast.makeText(DefaultLoginActivity.this,message, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(DefaultLoginActivity.this, R.string.error_account_not_found, Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



}
