package io.deloz.dhruv.deloz.Controller;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import io.deloz.dhruv.deloz.R;

/**
 * Created by Dhruv on 12/19/17.
 */

public class FragmentDashboardActivity extends Fragment  {

    // NOTE: Removed Some unwanted Boiler Plate Codes
    private OnFragmentInteractionListener mListener;
    public FragmentDashboardActivity() {}

    //UI Components
    Button btnQuickInsights;
    Button btnFeeds;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_dashboard, container, false);

        // NOTE : We are calling the onFragmentInteraction() declared in the MainActivity
        // ie we are sending "Fragment 1" as title parameter when fragment1 is activated
        if (mListener != null) {
            mListener.onFragmentInteraction("Dashboard");
        }
        setRetainInstance(true);

        btnQuickInsights = (Button)view.findViewById(R.id.btnQuickInsight);
        btnFeeds = (Button)view.findViewById(R.id.btnFeeds);
        btnQuickInsights.setTextColor(Color.parseColor("#A4A4A5"));
        btnFeeds.setTextColor(getResources().getColor(R.color.colorGettingStartedButton));
        insertNestedFragment(0);
        btnQuickInsights.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnFeeds.setTextColor(Color.parseColor("#A4A4A5"));
                btnQuickInsights.setTextColor(getResources().getColor(R.color.colorGettingStartedButton));
                insertNestedFragment(1);
            }
        });

        btnFeeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnQuickInsights.setTextColor(Color.parseColor("#A4A4A5"));
                btnFeeds.setTextColor(getResources().getColor(R.color.colorGettingStartedButton));
                insertNestedFragment(0);
            }
        });


        return view;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
      //  insertNestedFragment();
    }
    // Embeds the child fragment dynamically
    private void insertNestedFragment(int selectedFragment) {
        if (selectedFragment == 0){
            Fragment childFragment = new FeedsActivity();
            FragmentTransaction transaction = getChildFragmentManager()
                    .beginTransaction();
            transaction.replace(R.id.frameLayoutDashboard, childFragment)
                    .commit();
        }else {
            QuickInsightsActivity childFragment = new QuickInsightsActivity();
            FragmentTransaction transaction = getChildFragmentManager()
                    .beginTransaction();
            transaction.replace(R.id.frameLayoutDashboard, childFragment)
                    .commit();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            // NOTE: This is the part that usually gives you the error
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // NOTE : We changed the Uri to String.
        void onFragmentInteraction(String title);
        void onFragmentSwitch(String selectedFragment);
    }
}
