package io.deloz.dhruv.deloz.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import java.util.Timer;

import io.deloz.dhruv.deloz.Helper.API;
import io.deloz.dhruv.deloz.R;

public class MainActivity extends AppCompatActivity {

    private Timer splashTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hideNavigationBar();
        if (API.isLogIn(MainActivity.this)){
            startActivity(new Intent(getApplicationContext(), DrawerMainActivity.class));
            finish();
        }else {
            int TIME = 3000; //5000 ms (5 Seconds)
            final boolean b = new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    openNextScreen(); //call function!
                }
            }, TIME);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
    }

    public void hideNavigationBar(){
//        this.getWindow().getDecorView().setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    private void openNextScreen() {
        startActivity(new Intent(this, IntroductionScreenActivity.class));
    }


}
