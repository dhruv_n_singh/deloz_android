package io.deloz.dhruv.deloz.Controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.deloz.dhruv.deloz.Adapter.InboxToolAdapter;
import io.deloz.dhruv.deloz.Adapter.SharedInboxListAdapter;
import io.deloz.dhruv.deloz.Helper.API;
import io.deloz.dhruv.deloz.Helper.HttpUtility;
import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.Interfaces.RecycleViewInboxToolClickListener;
import io.deloz.dhruv.deloz.Interfaces.RecycleViewSharedInboxClickListener;
import io.deloz.dhruv.deloz.Modal.InboxTool;
import io.deloz.dhruv.deloz.Modal.SharedInboxList;
import io.deloz.dhruv.deloz.R;
import okhttp3.Response;

public class InboxMessagesToolsActivity extends AppCompatActivity {



    /**
     *UI Components
     */
    private RecyclerView recycleViewInboxTools;
    private RecyclerView recycleViewInboxSharedInbox;
    private ProgressDialog mProgressDialog;

    /**
     * Variables
     */
    static final int PICK_INBOX_REQUEST = 2;  // The request code

    List<InboxTool> inboxToolList ;
    List<SharedInboxList> sharedInboxList;
    InboxToolAdapter inboxToolAdapter;
    SharedInboxListAdapter sharedInboxListAdapter;
    private static final String TAG = InboxMessagesToolsActivity.class.getName();

    /**
     * Activity Lifecycle
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_messages_tools);
        initialSetup();
    }

    /**
     * Functions
     */

    public void initialSetup(){


        inboxToolList = new ArrayList<>();
        sharedInboxList = new ArrayList<>();

        InboxTool inboxTool = new InboxTool();
        inboxTool.setImage(R.drawable.ic_messages_inbox_tools);
        inboxTool.setTitle("All Messages");
        inboxToolList.add(inboxTool);

        inboxTool = new InboxTool();
        inboxTool.setImage(R.drawable.ic_favourite_inbox);
        inboxTool.setTitle("Favourites");
        inboxToolList.add(inboxTool);

        inboxTool = new InboxTool();
        inboxTool.setImage(R.drawable.ic_inbox_assigned_to_me);
        inboxTool.setTitle("Assigned To Me");
        inboxToolList.add(inboxTool);

        inboxTool = new InboxTool();
        inboxTool.setImage(R.drawable.ic_send_mail_inbox);
        inboxTool.setTitle("Send Mails");
        inboxToolList.add(inboxTool);

        inboxTool = new InboxTool();
        inboxTool.setImage(R.drawable.ic_draft_inbox);
        inboxTool.setTitle("Draft");
        inboxToolList.add(inboxTool);

        inboxTool = new InboxTool();
        inboxTool.setImage(R.drawable.ic_trash);
        inboxTool.setTitle("Trash");
        inboxToolList.add(inboxTool);

        recycleViewInboxTools = findViewById(R.id.recycleViewInboxTools);
        recycleViewInboxSharedInbox = findViewById(R.id.recycleViewInboxSharedInbox);

        final LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        RecycleViewInboxToolClickListener recycleViewInboxToolClickListener = new RecycleViewInboxToolClickListener() {
            @Override
            public void onClickInboxToolView(int position) {
                Log.d(TAG," clicked on position: "+ position);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("position",""+position);
                returnIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        };

        inboxToolAdapter = new InboxToolAdapter(this,inboxToolList,recycleViewInboxToolClickListener);
        recycleViewInboxTools.setLayoutManager(llm);
        recycleViewInboxTools.setAdapter(inboxToolAdapter);

        RecycleViewSharedInboxClickListener recycleViewSharedInboxClickListener = new RecycleViewSharedInboxClickListener() {
            @Override
            public void onClickSharedInbox(SharedInboxList sharedInboxList) {
                Log.d(TAG," id: " + sharedInboxList.get_key());
                Intent returnIntent = new Intent();
                returnIntent.putExtra("key",sharedInboxList.get_key());
                returnIntent.putExtra("name",sharedInboxList.getInbox_name());
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        };

        final LinearLayoutManager linearLayoutManager = new  LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        sharedInboxListAdapter = new SharedInboxListAdapter(this,sharedInboxList,recycleViewSharedInboxClickListener);
        recycleViewInboxSharedInbox.setLayoutManager(linearLayoutManager);
        recycleViewInboxSharedInbox.setAdapter(sharedInboxListAdapter);

        if (Utility.isOnline(InboxMessagesToolsActivity.this)){
            mProgressDialog = new ProgressDialog(InboxMessagesToolsActivity.this);
            mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            final String accountKey = PreferenceManager.getDefaultSharedPreferences(this)
                    .getString(API.SharedPreferenceKeys.account_key, "");
            String url =  API.KBaseUrlDeloz + "/" + accountKey + API.kGetSharedInbox;
            getSharedInboxList(url);
        }else {
            Toast.makeText(InboxMessagesToolsActivity.this, R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show();
        }


    }

    /**
     * Get SharedInboxList
     * @param url
     */

    public void getSharedInboxList(final String url){
        Log.d(TAG + " url: ",url);
        String deviceId = Utility.getDeviceId(InboxMessagesToolsActivity.this);
        String auth_code = PreferenceManager.getDefaultSharedPreferences(InboxMessagesToolsActivity.this).getString(API.SharedPreferenceKeys.auth_code, "");
        try {
            JSONObject headerData = new JSONObject();
            headerData.put(API.SharedPreferenceKeys.device_key,deviceId);
            headerData.put(API.SharedPreferenceKeys.auth_code,auth_code);
            HttpUtility.getData(url,HttpUtility.getHeaders(headerData),InboxMessagesToolsActivity.this,new HttpUtility.HttpCallback(){

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    InboxMessagesToolsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            new AlertDialog.Builder(getApplicationContext())
                                    .setTitle("")
                                    .setMessage(R.string.error_unable_to_get_response)
                                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                           getSharedInboxList(url);
                                        }
                                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();
                        }
                    });

                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                    final String result;
                    try {
                        result = response.body().string();
                        if (response.isSuccessful()){
                            InboxMessagesToolsActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (Utility.isRedirectFound(result)){
                                        Utility.sessionLogout(result,InboxMessagesToolsActivity.this,InboxMessagesToolsActivity.this);
                                    }else {
                                        try{
                                            JSONObject jsonResponse = new JSONObject(result);
                                            if (jsonResponse.has("payload")){
                                                Log.d(TAG + " payload: ",jsonResponse.toString());
                                                try {
                                                    JSONArray jsonList = jsonResponse.getJSONArray("payload");
                                                    parseSharedInboxList(jsonList);
                                                }catch (JSONException e){
                                                    e.printStackTrace();
                                                }
                                            }
                                        }catch (JSONException e){
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });

                        }else {
                            Toast.makeText(InboxMessagesToolsActivity.this, R.string.error_unable_to_get_response, Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            });
        } catch (JSONException e){
            e.printStackTrace();
        }

    }

    public void parseSharedInboxList(JSONArray jsonList){

        for (int i = 0; i < jsonList.length();i++){
            try {
                SharedInboxList modal = new SharedInboxList();
                JSONObject jsonObject = jsonList.getJSONObject(i);
                if (jsonObject.has("created_by")){
                    modal.setCreated_by(jsonObject.getString("created_by"));
                }
                if (jsonObject.has("default_from_mail")){
                    modal.setDefault_from_mail(jsonObject.getString("default_from_mail"));
                }
                if (jsonObject.has("isPrivate")){
                    Boolean isPrivate = jsonObject.getBoolean("isPrivate");
                    modal.setPrivate(isPrivate);

                }
                if (jsonObject.has("status")){
                    int status = jsonObject.getInt("status");
                    modal.setStatus(status);

                }

                if (jsonObject.has("account_id")){
                    modal.setAccount_id(jsonObject.getString("account_id"));
                }

                if (jsonObject.has("unreadlength")){
                    modal.setUnreadlength(jsonObject.getLong("unreadlength"));
                }

                if (jsonObject.has("register_for_calls")){
                    modal.setRegister_for_calls(jsonObject.getBoolean("register_for_calls"));
                }

                if (jsonObject.has("colour")){
                    modal.setColour(jsonObject.getString("colour"));
                }

                if (jsonObject.has("call_credits")){
                    modal.setCall_credits(jsonObject.getDouble("call_credits"));
                }

                if (jsonObject.has("_key")){
                    modal.set_key(jsonObject.getString("_key"));
                }

                if (jsonObject.has("created_at")){
                    modal.setCreated_at(jsonObject.getString("created_at"));
                }

                if (jsonObject.has("to_email_address")){
                    modal.setTo_email_address(jsonObject.getString("to_email_address"));
                }

                if (jsonObject.has("inbox_enabled")){
                    modal.setInbox_enabled(jsonObject.getBoolean("inbox_enabled"));
                }

                if (jsonObject.has("inbox_name")){
                    modal.setInbox_name(jsonObject.getString("inbox_name"));
                }

                if (jsonObject.has("_id")){
                    modal.set_id(jsonObject.getString("_id"));
                }

                if (jsonObject.has("inbox_type")){
                    modal.setInbox_type(jsonObject.getString("inbox_type"));
                }
                sharedInboxList.add(modal);
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
        sharedInboxListAdapter.notifyDataSetChanged();
    }



}
