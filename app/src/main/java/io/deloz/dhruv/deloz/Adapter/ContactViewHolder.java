package io.deloz.dhruv.deloz.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import io.deloz.dhruv.deloz.R;

/**
 * Created by Dhruv on 1/11/18.
 */

public class ContactViewHolder extends RecyclerView.ViewHolder {

    TextView textViewContactName;
    ImageView imgLogo;

    public ContactViewHolder(View itemView) {
        super(itemView);
        textViewContactName = (TextView) itemView.findViewById(R.id.textViewContactName);
        imgLogo = (ImageView) itemView.findViewById(R.id.imageViewContactLogo);
    }

}
