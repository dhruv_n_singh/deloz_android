package io.deloz.dhruv.deloz.Interfaces;

/**
 * Created by Dhruv on 2/14/18.
 */

public interface RecycleViewInboxToolClickListener {
    void onClickInboxToolView(int position);
}
