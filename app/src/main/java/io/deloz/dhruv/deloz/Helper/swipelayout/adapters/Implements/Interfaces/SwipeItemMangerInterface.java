package io.deloz.dhruv.deloz.Helper.swipelayout.adapters.Implements.Interfaces;

import java.util.List;

import io.deloz.dhruv.deloz.Helper.swipelayout.adapters.Attributes;
import io.deloz.dhruv.deloz.Helper.swipelayout.adapters.SwipeLayout;

/**
 * Created by Dhruv on 1/19/18.
 */


public interface SwipeItemMangerInterface {

    void openItem(int position);

    void closeItem(int position);

    void closeAllExcept(SwipeLayout layout);

    void closeAllItems();

    List<Integer> getOpenItems();

    List<SwipeLayout> getOpenLayouts();

    void removeShownLayouts(SwipeLayout layout);

    boolean isOpen(int position);

    Attributes.Mode getMode();

    void setMode(Attributes.Mode mode);
}