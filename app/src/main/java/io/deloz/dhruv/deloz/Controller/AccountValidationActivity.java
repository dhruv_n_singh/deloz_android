package io.deloz.dhruv.deloz.Controller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.deloz.dhruv.deloz.Helper.API;
import io.deloz.dhruv.deloz.Helper.HttpUtility;
import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.R;
import okhttp3.Response;

public class AccountValidationActivity extends AppCompatActivity {

    //Variables
    private ProgressDialog mProgressDialog;
    private static final String TAG = AccountValidationActivity.class.getName();

    //Components
    public static TextInputLayout textInputLayoutEditTextAccountName;
    public static Button btnContinue;
    public static Button btnHelp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_validation);
        setupUI();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    public void hideNavigationBar(){
//        this.getWindow().getDecorView().setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }


    public void verifyAccount(final String url){
        try{
            JSONObject json = new JSONObject();
            HttpUtility.getData(url,HttpUtility.getHeaders(json),AccountValidationActivity.this,new HttpUtility.HttpCallback(){

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                    new AlertDialog.Builder(AccountValidationActivity.this)
                            .setTitle("")
                            .setMessage("Error: Unable to get response from server!")
                            .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    verifyAccount(url);
                                }
                            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
                }

                @Override
                public void onSuccess(Response response) {
                    // <-------- Do some view manipulation here
                    final String result;  // 4
                    try {
                        result = response.body().string();
                        Log.d(TAG, response.code() + " " +result);
                        if (response.isSuccessful()){
                            AccountValidationActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    downloadComplete(result);
                                }
                            });
                        }else {
                            AccountValidationActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (mProgressDialog != null) {
                                        mProgressDialog.dismiss();

                                    }
                                    Toast.makeText(AccountValidationActivity.this, R.string.error_account_not_found, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            });

        } catch (Exception e){
        e.printStackTrace();
        }
    }

    public void setupUI(){
        textInputLayoutEditTextAccountName = (TextInputLayout) findViewById(R.id.textInputLayoutAccountName);
        btnContinue = (Button)findViewById(R.id.btnContinue);
        btnHelp = (Button)findViewById(R.id.btnHelp);
    }

    public void downloadComplete(String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (mProgressDialog != null) {
                mProgressDialog.hide();
            }

            if (json.has("account_key")){
                String accountKey = json.getString("account_key");
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(API.SharedPreferenceKeys.account_key, accountKey).apply();
                Intent intent = new Intent(getApplicationContext(), DefaultLoginActivity.class);
                startActivity(intent);
            }

        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    public void btnActionValidateAccount(View v) {
        String accountName = textInputLayoutEditTextAccountName.getEditText().getText().toString().trim();
         if (!accountName.isEmpty()){
             if (Utility.isOnline(getApplicationContext())){
                 mProgressDialog = new ProgressDialog(this);
                 mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
                 mProgressDialog.setCancelable(false);
                 mProgressDialog.show();
                 String url = API.kBaseUrl + API.kAccountValidation + "?account_name=" + accountName;
                 Log.d(TAG,url);
                 verifyAccount(url);
             }else {
                 new AlertDialog.Builder(this)
                         .setTitle(R.string.error_no_internet_connection)
                         .setMessage(R.string.error_internet_connection_message)
                         .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                             public void onClick(DialogInterface dialog, int which) {
                             }
                         }).setIcon(android.R.drawable.ic_dialog_alert).show();
             }
         }else {
             Toast.makeText(AccountValidationActivity.this, R.string.enter_account_details, Toast.LENGTH_SHORT).show();

         }
    }

}
