package io.deloz.dhruv.deloz.Modal;

import java.util.List;
import java.util.Date;

/**
 * Created by Dhruv on 2/5/18.
 */

public class Inbox implements Comparable<Inbox> {

    private Boolean replied;
    private String profile_pic;
    private String emailsyncsettingsl;
    private String updated_at;
    private String message;
    private String received_thread_id;
    private String subject;
    private String from;
    private String contact_color;
    private List<String> toList;
    private List<String> followersList;
    private String project_id;
    private String type;
    private int status;
    private String handle;
    private String account_id;
    private Boolean read;
    private String sharedinbox;
    private List<String> membersList;
    private String date;
    private String placeholder;
    private String _key;
    private String created_at;
    private Boolean starred;
    private String _id;
    private String colour;
    private InboxCategory inboxCategory;
    private int position;
    private Boolean draft;
    private String body;
    private Date dateTime;
    private String duration;
    private Boolean isLoadingIndicator;
    private Boolean attachments;

    /**
     * Getter
     */

    public Date getDateTime() {
        return dateTime;
    }

    public int getPosition() {
        return position;
    }

    public InboxCategory getInboxCategory() {
        return inboxCategory;
    }

    public Boolean getReplied() {
        return replied;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public String getEmailsyncsettingsl() {
        return emailsyncsettingsl;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getMessage() {
        return message;
    }

    public String getReceived_thread_id() {
        return received_thread_id;
    }

    public String getSubject() {
        return subject;
    }

    public String getFrom() {
        return from;
    }

    public String getContact_color() {
        return contact_color;
    }

    public List<String> getToList() {
        return toList;
    }

    public List<String> getFollowersList() {
        return followersList;
    }

    public String getProject_id() {
        return project_id;
    }

    public String getType() {
        return type;
    }

    public int getStatus() {
        return status;
    }

    public String getHandle() {
        return handle;
    }

    public String getAccount_id() {
        return account_id;
    }

    public Boolean getRead() {
        return read;
    }

    public String getSharedinbox() {
        return sharedinbox;
    }

    public List<String> getMembersList() {
        return membersList;
    }

    public String getDate() {
        return date;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public String get_key() {
        return _key;
    }

    public String getCreated_at() {
        return created_at;
    }

    public Boolean getStarred() {
        return starred;
    }

    public String get_id() {
        return _id;
    }

    public String getColour() {
        return colour;
    }


    public Boolean getDraft() {
        return draft;
    }

    public String getBody() {
        return body;
    }

    public String getDuration() {
        return duration;
    }

    public Boolean getLoadingIndicator() {
        return isLoadingIndicator;
    }

    public Boolean getAttachments() {
        return attachments;
    }

    /**
     * Setter
     */

    public void setInboxCategory(InboxCategory inboxCategory) {
        this.inboxCategory = inboxCategory;
    }

    public void setReplied(Boolean replied) {
        this.replied = replied;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public void setEmailsyncsettingsl(String emailsyncsettingsl) {
        this.emailsyncsettingsl = emailsyncsettingsl;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setReceived_thread_id(String received_thread_id) {
        this.received_thread_id = received_thread_id;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setContact_color(String contact_color) {
        this.contact_color = contact_color;
    }

    public void setToList(List<String> toList) {
        this.toList = toList;
    }

    public void setFollowersList(List<String> followersList) {
        this.followersList = followersList;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public void setSharedinbox(String sharedinbox) {
        this.sharedinbox = sharedinbox;
    }

    public void setMembersList(List<String> membersList) {
        this.membersList = membersList;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public void set_key(String _key) {
        this._key = _key;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setStarred(Boolean starred) {
        this.starred = starred;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setDraft(Boolean draft) {
        this.draft = draft;
    }

    public void setBody(String body) {
        this.body = body;
    }
    public void setDateTime(Date datetime) {
        this.dateTime = datetime;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setLoadingIndicator(Boolean loadingIndicator) {
        isLoadingIndicator = loadingIndicator;
    }

    public void setAttachments(Boolean attachments) {
        this.attachments = attachments;
    }

    @Override
    public int compareTo(Inbox o) {
        return getDateTime().compareTo(o.getDateTime());
    }

}
