package io.deloz.dhruv.deloz.Modal;

/**
 * Created by Dhruv on 1/24/18.
 */

public class DashboardBarChart {

    long numberOfDeals;
    double amountValue;
    String date;

    public String getDate() {
        return date;
    }

    public double getAmountValue() {
        return amountValue;
    }

    public long getNumberOfDeals() {
        return numberOfDeals;
    }

    public void setAmountValue(double amountValue) {
        this.amountValue = amountValue;
    }

    public void setNumberOfDeals(long numberOfDeals) {
        this.numberOfDeals = numberOfDeals;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
