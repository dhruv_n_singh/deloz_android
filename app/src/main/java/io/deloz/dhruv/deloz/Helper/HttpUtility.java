package io.deloz.dhruv.deloz.Helper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Dhruv on 12/18/17.
 */

public class HttpUtility {

    private static final String TAG = HttpUtility.class.getName();


    public static final MediaType MEDIA_TYPE =
            MediaType.parse("application/json");

    public static Headers getHeaders(JSONObject headerObjects){
        Headers.Builder headersBuilder = new Headers.Builder();
        headersBuilder.set("Content-Type", "application/json");
        headersBuilder.set("User-Agent","Android");
        if (headerObjects.has("Device-Key")){
            try {
                headersBuilder.set("Device-Key",headerObjects.getString("Device-Key"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (headerObjects.has("Authorization-Key")){
            try {
                headersBuilder.set("Authorization-Key",headerObjects.getString("Authorization-Key"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return headersBuilder.build();
    }

    public static void getData(String url, Headers header, @NonNull final Context mContext, final HttpCallback cb){
        OkHttpClient client = new OkHttpClient()
                .newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60,TimeUnit.SECONDS)
                .readTimeout(60,TimeUnit.SECONDS)
                .build();// 1

        final okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .headers(header)
                .build();
        Log.d(TAG + " url: ", url);
        Log.d(TAG + " headers: ",header.toString());
        client.newCall(request).enqueue(new okhttp3.Callback() { // 3

            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                if (e != null){
                    //e.printStackTrace();
                }

                cb.onFailure(null, e);
            }

            @Override
            public void onResponse(okhttp3.Call call, final okhttp3.Response response)
                    throws IOException {
                if (!response.isSuccessful()) {
                    cb.onFailure(response, null);
                    return;
                }
                cb.onSuccess(response);
            }

        });
    }


    public static void postData(String url, JSONObject postdata ,Headers header,@NonNull Context mContext, final HttpCallback cb){

        OkHttpClient client = new OkHttpClient()
                .newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60,TimeUnit.SECONDS)
                .readTimeout(60,TimeUnit.SECONDS)
                .build();
        RequestBody body = RequestBody.create(MEDIA_TYPE,
                postdata.toString());
        final Request request = new Request.Builder()
                .url(url)
                .post(body)
                .headers(header)
                .build();
        Log.d(TAG + " url: ", url);
        Log.d(TAG + " param: ",postdata.toString());
        Log.d(TAG + " headers: ",header.toString());

        client.newCall(request).enqueue(new okhttp3.Callback() { // 3
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                if (e != null){
                    e.printStackTrace();
                }
                cb.onFailure(null, e);
            }

            @Override
            public void onResponse(okhttp3.Call call, final okhttp3.Response response)
                    throws IOException {
                if (!response.isSuccessful()) {
                    cb.onFailure(response, null);
                    return;
                }
                cb.onSuccess(response);
            }

        });
    }

    public interface HttpCallback  {

        /**
         * called when the server response was not 2xx or when an exception was thrown in the process
         * @param response - in case of server error (4xx, 5xx) this contains the server response
         *                 in case of IO exception this is null
         * @param e - contains the exception. in case of server error (4xx, 5xx) this is null
         */
        public void onFailure(Response response,  IOException e);

        /**
         * contains the server response
         * @param response
         */
        public void onSuccess(Response response);
    }
}
