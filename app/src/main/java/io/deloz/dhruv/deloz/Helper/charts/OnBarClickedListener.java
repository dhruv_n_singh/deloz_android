package io.deloz.dhruv.deloz.Helper.charts;

/**
 * Created by Dhruv on 1/23/18.
 */

public interface OnBarClickedListener {
    void onBarClicked(int index);
}
