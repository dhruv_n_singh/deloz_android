package io.deloz.dhruv.deloz.Controller;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import io.deloz.dhruv.deloz.Adapter.IntroductionAdapter;
import io.deloz.dhruv.deloz.Modal.IntroductionModal;
import io.deloz.dhruv.deloz.R;

public class IntroductionScreenActivity extends AppCompatActivity {

    private Timer splashTimer;
    private RecyclerView recyclerViewIntro;
    private TextView txViewIntro1;
    private TextView txViewIntro2;
    private TextView txViewIntro3;
    private TextView txViewIntro4;
    private List<IntroductionModal> introductionList;
    private IntroductionAdapter introductionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction_screen);
        setupUI();
    }

    public void setupUI(){
      recyclerViewIntro = (RecyclerView)findViewById(R.id.recycleViewIntro);
      txViewIntro1 = (TextView)findViewById(R.id.txViewIntro1);
      txViewIntro2 = (TextView)findViewById(R.id.txViewIntro2);
      txViewIntro3 = (TextView)findViewById(R.id.txViewIntro3);
      txViewIntro4 = (TextView)findViewById(R.id.txViewIntro4);
     // setupHandler();
      setupModal();

    }

    public void setupHandler(){
        int TIME = 5000; //5000 ms (5 Seconds)
        final boolean b = new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                openNextScreen(); //call function!
            }
        }, TIME);
    }

    public void setupModal(){
        introductionList = new ArrayList<IntroductionModal>();
        introductionAdapter = new IntroductionAdapter(this, introductionList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false);
        recyclerViewIntro.setLayoutManager(mLayoutManager);
        recyclerViewIntro.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(4), true));
        recyclerViewIntro.setItemAnimator(new DefaultItemAnimator());
        recyclerViewIntro.setAdapter(introductionAdapter);

        IntroductionModal modal1 = new IntroductionModal();
        modal1.setTitle("Capture Leads and Priorities Opportunities");
        modal1.setImage(R.drawable.ic_screen_1);
        modal1.setStrDescription("Gather leads and move with system approach,know your opportunities better. ");
        introductionList.add(modal1);

        IntroductionModal modal2 = new IntroductionModal();
        modal2.setImage(R.drawable.ic_screen_2);
        modal2.setTitle("Focus on Customer");
        modal2.setStrDescription("Segregte customer to plan better.Deloz power you to identify individual opportunities,allowing to excel.");
        introductionList.add(modal2);

        IntroductionModal modal3 = new IntroductionModal();
        modal3.setImage(R.drawable.ic_screen_3);
        modal3.setTitle("Seamless Communication made Easy");
        modal3.setStrDescription("Team talk or a client call,Deloz power your communication as preffered.Call or a mail,we've got it covered.");
        introductionList.add(modal3);

        IntroductionModal modal4 = new IntroductionModal();
        modal4.setImage(R.drawable.ic_screen_4);
        modal4.setTitle("Data Driver Sales Process");
        modal4.setStrDescription("Relaunch sales strategy with data based approach and outperform. Use Deloz powered metrics to attain target effectivelly.");
        introductionList.add(modal4);

        // add pager behavior
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewIntro);

        introductionAdapter.notifyDataSetChanged();



    }

    private void openNextScreen() {
        startActivity(new Intent(this, GettingStartedActivity.class));
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position

            Log.d("Current position: "," " + position);
            switch (position){
                case 0:
                    txViewIntro1.setBackgroundColor(getResources().getColor(R.color.selectedIntro));
                    txViewIntro2.setBackgroundColor(getResources().getColor(R.color.deselectedIntro));
                    txViewIntro3.setBackgroundColor(getResources().getColor(R.color.deselectedIntro));
                    txViewIntro4.setBackgroundColor(getResources().getColor(R.color.deselectedIntro));

                    break;
                case 1:
                    txViewIntro1.setBackgroundColor(getResources().getColor(R.color.deselectedIntro));
                    txViewIntro2.setBackgroundColor(getResources().getColor(R.color.selectedIntro));
                    txViewIntro3.setBackgroundColor(getResources().getColor(R.color.deselectedIntro));
                    txViewIntro4.setBackgroundColor(getResources().getColor(R.color.deselectedIntro));
                    break;
                case  2:
                    txViewIntro1.setBackgroundColor(getResources().getColor(R.color.deselectedIntro));
                    txViewIntro2.setBackgroundColor(getResources().getColor(R.color.deselectedIntro));
                    txViewIntro3.setBackgroundColor(getResources().getColor(R.color.selectedIntro));
                    txViewIntro4.setBackgroundColor(getResources().getColor(R.color.deselectedIntro));
                    break;
                case  3:
                    txViewIntro1.setBackgroundColor(getResources().getColor(R.color.deselectedIntro));
                    txViewIntro2.setBackgroundColor(getResources().getColor(R.color.deselectedIntro));
                    txViewIntro3.setBackgroundColor(getResources().getColor(R.color.deselectedIntro));
                    txViewIntro4.setBackgroundColor(getResources().getColor(R.color.selectedIntro));
                    openNextScreen();
                    break;
                    default:
                        break;
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
