package io.deloz.dhruv.deloz.Controller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.deloz.dhruv.deloz.Adapter.LeadsAdapter;
import io.deloz.dhruv.deloz.Helper.API;
import io.deloz.dhruv.deloz.Helper.HttpUtility;
import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.Interfaces.RecyclerViewClickListener;
import io.deloz.dhruv.deloz.Modal.LeadModal;
import io.deloz.dhruv.deloz.R;
import okhttp3.Response;

/**
 * Created by Dhruv on 12/19/17.
 */

public class FragmentLeadsActivity extends Fragment {


    private FragmentDashboardActivity.OnFragmentInteractionListener mListener;

    public FragmentLeadsActivity() {}
    //UI Components
    private ProgressDialog mProgressDialog;
    private RecyclerView recyclerViewLeads;
    Toolbar mToolbar;
    Spinner mSpinner;

    //Variables
    private LeadsAdapter leadAdapter;
    public static int CURRENT_PAGE = 0;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private Color selectedColor;
    private Color deselectedColor;
    private List listLeads;
    Integer currentPageNumber;
    private static final String TAG = AccountValidationActivity.class.getName();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_leads, container, false);
        setRetainInstance(true);

        final Button btnPeople = (Button) view.findViewById(R.id.btnQuickInsight);
        final Button btnOrganization = (Button) view.findViewById(R.id.btnOrganizationContact);
        setDropDownToolBar();
        btnPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                leadAdapter.isPeople = true;
                leadAdapter.notifyDataSetChanged();
                btnOrganization.setTextColor(Color.parseColor("#A4A4A5"));
                btnPeople.setTextColor(getResources().getColor(R.color.colorGettingStartedButton));
            }
        });

        btnOrganization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                leadAdapter.isPeople = false;
                leadAdapter.notifyDataSetChanged();
                btnPeople.setTextColor(Color.parseColor("#A4A4A5"));
                btnOrganization.setTextColor(getResources().getColor(R.color.colorGettingStartedButton));
            }
        });

        if (mListener != null) {
            mListener.onFragmentInteraction("Leads");
        }
        listLeads = new ArrayList<LeadModal>();
        recyclerViewLeads = (RecyclerView)view.findViewById(R.id.recyclerViewLeads);
        RecyclerViewClickListener listener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View child, final int position, int editOption) {
                Log.d(TAG," clicked on "+ position + " edit option "+ editOption);
                final String accountKey = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.account_key, "");;
                final LeadModal modal = (LeadModal) listLeads.get(position);
                switch (editOption){
                    case 0:
                        new AlertDialog.Builder(getContext())
                                .setTitle("")
                                .setMessage(R.string.alert_title_delete_lead)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (Utility.isOnline(getContext())){
                                            mProgressDialog = new ProgressDialog(getContext());
                                            mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
                                            mProgressDialog.setCancelable(true );
                                            mProgressDialog.show();
                                            String url = API.KBaseUrlDeloz + "/" + accountKey + "/lead/" + modal.getKey() + API.kDeleteLead;
                                            deleteLead(url,position);
                                        }else {
                                            new AlertDialog.Builder(getContext())
                                                    .setTitle(R.string.error_no_internet_connection)
                                                    .setMessage(R.string.error_internet_connection_message)
                                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                        }
                                                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
                                        }
                                    }
                                })
                                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert).show();
                        break;
                    case 1:
                        if (Utility.isOnline(getContext())){
                            mProgressDialog = new ProgressDialog(getContext());
                            mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
                            mProgressDialog.setCancelable(false);
                            mProgressDialog.show();
                            String url = API.KBaseUrlDeloz + "/" + accountKey + "/" +
                                      modal.getProjectId() + "/" +  modal.getKey() + API.kConvertLeadToContact;
                            leadToContact(url,position);
                        }else {
                            new AlertDialog.Builder(getContext())
                                    .setTitle(R.string.error_no_internet_connection)
                                    .setMessage(R.string.error_internet_connection_message)
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
                        }

                        break;
                    case 2:
                        String url = API.KBaseUrlDeloz  + "/" + accountKey + "/lead/" + modal.getKey() + API.kDeleteOrArchiveLeads;
                        if (Utility.isOnline(getContext())){
                            mProgressDialog = new ProgressDialog(getContext());
                            mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
                            mProgressDialog.setCancelable(false);
                            mProgressDialog.show();
                            archiveContact(url,position);
                        }else {
                            new AlertDialog.Builder(getContext())
                                    .setTitle(R.string.error_no_internet_connection)
                                    .setMessage(R.string.error_internet_connection_message)
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
                        }
                        break;
                    default:
                        break;
                }
            }
        };
        leadAdapter = new LeadsAdapter(getContext(), listLeads,listener);
        final LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        leadAdapter.isLoadingAdded = false;
        leadAdapter.isPeople = true;
        recyclerViewLeads.setLayoutManager(llm);
        recyclerViewLeads.setAdapter(leadAdapter);
        isLoading = false;
        currentPageNumber = 0;
        recyclerViewLeads.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        Log.d(TAG,"The RecyclerView is not scrolling");
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        Log.d(TAG,"Scrolling now");
                        leadAdapter.notifyDataSetChanged();
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        Log.d(TAG,"Scroll Settling");
                        break;
                }
            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = llm.getChildCount();
                int totalItemCount = llm.getItemCount();
                int firstVisibleItemPosition = llm.findFirstVisibleItemPosition();
                if (!isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        leadAdapter.isLoadingAdded = true;
                        leadAdapter.addLoadingFooter();
                        getLeadData(true);
                    }
                }
            }
        });

        if (Utility.isOnline(getContext())){
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            getLeadData(false);
        }else {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_no_internet_connection)
                    .setMessage(R.string.error_internet_connection_message)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentDashboardActivity.OnFragmentInteractionListener) {
            mListener = (FragmentDashboardActivity.OnFragmentInteractionListener) context;
        } else {
            // NOTE: This is the part that usually gives you the error
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public void setDropDownToolBar(){
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        //you can also set the style with the constructor
        mSpinner = new Spinner(getActivity());
        mSpinner.getBackground().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);

        mSpinner.setGravity(Gravity.LEFT);
        final String[] frags = new String[]{
                "Leads",
                "Contacts",

        };
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,frags);
        mSpinner.setAdapter(arrayAdapter);
        mSpinner.setSelection(0, true);
        View v = mSpinner.getSelectedView();
        ((TextView)v).setTextSize(Utility.SPINNER_TEXT_SIZE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            ((TextView)v).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
        }
        ((TextView)v).setTextColor(getResources().getColor(R.color.colorWhite));
        mToolbar.addView(mSpinner);

        //Set the listener for when each option is clicked.
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                //Change the selected item's text color
                ((TextView) view).setTextColor(getResources().getColor(R.color.colorWhite));
                ((TextView)view).setTextSize(Utility.SPINNER_TEXT_SIZE);
                if (mListener != null) {
                    mListener.onFragmentSwitch(frags[position]);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });
    }


    public interface OnFragmentInteractionListener {
        // NOTE : We changed the Uri to String.
        void onFragmentInteraction(String title);
    }

    public void getLeadData(final Boolean isPaginated){

        isLoading = true;
        String accountKey = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.account_key, "");;
        String url = API.KBaseUrlDeloz + "/" + accountKey + API.kGetLeads + "?page=" + currentPageNumber;
        Log.d(TAG + " url: ",url);
        String deviceId = Utility.getDeviceId(getContext());
        String auth_code = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.auth_code, "");
        try {
            JSONObject headerData = new JSONObject();
            headerData.put(API.SharedPreferenceKeys.device_key,deviceId);
            headerData.put(API.SharedPreferenceKeys.auth_code,auth_code);
            HttpUtility.getData(url,HttpUtility.getHeaders(headerData),getContext(),new HttpUtility.HttpCallback(){

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    isLoading = false;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (isPaginated){
                                leadAdapter.isLoadingAdded = false;
                                leadAdapter.removeLoadingFooter();
                            }
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("")
                                    .setMessage(R.string.error_unable_to_get_response)
                                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            getLeadData(isPaginated);
                                        }
                                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();
                        }
                    });

                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (isPaginated){
                                leadAdapter.isLoadingAdded = false;
                                leadAdapter.removeLoadingFooter();
                            }
                        }
                    });

                    isLoading = false;
                    final String result;
                    try {
                        result = response.body().string();
                        if (response.isSuccessful()){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (Utility.isRedirectFound(result)){
                                        Utility.sessionLogout(result,getActivity(),getActivity());
                                    }else {
                                        try{
                                            JSONObject jsonResponse = new JSONObject(result);
                                            if (jsonResponse.has("payload")){
                                                Log.d(TAG + " payload: ",jsonResponse.toString());
                                                try {
                                                    JSONObject jsonPayload = jsonResponse.getJSONObject("payload");
                                                    if (jsonPayload.has("list")){
                                                        JSONArray jsonList = jsonPayload.getJSONArray("list");
                                                        setLeadsData(jsonList);
                                                    }

                                                }catch (JSONException e){
                                                    e.printStackTrace();
                                                }
                                            }
                                        }catch (JSONException e){
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });

                        }else {
                            Toast.makeText(getContext(), R.string.error_unable_to_get_response, Toast.LENGTH_SHORT).show();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            });
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void setLeadsData(JSONArray jsonList){
        Log.d(TAG," json: " + jsonList.toString());
        for (int i = 0; i < jsonList.length(); i++) {
            try{
                JSONObject jsonLead  = jsonList.getJSONObject(i);
                LeadModal modal = new LeadModal();
                if (jsonLead.has("leadchannel")){
                    modal.setLeadChannel(jsonLead.getString("leadchannel"));
                }
                if (jsonLead.has("status")){
                    modal.setStatus(jsonLead.getInt("status"));
                }
                if (jsonLead.has("project_id")){
                    modal.setProject_id(jsonLead.getString("project_id"));
                }
                if (jsonLead.has("account_id")){
                    modal.setLeadAccountId(jsonLead.getString("account_id"));
                }
                if (jsonLead.has("_key")){
                    modal.setKey(jsonLead.getString("_key"));
                }
                if (jsonLead.has("_rev")){
                    modal.setRev(jsonLead.getString("_rev"));
                }
                if (jsonLead.has("profile_image_thumbnail")){
                    modal.setUserImageThumbURL(jsonLead.getString("profile_image_thumbnail"));
                }

                if (jsonLead.has("profile_image")){
                    modal.setCreatedUserProfilePic(jsonLead.getString("profile_image"));
                }

                if (jsonLead.has("company")){
                    modal.setCompanyName(jsonLead.getString("company"));
                }

                if (jsonLead.has("created_by")){
                    JSONObject jsonCreatedBy = jsonLead.getJSONObject("created_by");
                    if (jsonCreatedBy.has("username")){
                        modal.setCreatedUserName(jsonCreatedBy.getString("username"));
                    }
                    if (jsonCreatedBy.has("first_name")){
                        modal.setFirstName(jsonCreatedBy.getString("first_name"));
                    }
                    if (jsonCreatedBy.has("last_name")){
                        modal.setLastName(jsonCreatedBy.getString("last_name"));
                    }
                    if (jsonCreatedBy.has("colour")){
                        modal.setCreatedByColor(jsonCreatedBy.getString("colour"));
                    }
                    if (jsonCreatedBy.has("email")){
                        modal.setCreatedEmail(jsonCreatedBy.getString("email"));
                    }
                    if (jsonCreatedBy.has("id")){
                        modal.setCreatedId(jsonCreatedBy.getString("id"));
                    }
                }

                if (jsonLead.has("email")){
                    JSONObject jsonLeadEmail = jsonLead.getJSONObject("email");
                    if (jsonLeadEmail.has("primaryemail")){
                        modal.setLeadPrimaryEmail(jsonLeadEmail.getString("primaryemail"));
                    }
                }

                if (jsonLead.has("phone")){
                    JSONObject jsonPhone = jsonLead.getJSONObject("phone");
                    if (jsonPhone.has("work")){
                        modal.setLeadWorkPhone(jsonPhone.getString("work"));
                    }
                }

                if (jsonLead.has("value")){
                    modal.setValue(jsonLead.getInt("value"));
                }

                if (jsonLead.has("created_at")){
                    modal.setCreatedTime(jsonLead.getString("created_at"));
                }

                if (jsonLead.has("_id")){
                    modal.setLeadId(jsonLead.getString("_id"));
                }
                if (jsonLead.has("contactspeople")){
                    modal.setContactsPeople(jsonLead.getString("contactspeople"));
                }
                if (jsonLead.has("colour")){
                    modal.setLeadColor(jsonLead.getString("colour"));
                }

                if (jsonLead.has("account_id")){
                    modal.setLeadAccountId(jsonLead.getString("account_id"));
                }

                if (jsonLead.has("name")){
                    JSONObject jsonName = jsonLead.getJSONObject("name");
                    if (jsonName.has("FirstName")){
                        modal.setFirstName(jsonName.getString("FirstName"));
                    }
                    if (jsonName.has("LastName")){
                        modal.setLastName(jsonName.getString("LastName"));
                    }
                }
                listLeads.add(modal);
            }catch (JSONException e){
                e.printStackTrace();
            }

        }

        if (jsonList.length() > 0){
            currentPageNumber = currentPageNumber + 1;
            Log.d(TAG ," currentPage: " + currentPageNumber);
        }else {
            currentPageNumber = currentPageNumber - 1;
            if (currentPageNumber < 0){
                currentPageNumber = 0;
            }
            Log.d(TAG ," currentPage: " + currentPageNumber);
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                leadAdapter.notifyDataSetChanged();
            }
        });


    }


    /**
     *
     * @param url ,Edit leads
     */
    public  void deleteLead(final String url , final int position){
        String deviceId = Utility.getDeviceId(getContext());
        String auth_code = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.auth_code, "");
        try {
            JSONObject headerData = new JSONObject();
            headerData.put(API.SharedPreferenceKeys.device_key,deviceId);
            headerData.put(API.SharedPreferenceKeys.auth_code,auth_code);
            HttpUtility.getData(url,HttpUtility.getHeaders(headerData),getContext(),new HttpUtility.HttpCallback(){

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("")
                                    .setMessage(R.string.error_unable_to_get_response)
                                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            deleteLead(url,position);
                                        }
                                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();
                            leadAdapter.notifyDataSetChanged();
                        }
                    });
                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    final String mMessage;
                    try {
                        mMessage = response.body().string();
                        if (response.isSuccessful()){
                            Log.d(TAG + " lead deleted ",mMessage);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (Utility.isRedirectFound(mMessage)){
                                        Utility.sessionLogout(mMessage,getActivity(),getActivity());
                                    }else {
                                        listLeads.remove(position);
                                        leadAdapter.notifyDataSetChanged();
                                        Toast.makeText(getContext(), "Lead deleted successfully!", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            });
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            });
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public  void leadToContact(final String url, final int position){
        String deviceId = Utility.getDeviceId(getContext());
        String auth_code = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.auth_code, "");
        JSONObject postdata = new JSONObject();
        try {
            JSONObject jsonObjectPayload = new JSONObject();
            postdata.put("payload",jsonObjectPayload);
        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d(TAG ,postdata.toString());

        try {
            JSONObject headerData = new JSONObject();
            headerData.put("Device-Key",deviceId);
            headerData.put("Authorization-Key",auth_code);
            Log.d(TAG+ " Header: ",headerData.toString());
            HttpUtility.postData(url,postdata,HttpUtility
                    .getHeaders(headerData),getContext(), new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("")
                                    .setMessage(R.string.error_unable_to_get_response)
                                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            leadToContact(url,position);
                                        }
                                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();
                        }
                    });

                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                  // final String mMessage = null;
                    try {
                        final String mMessage = response.body().string();
                        Log.d(TAG + " onSuccess: ",mMessage);
                        Log.d(TAG,mMessage);
                        if (response.isSuccessful()){

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (Utility.isRedirectFound(mMessage)){
                                        Utility.sessionLogout(mMessage,getActivity(),getActivity());
                                    }else {
                                        try {
                                            JSONObject json = new JSONObject(mMessage);
                                            if (json.has("payload")){
                                                JSONObject jsonPayload = json.getJSONObject("payload");
                                                if (jsonPayload.has("contactspeople")){
                                                    JSONObject contactsPeopleObject = jsonPayload.getJSONObject("contactspeople");
                                                    if (contactsPeopleObject.has("_key")){
                                                        LeadModal modal = (LeadModal) listLeads.get(position);
                                                        modal.setContactsPeople(contactsPeopleObject.getString("_key"));
                                                        listLeads.set(position,modal);
                                                        leadAdapter.notifyDataSetChanged();
                                                        Toast.makeText(getContext(), R.string.lead_to_contact, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                            }
                                        } catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                        }else {
                            Toast.makeText(getContext(), R.string.error_account_not_found, Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public  void archiveContact(final String url, final int position){
        String deviceId = Utility.getDeviceId(getContext());
        String auth_code = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.auth_code, "");
        JSONObject postdata = new JSONObject();
        try {
            JSONObject jsonObjectPayload = new JSONObject();
            jsonObjectPayload.put("status",0);

            postdata.put("payload",jsonObjectPayload);
        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d(TAG ,postdata.toString());

        try {
            JSONObject headerData = new JSONObject();
            headerData.put("Device-Key",deviceId);
            headerData.put("Authorization-Key",auth_code);
            Log.d(TAG+ " Header: ",headerData.toString());
            HttpUtility.postData(url,postdata,HttpUtility
                    .getHeaders(headerData),getContext(), new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("")
                                    .setMessage(R.string.error_unable_to_get_response)
                                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            archiveContact(url,position);
                                        }
                                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();

                        }
                    });


                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    // final String mMessage = null;
                    try {
                        final String mMessage = response.body().string();
                        Log.d(TAG + " onSuccess: ",mMessage);
                        Log.d(TAG,mMessage);
                        if (response.isSuccessful()){

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (Utility.isRedirectFound(mMessage)){
                                        Utility.sessionLogout(mMessage,getActivity(),getActivity());
                                    }else {
                                        try {
                                            JSONObject json = new JSONObject(mMessage);
                                            if (json.has("status")){
                                                    int status = json.getInt("status");
                                                    if (status == 200){
                                                        if (json.has("message")){
                                                            String message = json.getString("message");
                                                            listLeads.remove(position);
                                                            leadAdapter.notifyDataSetChanged();
                                                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                                                        }

                                                    }else{
                                                        if (json.has("message")){
                                                            String message = json.getString("message");
                                                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }


                                            }
                                        } catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }



                                }
                            });
                        }else {
                            Toast.makeText(getContext(), R.string.error_account_not_found, Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mToolbar != null && mSpinner != null) {
            mToolbar.removeView(mSpinner);
        }
    }

}
