package io.deloz.dhruv.deloz.Interfaces;

import io.deloz.dhruv.deloz.Modal.PipelineList;

/**
 * Created by Dhruv on 1/30/18.
 */

public interface PipelineListSelectedListener {

    void selectedPipelineList(PipelineList modal);
}
