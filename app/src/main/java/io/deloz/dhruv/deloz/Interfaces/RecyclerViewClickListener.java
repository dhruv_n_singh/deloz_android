package io.deloz.dhruv.deloz.Interfaces;

import android.view.View;

/**
 * Created by Dhruv on 1/19/18.
 */

public interface RecyclerViewClickListener {
    void onClick(View child, int position,int editOption);
}


