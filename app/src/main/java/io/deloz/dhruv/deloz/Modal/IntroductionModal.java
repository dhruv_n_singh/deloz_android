package io.deloz.dhruv.deloz.Modal;

/**
 * Created by Dhruv on 12/26/17.
 */

public class IntroductionModal {

    private String title;
    private int image;
    private String strDescription;

    public String getTitle() {
        return title;
    }

    public int getImage() {
        return image;
    }

    public String getStrDescription() {
        return strDescription;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public void setImage(int image) {
        this.image = image;
    }

    public void setStrDescription(String strDescription) {
        this.strDescription = strDescription;
    }
}
