package io.deloz.dhruv.deloz.Modal;

/**
 * Created by Dhruv on 1/30/18.
 */

public class PipeLineDeal {

    private String updated_at;
    private String account_id;
    private String leadsKey;
    private String leadsFirstName;
    private String leadsLastName;
    private String contactsorganisationKey;
    private String contactsorganisationPrimaryPhone;
    private String contactsorganisationWorkPhone;
    private String contactsorganisationName;
    private String contactsorganisationEmail;
    private String description;
    private String createdByUsername;
    private String createdByFirstName;
    private String createdByLastName;
    private String createdByColor;
    private String createdById;
    private String createdByProfilePicture;
    private String createdByEmail;
    private String state;
    private String project_id;
    private int    status;
    private String due_date;
    private String productKey;
    private String produtCreatedAt;
    private String productName;
    private String updated_by;
    private String deallossreasonKey;
    private String deallossreasonDescription;
    private String pipelineKey;
    private String pipelineName;
    private String id;
    private String name;
    private String _key;
    private String created_at;
    private long value;
    private String assignedToUsername;
    private String assignedToFirstName;
    private String assignedToLastName;
    private String assignedToColor;
    private String assignedToId;
    private String assignedToProfilePicture;
    private String assignedToEmail;
    private String stageKey;
    private String stageName;
    private String contactspeopleKey;
    private String contactspeopleWorkPhone;
    private String contactspeoplePrimaryPhone;
    private String contactsPeopleFirstName;
    private String contactspeopleMiddleName;
    private String contactsPeopleLastName;
    private String primaryemail;
    private String color;


    //Getter

    public String getUpdated_at() {
        return updated_at;
    }

    public String getAccount_id() {
        return account_id;
    }

    public String getLeadsKey() {
        return leadsKey;
    }

    public String getLeadsFirstName() {
        return leadsFirstName;
    }

    public String getLeadsLastName() {
        return leadsLastName;
    }

    public String getContactsorganisationKey() {
        return contactsorganisationKey;
    }

    public String getContactsorganisationPrimaryPhone() {
        return contactsorganisationPrimaryPhone;
    }

    public String getContactsorganisationWorkPhone() {
        return contactsorganisationWorkPhone;
    }

    public String getContactsorganisationName() {
        return contactsorganisationName;
    }

    public String getContactsorganisationEmail() {
        return contactsorganisationEmail;
    }

    public String getDescription() {
        return description;
    }

    public String getCreatedByUsername() {
        return createdByUsername;
    }

    public String getCreatedByFirstName() {
        return createdByFirstName;
    }

    public String getCreatedByLastName() {
        return createdByLastName;
    }

    public String getCreatedByColor() {
        return createdByColor;
    }

    public String getCreatedById() {
        return createdById;
    }

    public String getCreatedByProfilePicture() {
        return createdByProfilePicture;
    }

    public String getCreatedByEmail() {
        return createdByEmail;
    }

    public String getState() {
        return state;
    }

    public String getProject_id() {
        return project_id;
    }

    public int getStatus() {
        return status;
    }

    public String getDue_date() {
        return due_date;
    }

    public String getProductKey() {
        return productKey;
    }

    public String getProdutCreatedAt() {
        return produtCreatedAt;
    }

    public String getProductName() {
        return productName;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public String getDeallossreasonKey() {
        return deallossreasonKey;
    }

    public String getDeallossreasonDescription() {
        return deallossreasonDescription;
    }

    public String getPipelineKey() {
        return pipelineKey;
    }

    public String getPipelineName() {
        return pipelineName;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String get_key() {
        return _key;
    }

    public String getCreated_at() {
        return created_at;
    }

    public long getValue() {
        return value;
    }

    public String getAssignedToUsername() {
        return assignedToUsername;
    }

    public String getAssignedToFirstName() {
        return assignedToFirstName;
    }

    public String getAssignedToLastName() {
        return assignedToLastName;
    }

    public String getAssignedToColor() {
        return assignedToColor;
    }

    public String getAssignedToId() {
        return assignedToId;
    }

    public String getAssignedToProfilePicture() {
        return assignedToProfilePicture;
    }

    public String getAssignedToEmail() {
        return assignedToEmail;
    }

    public String getStageKey() {
        return stageKey;
    }

    public String getStageName() {
        return stageName;
    }

    public String getContactspeopleKey() {
        return contactspeopleKey;
    }

    public String getContactsPeopleFirstName() {
        return contactsPeopleFirstName;
    }

    public String getContactsPeopleLastName() {
        return contactsPeopleLastName;
    }

    public String getContactspeopleMiddleName() {
        return contactspeopleMiddleName;
    }

    public String getContactspeoplePrimaryPhone() {
        return contactspeoplePrimaryPhone;
    }

    public String getContactspeopleWorkPhone() {
        return contactspeopleWorkPhone;
    }

    public String getColor() {
        return color;
    }

    public String getPrimaryemail() {
        return primaryemail;
    }
    //Setter


    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public void setLeadsKey(String leadsKey) {
        this.leadsKey = leadsKey;
    }

    public void setLeadsFirstName(String leadsFirstName) {
        this.leadsFirstName = leadsFirstName;
    }

    public void setLeadsLastName(String leadsLastName) {
        this.leadsLastName = leadsLastName;
    }

    public void setContactsorganisationKey(String contactsorganisationKey) {
        this.contactsorganisationKey = contactsorganisationKey;
    }

    public void setContactsorganisationPrimaryPhone(String contactsorganisationPrimaryPhone) {
        this.contactsorganisationPrimaryPhone = contactsorganisationPrimaryPhone;
    }

    public void setContactsorganisationWorkPhone(String contactsorganisationWorkPhone) {
        this.contactsorganisationWorkPhone = contactsorganisationWorkPhone;
    }

    public void setContactsorganisationEmail(String contactsorganisationEmail) {
        this.contactsorganisationEmail = contactsorganisationEmail;
    }

    public void setContactsorganisationName(String contactsorganisationName) {
        this.contactsorganisationName = contactsorganisationName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreatedByUsername(String createdByUsername) {
        this.createdByUsername = createdByUsername;
    }

    public void setCreatedByFirstName(String createdByFirstName) {
        this.createdByFirstName = createdByFirstName;
    }

    public void setCreatedByLastName(String createdByLastName) {
        this.createdByLastName = createdByLastName;
    }

    public void setCreatedByColor(String createdByColor) {
        this.createdByColor = createdByColor;
    }

    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }

    public void setCreatedByEmail(String createdByEmail) {
        this.createdByEmail = createdByEmail;
    }

    public void setCreatedByProfilePicture(String createdByProfilePicture) {
        this.createdByProfilePicture = createdByProfilePicture;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    public void setProductKey(String productKey) {
        this.productKey = productKey;
    }

    public void setProdutCreatedAt(String produtCreatedAt) {
        this.produtCreatedAt = produtCreatedAt;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public void setDeallossreasonKey(String deallossreasonKey) {
        this.deallossreasonKey = deallossreasonKey;
    }

    public void setDeallossreasonDescription(String deallossreasonDescription) {
        this.deallossreasonDescription = deallossreasonDescription;
    }

    public void setPipelineKey(String pipelineKey) {
        this.pipelineKey = pipelineKey;
    }

    public void setPipelineName(String pipelineName) {
        this.pipelineName = pipelineName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void set_key(String _key) {
        this._key = _key;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public void setAssignedToUsername(String assignedToUsername) {
        this.assignedToUsername = assignedToUsername;
    }

    public void setAssignedToFirstName(String assignedToFirstName) {
        this.assignedToFirstName = assignedToFirstName;
    }

    public void setAssignedToLastName(String assignedToLastName) {
        this.assignedToLastName = assignedToLastName;
    }

    public void setAssignedToColor(String assignedToColor) {
        this.assignedToColor = assignedToColor;
    }

    public void setAssignedToId(String assignedToId) {
        this.assignedToId = assignedToId;
    }

    public void setAssignedToProfilePicture(String assignedToProfilePicture) {
        this.assignedToProfilePicture = assignedToProfilePicture;
    }

    public void setAssignedToEmail(String assignedToEmail) {
        this.assignedToEmail = assignedToEmail;
    }

    public void setStageKey(String stageKey) {
        this.stageKey = stageKey;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public void setContactspeopleKey(String contactspeopleKey) {
        this.contactspeopleKey = contactspeopleKey;
    }

    public void setContactsPeopleFirstName(String contactsPeopleFirstName) {
        this.contactsPeopleFirstName = contactsPeopleFirstName;
    }

    public void setContactspeopleMiddleName(String contactspeopleMiddleName) {
        this.contactspeopleMiddleName = contactspeopleMiddleName;
    }

    public void setContactsPeopleLastName(String contactsPeopleLastName) {
        this.contactsPeopleLastName = contactsPeopleLastName;
    }

    public void setContactspeoplePrimaryPhone(String contactspeoplePrimaryPhone) {
        this.contactspeoplePrimaryPhone = contactspeoplePrimaryPhone;
    }

    public void setContactspeopleWorkPhone(String contactspeopleWorkPhone) {
        this.contactspeopleWorkPhone = contactspeopleWorkPhone;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPrimaryemail(String primaryemail) {
        this.primaryemail = primaryemail;
    }
}
