package io.deloz.dhruv.deloz.Modal;

/**
 * Created by Dhruv on 2/14/18.
 */

public class InboxTool {

    String title;
    int image;

    //Getter

    public String getTitle() {
        return title;
    }

    public int getImage() {
        return image;
    }

    //Setter


    public void setImage(int image) {
        this.image = image;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
