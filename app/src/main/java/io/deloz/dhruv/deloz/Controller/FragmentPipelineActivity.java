package io.deloz.dhruv.deloz.Controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.deloz.dhruv.deloz.Helper.API;
import io.deloz.dhruv.deloz.Helper.HttpUtility;
import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.Helper.boardView.BoardView;
import io.deloz.dhruv.deloz.Helper.boardView.SimpleBoardAdapter;
import io.deloz.dhruv.deloz.Modal.PipeLineDeal;
import io.deloz.dhruv.deloz.Modal.PipeLineStage;
import io.deloz.dhruv.deloz.Modal.PipelineList;
import io.deloz.dhruv.deloz.R;
import okhttp3.Response;

/**
 * Created by Dhruv on 12/19/17.
 */

public class FragmentPipelineActivity extends Fragment {

    private FragmentDashboardActivity.OnFragmentInteractionListener mListener;
    public FragmentPipelineActivity() { }

    /**
     * UI Components
     */
    private ProgressDialog mProgressDialog;
    Toolbar mToolbar;
    Spinner mSpinner;
    BoardView boardView;
    /**
     * Variables
     */
    private Handler h;
    List<String> listPipelineListSpinner;
    ArrayAdapter<String> arrayAdapter;

    private static final String TAG = FragmentPipelineActivity.class.getName();
    String project_id ;
    String accountKey;
    String deviceId ;
    String auth_code ;
    List<List<PipeLineStage>> listListCurrentPipeLineDealsData;
    int CURRENT_STATE = 1;
    private List<PipelineList> pipelineLists;
    PipeLineDeal currentDeal;
    int pickedDealIndex  = 0;
    int pickedStageIndex = 0;
    int droppedDealIndex = 0;
    int droppedStageIndex = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pipeline, container, false);
        setRetainInstance(true);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Log.d(TAG," width: "+ width + "height: "+ height);
        boardView = (BoardView)view.findViewById(R.id.boardView);
        project_id = PreferenceManager.getDefaultSharedPreferences(getContext())
                .getString(API.SharedPreferenceKeys.project_id, "");
        accountKey = PreferenceManager.getDefaultSharedPreferences(getContext())
                .getString(API.SharedPreferenceKeys.account_key, "");
        deviceId = Utility.getDeviceId(getContext());
        auth_code = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.auth_code, "");
        if (mListener != null) {
            mListener.onFragmentInteraction("Pipeline");
        }
        setupSpinner();
        listListCurrentPipeLineDealsData = new ArrayList<>();
        currentDeal = new PipeLineDeal();
        setupBoardView();
        if (Utility.isOnline(getContext())){
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            getPipeLineList();
        }else {
            Toast.makeText(getContext(), R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentDashboardActivity.OnFragmentInteractionListener) {
            mListener = (FragmentDashboardActivity.OnFragmentInteractionListener) context;
        } else {
            // NOTE: This is the part that usually gives you the error
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mToolbar != null && mSpinner != null) {
            mToolbar.removeView(mSpinner);
        }
    }

    public void setupWindowScreenSize(){

    }

    public void setupSpinner(){
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mSpinner = new Spinner(getActivity());
        mSpinner.getBackground().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        mSpinner.setGravity(Gravity.LEFT);
        listPipelineListSpinner = new ArrayList<>();
        listPipelineListSpinner.add("Pipeline");
        arrayAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, listPipelineListSpinner);
        mSpinner.setAdapter(arrayAdapter);
        mSpinner.setSelection(0, true);
        View v = mSpinner.getSelectedView();
        TextView SpinnerText = (TextView)v;
        if (SpinnerText == null) {
            Log.d(TAG,"Not found");
        } else {
            SpinnerText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        mSpinner.cancelPendingInputEvents();
                    }
                    Log.d(TAG,"Set onClick SpinnerText");
                    Intent intent = new Intent(getActivity(), PipelineListActivity.class);
                    startActivityForResult(intent, Utility.RequestResultConst.PICK_PIPELINE_REQUEST);
                }
            });
        }

        if (listPipelineListSpinner.size() > 0){
            ((TextView)v).setTextSize(Utility.SPINNER_TEXT_SIZE);
            ((TextView)v).setText("Pipeline");
            ((TextView)v).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            ((TextView)v).setTextColor(getResources().getColor(R.color.colorWhite));
        }

        mToolbar.addView(mSpinner);



        //Set the listener for when each option is clicked.
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Change the selected item's text color
                ((TextView) view).setTextColor(getResources().getColor(R.color.colorWhite));
                ((TextView)view).setTextSize(Utility.SPINNER_TEXT_SIZE);
                ((TextView)view).setText("Pipeline");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utility.RequestResultConst.PICK_PIPELINE_REQUEST) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("key");
                Log.d(TAG,"Wow getting key: " + result );
                getPipeLineStages(result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    public interface OnFragmentInteractionListener {
        // NOTE : We changed the Uri to String.
        void onFragmentInteraction(String title);
    }

    /**
     * Get pipeline stages using Pipeline key from Pipline List
     */
    public void getPipeLineStages(String pipelineKey){
        if (Utility.isOnline(getContext())){
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            String url = API.KBaseUrlDeloz + "/" + accountKey + "/" + project_id + API.kStageList;
            JSONObject postdata = new JSONObject();
            try {
                postdata.put("pipeline",pipelineKey);
            }catch (JSONException e){
                e.printStackTrace();
            }
            getStages(url,postdata,pipelineKey);
        }else {
            Toast.makeText(getContext(), R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show();
        }


    }

    public void getStages(final String url , final JSONObject jsonObjectPostData, final String pipelineKey){
        try {
            JSONObject headerData = new JSONObject();
            headerData.put(API.SharedPreferenceKeys.device_key,deviceId);
            headerData.put(API.SharedPreferenceKeys.auth_code,auth_code);
            HttpUtility.postData(url,jsonObjectPostData,HttpUtility
                    .getHeaders(headerData),getContext(), new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("")
                                    .setMessage(R.string.error_unable_to_get_response)
                                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            getStages(url,jsonObjectPostData,pipelineKey);
                                        }
                                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();
                        }
                    });



                }

                @Override
                public void onSuccess(Response response) {

                    try {
                        final String mMessage = response.body().string();
                        Log.d(TAG + " onSuccess: ",mMessage);
                        Log.d(TAG,mMessage);
                        if (response.isSuccessful()){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (Utility.isRedirectFound(mMessage)){
                                        Utility.sessionLogout(mMessage,getContext(),getActivity());
                                    }else {
                                        try {
                                            JSONObject jsonObjectDataStages = new JSONObject(mMessage);
                                            if (jsonObjectDataStages.has("payload")){
                                                JSONArray jsonArrayStages = jsonObjectDataStages.getJSONArray("payload");
                                                Log.d(TAG," stages: "+jsonArrayStages.toString());
//                                                List<PipeLineStage> stageList = parseStageList(jsonArrayStages);
                                                getPipeLineDetails(jsonArrayStages,pipelineKey);
                                            }
                                        }catch (JSONException e){
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });

                        }else {
                            Toast.makeText(getContext(), R.string.error_account_not_found, Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    /**
     * Parse stage list
     */
    public List<PipeLineStage> parseStageList(JSONArray jsonArrayStageData){
        List<PipeLineStage> pipeLineStageList = new ArrayList<>();
        for (int i = 0; i < jsonArrayStageData.length();i++){
            try{
                JSONObject jsonObjectStage = jsonArrayStageData.getJSONObject(i);
                PipeLineStage stageModal = new PipeLineStage();
                if (jsonObjectStage.has("created_at")){
                    String createdAt = jsonObjectStage.getString("created_at");
                    stageModal.setCreated_at(createdAt);
                }

                if (jsonObjectStage.has("stage_name")){
                    String stageName = jsonObjectStage.getString("stage_name");
                    stageModal.setStage_name(stageName);
                }

                if (jsonObjectStage.has("project_id")){
                    String project_id = jsonObjectStage.getString("project_id");
                    stageModal.setProject_id(project_id);
                }

                if (jsonObjectStage.has("account_id")){
                    String account_id = jsonObjectStage.getString("account_id");
                    stageModal.setAccount_id(account_id);
                }

                if (jsonObjectStage.has("_key")){
                    String _key = jsonObjectStage.getString("_key");
                    stageModal.set_key(_key);
                }

                if (jsonObjectStage.has("pipeline")){
                    String pipeline = jsonObjectStage.getString("pipeline");
                    stageModal.setPipeline(pipeline);
                }

                if (jsonObjectStage.has("created_by")){
                    String created_by = jsonObjectStage.getString("created_by");
                    stageModal.setCreated_by(created_by);
                }

                if (jsonObjectStage.has("colour")){
                    String color = jsonObjectStage.getString("colour");
                    stageModal.setColour(color);
                }

                if (jsonObjectStage.has("description")){
                    String description = jsonObjectStage.getString("description");
                    stageModal.setDescription(description);
                }

                List<PipeLineDeal> pipeLineDealList = new ArrayList<>();
                stageModal.setLineDealList(pipeLineDealList);

                pipeLineStageList.add(stageModal);
            } catch (JSONException e){
                e.printStackTrace();
            }
        }
        return pipeLineStageList;
    }

    /**
     * Get Pipeline deals
     */

    public void getPipeLineDetails(JSONArray jsonArrayStage,String pipelineKey){
        if (Utility.isOnline(getContext())){
            String url = API.KBaseUrlDeloz + "/" + accountKey + "/" + project_id + API.kGetPipeLineDeals;
            JSONObject postdata = new JSONObject();
            try {
                postdata.put("pipeline",pipelineKey);
            }catch (JSONException e){
                e.printStackTrace();
            }
            getPipeLineDeals(url,postdata,jsonArrayStage);

        }else {
            Toast.makeText(getContext(), R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show();
        }

    }

    public void getPipeLineDeals(final String url , final JSONObject jsonObjectPostData, final JSONArray jsonArrayStages){
        try {
            JSONObject headerData = new JSONObject();
            headerData.put(API.SharedPreferenceKeys.device_key,deviceId);
            headerData.put(API.SharedPreferenceKeys.auth_code,auth_code);
            HttpUtility.postData(url,jsonObjectPostData,HttpUtility
                    .getHeaders(headerData),getContext(), new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    new AlertDialog.Builder(getActivity())
                            .setTitle("")
                            .setMessage(R.string.error_unable_to_get_response)
                            .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    getPipeLineDeals(url,jsonObjectPostData,jsonArrayStages);
                                }
                            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();

                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    try {
                        final String mMessage = response.body().string();
                        Log.d(TAG + " onSuccess: ",mMessage);
                        Log.d(TAG,mMessage);
                        if (response.isSuccessful()){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (Utility.isRedirectFound(mMessage)){
                                        Utility.sessionLogout(mMessage,getContext(),getActivity());
                                    }else {
                                        try {
                                            JSONObject jsonObjectDataDeals = new JSONObject(mMessage);
                                            if (jsonObjectDataDeals.has("payload")){
                                                JSONArray jsonArrayDeals = jsonObjectDataDeals.getJSONArray("payload");
                                                Log.d(TAG," deals: "+jsonArrayDeals.toString());
                                                listListCurrentPipeLineDealsData = parsingDealsData(jsonArrayDeals,jsonArrayStages);
                                                Log.d(TAG," pipeline: "+listListCurrentPipeLineDealsData.size());
                                                setupBoardView();
                                            }
                                        }catch (JSONException e){
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });

                        }else {
                            Toast.makeText(getContext(), R.string.error_account_not_found, Toast.LENGTH_SHORT).show();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    /**
     * Parse Pipeline deals
     */
    public List<List<PipeLineStage>> parsingDealsData(JSONArray jsonArrayDeals, JSONArray jsonArrayStages){
        /**
         * Prepare List for Won,InProgress,Lost,Archive,Trash
         */
        List<List<PipeLineStage>> listListPipelineDeals = new ArrayList<>();
        for (int i = 0;i< 5;i++){
            List<PipeLineStage> pipeLineStageList = parseStageList(jsonArrayStages);
            listListPipelineDeals.add(pipeLineStageList);
        }

        /**
         * Parse deals and arrange them according to states in listListPipelineDeals
         */

        for (int i = 0;i< jsonArrayDeals.length();i++){
            try{
                PipeLineDeal pipeLineDeal = new PipeLineDeal();
                JSONObject jsonObjectDeal = jsonArrayDeals.getJSONObject(i);
                if (jsonObjectDeal.has("updated_at")){
                    pipeLineDeal.setUpdated_at(jsonObjectDeal.getString("updated_at"));
                }
                if (jsonObjectDeal.has("updated_by")){
                    pipeLineDeal.setUpdated_by(jsonObjectDeal.getString("updated_by"));
                }
                if (jsonObjectDeal.has("created_by")){
                        JSONObject jsonObjectCreatedBy = jsonObjectDeal.getJSONObject("created_by");
                        if (jsonObjectCreatedBy.has("username")){
                            pipeLineDeal.setCreatedByUsername(jsonObjectCreatedBy.getString("username"));
                        }
                        if (jsonObjectCreatedBy.has("first_name")){
                            pipeLineDeal.setCreatedByFirstName(jsonObjectCreatedBy.getString("first_name"));
                        }
                        if (jsonObjectCreatedBy.has("last_name")){
                            pipeLineDeal.setCreatedByLastName(jsonObjectCreatedBy.getString("last_name"));
                        }
                        if (jsonObjectCreatedBy.has("colour")){
                            pipeLineDeal.setCreatedByColor(jsonObjectCreatedBy.getString("colour"));
                        }
                        if (jsonObjectCreatedBy.has("id")){
                            pipeLineDeal.setId(jsonObjectCreatedBy.getString("id"));
                        }
                        if (jsonObjectCreatedBy.has("profile_picture")){
                            pipeLineDeal.setCreatedByProfilePicture(jsonObjectCreatedBy.getString("profile_picture"));
                        }
                        if (jsonObjectCreatedBy.has("email")){
                            pipeLineDeal.setCreatedByEmail(jsonObjectCreatedBy.getString("email"));
                        }

                }

                if (jsonObjectDeal.has("state")){
                    pipeLineDeal.setState(jsonObjectDeal.getString("state"));
                }
                if (jsonObjectDeal.has("project_id")){
                    pipeLineDeal.setProject_id(jsonObjectDeal.getString("project_id"));
                }
                if (jsonObjectDeal.has("status")){
                    int status = jsonObjectDeal.getInt("status");
                    pipeLineDeal.setStatus(status);
                }

                if (jsonObjectDeal.has("due_date")){
                    pipeLineDeal.setDue_date(jsonObjectDeal.getString("due_date"));
                }
                if (jsonObjectDeal.has("product")){
                    JSONObject jsonObjectProduct = jsonObjectDeal.getJSONObject("product");
                        if (jsonObjectProduct.has("_key")){
                            pipeLineDeal.setProductKey(jsonObjectProduct.getString("_key"));
                        }
                        if (jsonObjectProduct.has("created_at")){
                            pipeLineDeal.setCreated_at(jsonObjectProduct.getString("created_at"));
                        }
                        if (jsonObjectProduct.has("name")){
                            pipeLineDeal.setProductName(jsonObjectProduct.getString("name"));
                        }
                }

                if (jsonObjectDeal.has("account_id")){
                    pipeLineDeal.setAccount_id(jsonObjectDeal.getString("account_id"));
                }
                if (jsonObjectDeal.has("pipeline")){
                        JSONObject jsonObjectPipeline = jsonObjectDeal.getJSONObject("pipeline");
                        if (jsonObjectPipeline.has("_key")){
                            pipeLineDeal.setPipelineKey(jsonObjectPipeline.getString("_key"));
                        }
                        if (jsonObjectPipeline.has("pipeline_name")){
                            pipeLineDeal.setPipelineName(jsonObjectPipeline.getString("pipeline_name"));
                        }
                }

                if (jsonObjectDeal.has("_id")){
                    pipeLineDeal.setId(jsonObjectDeal.getString("_id"));
                }
                if (jsonObjectDeal.has("name")){
                    pipeLineDeal.setName(jsonObjectDeal.getString("name"));
                }

                if (jsonObjectDeal.has("_key")){
                    pipeLineDeal.set_key(jsonObjectDeal.getString("_key"));
                }
                if (jsonObjectDeal.has("created_at")){
                    pipeLineDeal.setCreated_at(jsonObjectDeal.getString("created_at"));
                }
                if (jsonObjectDeal.has("value")){
                    long value  = jsonObjectDeal.getLong("value");
                    pipeLineDeal.setValue(value);
                }

                if (jsonObjectDeal.has("assigned_to")){
                        JSONObject jsonObjectAssigned = jsonObjectDeal.getJSONObject("assigned_to");
                        if (jsonObjectAssigned.has("username")){
                            pipeLineDeal.setAssignedToUsername(jsonObjectAssigned.getString("username"));
                        }
                        if (jsonObjectAssigned.has("first_name")){
                            pipeLineDeal.setAssignedToFirstName(jsonObjectAssigned.getString("first_name"));
                        }
                        if (jsonObjectAssigned.has("last_name")){
                            pipeLineDeal.setAssignedToLastName(jsonObjectAssigned.getString("last_name"));
                        }
                        if (jsonObjectAssigned.has("colour")){
                            pipeLineDeal.setAssignedToColor(jsonObjectAssigned.getString("colour"));
                        }
                        if (jsonObjectAssigned.has("id")){
                            pipeLineDeal.setAssignedToId(jsonObjectAssigned.getString("id"));
                        }
                        if (jsonObjectAssigned.has("profile_picture")){
                            pipeLineDeal.setAssignedToProfilePicture(jsonObjectAssigned.getString("profile_picture"));
                        }
                        if (jsonObjectAssigned.has("email")){
                            pipeLineDeal.setAssignedToEmail(jsonObjectAssigned.getString("email"));
                        }

                }

                if (jsonObjectDeal.has("stages")){
                        JSONObject jsonObjectStages = jsonObjectDeal.getJSONObject("stages");
                        if (jsonObjectStages.has("_key")){
                            pipeLineDeal.setStageKey(jsonObjectStages.getString("_key"));
                        }
                        if (jsonObjectStages.has("stage_name")){
                            pipeLineDeal.setStageName(jsonObjectStages.getString("stage_name"));
                        }

                }

                if (jsonObjectDeal.has("contactspeople")){
                        JSONObject jsonObjectContactsPeople = jsonObjectDeal.getJSONObject("contactspeople");
                        if (jsonObjectContactsPeople.has("_key")){
                            pipeLineDeal.setContactspeopleKey(jsonObjectContactsPeople.getString("_key"));
                        }
                        if (jsonObjectContactsPeople.has("phone")){
                                JSONObject jsonObjectContactsPeoplePhone = jsonObjectContactsPeople.getJSONObject("phone");
                                if (jsonObjectContactsPeoplePhone.has("work")){
                                    pipeLineDeal.setContactspeopleWorkPhone(jsonObjectContactsPeoplePhone.getString("work"));
                                }
                                if (jsonObjectContactsPeoplePhone.has("primary")){
                                    pipeLineDeal.setContactspeoplePrimaryPhone(jsonObjectContactsPeoplePhone.getString("primary"));
                                }
                        }

                        if (jsonObjectContactsPeople.has("name")){
                                JSONObject jsonObjectPeopleName = jsonObjectContactsPeople.getJSONObject("name");
                                if (jsonObjectPeopleName.has("FirstName")){
                                    pipeLineDeal.setContactsPeopleFirstName(jsonObjectPeopleName.getString("FirstName"));
                                }
                                if (jsonObjectPeopleName.has("LastName")){
                                    pipeLineDeal.setContactsPeopleLastName(jsonObjectPeopleName.getString("LastName"));
                                }
                        }
                }

                if (jsonObjectDeal.has("colour")){
                    pipeLineDeal.setColor(jsonObjectDeal.getString("colour"));
                }

                for (int state_index = 0 ; state_index < listListPipelineDeals.size();state_index++){
                    List<PipeLineStage> lineStageList = listListPipelineDeals.get(state_index);
                    for (int stage_index = 0 ;stage_index < lineStageList.size();stage_index++){
                        PipeLineStage stage = lineStageList.get(stage_index);
                        if (stage.get_key() != null && stage.get_key() != "null"){
                            if (stage.get_key().equalsIgnoreCase(pipeLineDeal.getStageKey())){
                               //Won
                                if ((pipeLineDeal.getState().equalsIgnoreCase("won")) && (pipeLineDeal.getStatus() == 1) && (state_index == 0)){
                                    stage.getLineDealList().add(pipeLineDeal);
                                    lineStageList.set(stage_index,stage);
                                    listListPipelineDeals.set(0,lineStageList);
                                }

                                //InProgress
                                if ((pipeLineDeal.getState().equalsIgnoreCase("in_progress")) && (pipeLineDeal.getStatus() == 1) && (state_index == 1)){
                                    stage.getLineDealList().add(pipeLineDeal);
                                    lineStageList.set(stage_index,stage);
                                    listListPipelineDeals.set(1,lineStageList);
                                }

                                //Lost
                                if ((pipeLineDeal.getState().equalsIgnoreCase("lost")) && (pipeLineDeal.getStatus() == 1) && (state_index == 2)){
                                    stage.getLineDealList().add(pipeLineDeal);
                                    lineStageList.set(stage_index,stage);
                                    listListPipelineDeals.set(2,lineStageList);
                                }

                                //Archive
                                if ((pipeLineDeal.getState().equalsIgnoreCase("in_progress")) && (pipeLineDeal.getStatus() == 0) && (state_index == 3)){
                                    stage.getLineDealList().add(pipeLineDeal);
                                    lineStageList.set(stage_index,stage);
                                    listListPipelineDeals.set(3,lineStageList);
                                }

                                //Trash
                                if ((pipeLineDeal.getState().equalsIgnoreCase("in_progress")) && (pipeLineDeal.getStatus() == -1) && (state_index == 4)){
                                    stage.getLineDealList().add(pipeLineDeal);
                                    lineStageList.set(stage_index,stage);
                                    listListPipelineDeals.set(4,lineStageList);
                                }
                            }
                        }
                    }
                }


            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        return listListPipelineDeals;
    }

    public void setupBoardView(){
        ArrayList<SimpleBoardAdapter.SimpleColumn> data = new ArrayList<>();
        if (listListCurrentPipeLineDealsData.size() > 0){
            List<PipeLineStage> lineStageList = listListCurrentPipeLineDealsData.get(CURRENT_STATE);
            for (int i = 0; i < lineStageList.size();i++){
                PipeLineStage lineStage = lineStageList.get(i);
                data.add(new SimpleBoardAdapter.SimpleColumn(lineStage,lineStage.getLineDealList()));
            }
        }
        final SimpleBoardAdapter boardAdapter = new SimpleBoardAdapter(getContext(),data);
        boardView.setAdapter(boardAdapter);
        boardView.setOnDoneListener(new BoardView.DoneListener() {
            @Override
            public void onDone() {
               // Log.e("scroll","done");
            }
        });
        boardView.setOnItemClickListener(new BoardView.ItemClickListener() {
            @Override
            public void onClick(View v, int column_pos, int item_pos) {
                boardView.scrollToColumn(column_pos,false);
            }
        });
        boardView.setOnHeaderClickListener(new BoardView.HeaderClickListener() {
            @Override
            public void onClick(View v, int column_pos) {
              //  Log.e("ee",String.valueOf(column_pos));
            }
        });
        boardView.setOnDragColumnListener(new BoardView.DragColumnStartCallback() {
            @Override
            public void startDrag(View view, int i) {
              //  Log.e("Start Drag Column",String.valueOf(i));
            }

            @Override
            public void changedPosition(View view, int i, int i1) {
               // Log.e("Change Drag Column",String.valueOf(i1));
            }

            @Override
            public void dragging(View itemView, MotionEvent event) {
               // Log.e("Pos X",String.valueOf(event.getRawX()));
                //Log.e("Pos Y",String.valueOf(event.getRawY()));
            }

            @Override
            public void endDrag(View view, int i, int i1) {
               // Log.e("End Drag Column",String.valueOf(i1));
            }
        });
        boardView.setOnFooterClickListener(new BoardView.FooterClickListener() {
            @Override
            public void onClick(View v, int column_pos) {
               // Log.e("Footer Click","Column: "+String.valueOf(column_pos));
            }
        });
        boardView.setOnDragItemListener(new BoardView.DragItemStartCallback() {
            @Override
            public void startDrag(View view, int i, int i1) {
                Log.e("Start Drag Item","Item: "+String.valueOf(i1)+"; Column:"+String.valueOf(i));
                List<PipeLineStage> lineStageList = listListCurrentPipeLineDealsData.get(CURRENT_STATE);
                PipeLineStage stageModal = lineStageList.get(i);
                List<PipeLineDeal> lineDeals = stageModal.getLineDealList();
                currentDeal = lineDeals.get(i1);
                lineDeals.remove(i1);
                stageModal.setLineDealList(lineDeals);
                lineStageList.set(i,stageModal);
                listListCurrentPipeLineDealsData.set(CURRENT_STATE,lineStageList);
                Log.d(TAG,"current deal: " + currentDeal.getName() + " key: " + currentDeal.get_key());
                pickedDealIndex = i1;
                pickedStageIndex = i;
            }

            @Override
            public void changedPosition(View view, int i, int i1, int i2, int i3) {
               // Log.e("Change Drag Item","Item: "+String.valueOf(i2)+"; Column:"+String.valueOf(i3));
            }

            @Override
            public void dragging(View itemView, MotionEvent event) {
               // Log.e("Pos X",String.valueOf(event.getRawX()));
               // Log.e("Pos Y",String.valueOf(event.getRawY()));
            }

            @Override
            public void endDrag(View view, int i, int i1, int i2, int i3) {
                Log.e("End Drag Item","Item: "+ String.valueOf(i2) + "; Column:"+String.valueOf(i3));
                droppedDealIndex = i2;
                droppedStageIndex = i3;
                List<PipeLineStage> lineStageList = listListCurrentPipeLineDealsData.get(CURRENT_STATE);
                PipeLineStage stageModal = lineStageList.get(i3);
//                List<PipeLineDeal> deals = stageModal.getLineDealList();
//                deals.add(i2,currentDeal);
//                stageModal.setLineDealList(deals);
//                lineStageList.set(i3,stageModal);
//                listListCurrentPipeLineDealsData.set(CURRENT_STATE,lineStageList);

                if (Utility.isOnline(getContext())){
                    mProgressDialog = new ProgressDialog(getContext());
                    mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();
                    Log.d(TAG,"current deal: " + currentDeal.getName() + " key: " + currentDeal.get_key());
                    String url = API.KBaseUrlDeloz + "/" + accountKey
                            + "/" + project_id + API.kDeal + "/" + currentDeal.get_key() + API.kDealStageUpdate ;
                    JSONObject postData  = new JSONObject();
                    try {
                        postData.put("stages",stageModal.get_key());
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                    updateDeal(url,postData);
                }else {

                    List<PipeLineStage> lineStageListDrop = listListCurrentPipeLineDealsData.get(CURRENT_STATE);
                    PipeLineStage stageModalDrop = lineStageListDrop.get(pickedStageIndex);
                    List<PipeLineDeal> deals = stageModalDrop.getLineDealList();
                    deals.add(pickedDealIndex,currentDeal);
                    stageModalDrop.setLineDealList(deals);
                    lineStageListDrop.set(pickedStageIndex,stageModalDrop);
                    listListCurrentPipeLineDealsData.set(CURRENT_STATE,lineStageListDrop);
                    setupBoardView();
                    Toast.makeText(getContext(), R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void removeDealAfterError(){
        List<PipeLineStage> lineStageList = listListCurrentPipeLineDealsData.get(CURRENT_STATE);
        PipeLineStage stageModal = lineStageList.get(droppedStageIndex);
        List<PipeLineDeal> lineDeals = stageModal.getLineDealList();
        lineDeals.remove(droppedDealIndex);
        stageModal.setLineDealList(lineDeals);
        lineStageList.set(droppedStageIndex,stageModal);
        listListCurrentPipeLineDealsData.set(CURRENT_STATE,lineStageList);
    }

    public void updateDeal(String url ,JSONObject postData){

        try {
            JSONObject headerData = new JSONObject();
            headerData.put(API.SharedPreferenceKeys.device_key,deviceId);
            headerData.put(API.SharedPreferenceKeys.auth_code,auth_code);
            HttpUtility.postData(url,postData,HttpUtility
                    .getHeaders(headerData),getActivity(), new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                          //  removeDealAfterError();
                            List<PipeLineStage> lineStageList = listListCurrentPipeLineDealsData.get(CURRENT_STATE);
                            PipeLineStage stageModal = lineStageList.get(pickedStageIndex);
                            List<PipeLineDeal> deals = stageModal.getLineDealList();
                            deals.add(pickedDealIndex,currentDeal);
                            stageModal.setLineDealList(deals);
                            lineStageList.set(pickedStageIndex,stageModal);
                            listListCurrentPipeLineDealsData.set(CURRENT_STATE,lineStageList);
                            if ((CURRENT_STATE == 1) || (CURRENT_STATE == 3)){
                                Toast.makeText(getActivity(), R.string.error_unable_to_update_deal, Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getActivity(), R.string.error_in_progress_or_active_deals, Toast.LENGTH_SHORT).show();
                            }
                            setupBoardView();
                        }
                    });
                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    try {
                        final String mMessage = response.body().string();
                        Log.d(TAG + " onSuccess: ",mMessage);
                        Log.d(TAG,mMessage);
                        if (response.isSuccessful()){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (Utility.isRedirectFound(mMessage)){
                                        Utility.sessionLogout(mMessage,getContext(),getActivity());
                                    }else {
                                        Toast.makeText(getActivity(), "Deal updated successfully!", Toast.LENGTH_SHORT).show();
                                        List<PipeLineStage> lineStageList = listListCurrentPipeLineDealsData.get(CURRENT_STATE);
                                        PipeLineStage stageModal = lineStageList.get(droppedStageIndex);
                                        List<PipeLineDeal> deals = stageModal.getLineDealList();
                                        deals.add(droppedDealIndex,currentDeal);
                                        stageModal.setLineDealList(deals);
                                        lineStageList.set(droppedStageIndex,stageModal);
                                        listListCurrentPipeLineDealsData.set(CURRENT_STATE,lineStageList);
                                        setupBoardView();
                                    }
                                }
                            });

                        }else {
                            Toast.makeText(getActivity(), R.string.error_account_not_found, Toast.LENGTH_SHORT).show();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (JSONException e){
            e.printStackTrace();
        }
    }


    /**
     * Get Pipeline list
     */

    public void getPipeLineList(){
        final String project_id = PreferenceManager.getDefaultSharedPreferences(getContext())
                .getString(API.SharedPreferenceKeys.project_id, "");
        final String accountKey = PreferenceManager.getDefaultSharedPreferences(getContext())
                .getString(API.SharedPreferenceKeys.account_key, "");
        String url = API.KBaseUrlDeloz + "/" + accountKey + "/" + project_id + API.kGetPipeLineList;
        String deviceId = Utility.getDeviceId(getContext());
        String auth_code = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.auth_code, "");
        JSONObject postdata = new JSONObject();

        try {
            JSONObject headerData = new JSONObject();
            headerData.put(API.SharedPreferenceKeys.device_key,deviceId);
            headerData.put(API.SharedPreferenceKeys.auth_code,auth_code);
            HttpUtility.postData(url,postdata,HttpUtility
                    .getHeaders(headerData),getActivity(), new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    try {
                        final String mMessage = response.body().string();
                        Log.d(TAG + " onSuccess: ",mMessage);
                        Log.d(TAG,mMessage);
                        if (response.isSuccessful()){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (Utility.isRedirectFound(mMessage)){
                                        Utility.sessionLogout(mMessage,getContext(),getActivity());
                                    }else {
                                        try {
                                            JSONArray jsonArray = new JSONArray(mMessage);
                                            Log.d(TAG," json array "+ jsonArray.toString());
                                            parsePipelinelist(jsonArray);
                                        } catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }

                                }
                            });

                        }else {
                            Toast.makeText(getActivity(), R.string.error_account_not_found, Toast.LENGTH_SHORT).show();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void parsePipelinelist(JSONArray jsonArrayData){
        pipelineLists = new ArrayList<>();


        for (int i = 0;i< jsonArrayData.length();i++){
            try {
                JSONObject jsonObjectPipeline = jsonArrayData.getJSONObject(i);
                PipelineList pipelineListModal = new PipelineList();
                if (jsonObjectPipeline.has("account_id")){
                    pipelineListModal.setAccount_id(jsonObjectPipeline.getString("account_id"));
                }
                if (jsonObjectPipeline.has("_key")){
                    pipelineListModal.set_key(jsonObjectPipeline.getString("_key"));
                }
                if (jsonObjectPipeline.has("created_at")){
                    pipelineListModal.setCreated_at(jsonObjectPipeline.getString("created_at"));
                }
                if (jsonObjectPipeline.has("description")){
                    pipelineListModal.setDescription(jsonObjectPipeline.getString("description"));

                }
                if (jsonObjectPipeline.has("updated_at")){
                    pipelineListModal.setUpdated_at("updated_at");
                }
                if (jsonObjectPipeline.has("created_by")){
                    pipelineListModal.setCreated_by(jsonObjectPipeline.getString("created_by"));
                }
                if (jsonObjectPipeline.has("pipeline_name")){
                    pipelineListModal.setPipeline_name(jsonObjectPipeline.getString("pipeline_name"));
                }
                if (jsonObjectPipeline.has("project_id")){
                    pipelineListModal.setProject_id(jsonObjectPipeline.getString("project_id"));
                }
                if (jsonObjectPipeline.has("colour")){
                    pipelineListModal.setColour(jsonObjectPipeline.getString("colour"));
                }
                if (jsonObjectPipeline.has("updated_by")){
                    pipelineListModal.setUpdated_by(jsonObjectPipeline.getString("updated_by"));
                }
                pipelineLists.add(pipelineListModal);

            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        if (pipelineLists.size() > 0){
            PipelineList modal = pipelineLists.get(0);
            getPipeLineStages(modal.get_key());
        }


    }

}
