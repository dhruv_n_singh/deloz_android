package io.deloz.dhruv.deloz.Interfaces;

/**
 * Created by Dhruv on 1/30/18.
 */

public interface RecycleViewPipelineListClickListener {

    void onClickView(int position);
}
