package io.deloz.dhruv.deloz.Helper.Camera;

import android.content.Context;

import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;

/**
 * Created by Dhruv on 12/30/17.
 */

public class BarcodeTrackerFactory implements MultiProcessor.Factory<Barcode>{

    public Context mContext;

    public BarcodeTrackerFactory(Context context) {
        mContext = context;
    }

    @Override
    public Tracker<Barcode> create(Barcode barcode) {
        return new BarcodeTracker(mContext);
    }
}
