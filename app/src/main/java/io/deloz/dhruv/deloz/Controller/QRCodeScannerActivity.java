package io.deloz.dhruv.deloz.Controller;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.deloz.dhruv.deloz.Helper.API;
import io.deloz.dhruv.deloz.Helper.Camera.BarcodeTracker;
import io.deloz.dhruv.deloz.Helper.Camera.BarcodeTrackerFactory;
import io.deloz.dhruv.deloz.Helper.Camera.CameraSource;
import io.deloz.dhruv.deloz.Helper.Camera.CameraSourcePreview;
import io.deloz.dhruv.deloz.Helper.HttpUtility;
import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.R;
import okhttp3.Response;

public class QRCodeScannerActivity extends AppCompatActivity  implements BarcodeTracker.BarcodeGraphicTrackerCallback{


    // Intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;

    // Permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    // Constants used to pass extra data in the intent
    public static final String BarcodeObject = "Barcode";
    private String accountKey;
    private String barcodeValue;
    private CameraSource mCameraSource;
    private CameraSourcePreview mPreview;
    //VARIABLES
    private static final String TAG = AccountValidationActivity.class.getName();
    private ProgressDialog mProgressDialog;

    /**
     * Initializes the UI and creates the detector pipeline.
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_qrcode_scanner);

        mPreview = (CameraSourcePreview) findViewById(R.id.preview);
        accountKey = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(API.SharedPreferenceKeys.account_key, "");

        boolean autoFocus = true;
        boolean useFlash = false;

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource(autoFocus, useFlash);
        } else {
            requestCameraPermission();
        }
    }

    @Override
    public void onDetectedQrCode(Barcode barcode) {
        if (barcode != null) {
            Log.d("QRCodeScannerActivity:",barcode.displayValue);
            barcodeValue = barcode.displayValue;
            mPreview.stop();
            mPreview.release();
        }
    }

    public void verifyQRCode(final String barcode){
        String baseURL = API.kBaseUrl   + API.kVerifyQrCode + "/" + barcode;
        String deviceId = Utility.getDeviceId(getApplicationContext());
        Log.d(TAG + "url: ",baseURL);
        JSONObject postdata = new JSONObject();
        Log.d(TAG + "Param:",postdata.toString());

        try {
            JSONObject headerData = new JSONObject();
            headerData.put("Device-Key",deviceId);
            Log.d(TAG+ "Header",headerData.toString());
            HttpUtility.postData(baseURL,postdata,HttpUtility
                    .getHeaders(headerData),QRCodeScannerActivity.this, new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                    QRCodeScannerActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(QRCodeScannerActivity.this)
                                    .setTitle("")
                                    .setMessage(R.string.error_unable_to_get_response)
                                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            verifyQRCode(barcode);
                                        }
                                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();
                        }
                    });
                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    String mMessage = null;
                    try {
                        mMessage = response.body().string();
                        Log.d(TAG,mMessage);
                        if (response.isSuccessful()){
                            try {
                                JSONObject json = new JSONObject(mMessage);
                                if (json.has("Authorization-Key")){
                                    String authorizationKey = json.getString("Authorization-Key");
                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                                            .edit().putString(API.SharedPreferenceKeys.auth_code, authorizationKey).apply();
                                    Log.d(TAG,authorizationKey);
                                    String strURL = API.KBaseUrlDeloz + "/" + accountKey + API.kGetProjectList;
                                    getProjectId(strURL);

                                }
                            } catch (Exception e){
                                e.printStackTrace();
                            }

                        }else {
                            Toast.makeText(QRCodeScannerActivity.this, R.string.error_account_not_found, Toast.LENGTH_SHORT).show();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // Handles the requesting of the camera permission.
    private void requestCameraPermission() {
        Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
        }
    }

    public void onGotIt(View view){
        if (barcodeValue != null){

            QRCodeScannerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    verifyQRCode(barcodeValue);
                }
            });

        }else{
            Toast.makeText(QRCodeScannerActivity.this, R.string.error_scan_qrcode, Toast.LENGTH_SHORT).show();

        }
    }

    /**
     * Creates and starts the camera.
     *
     * Suppressing InlinedApi since there is a check that the minimum version is met before using
     * the constant.
     */
    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        Context context = getApplicationContext();

        // A barcode detector is created to track barcodes.  An associated multi-processor instance
        // is set to receive the barcode detection results, track the barcodes, and maintain
        // graphics for each barcode on screen.  The factory is used by the multi-processor to
        // create a separate tracker instance for each barcode.
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(context)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();
        BarcodeTrackerFactory barcodeFactory = new BarcodeTrackerFactory(this);
        barcodeDetector.setProcessor(new MultiProcessor.Builder<>(barcodeFactory).build());

        if (!barcodeDetector.isOperational()) {
            // Note: The first time that an app using the barcode or face API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any barcodes
            // and/or faces.
            //
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.
            Log.w(TAG, "Detector dependencies are not yet available.");

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(this, R.string.low_storage_error,
                        Toast.LENGTH_LONG).show();
                Log.w(TAG, getString(R.string.low_storage_error));
            }
        }

        // Creates and starts the camera.  Note that this uses a higher resolution in comparison
        // to other detection examples to enable the barcode detector to detect small barcodes
        // at long distances.
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        CameraSource.Builder builder = new CameraSource.Builder(getApplicationContext(), barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(metrics.widthPixels, metrics.heightPixels)
                .setRequestedFps(24.0f);

        // make sure that auto focus is an available option
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            builder = builder.setFocusMode(
                    autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null);
        }

        mCameraSource = builder
                .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                .build();
    }

    // Restarts the camera
    @Override
    protected void onResume() {
        super.onResume();
        startCameraSource();
    }

    // Stops the camera
    @Override
    protected void onPause() {
        super.onPause();
        if (mPreview != null) {
            mPreview.stop();
        }
    }

    /**
     * Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPreview != null) {
            mPreview.release();
        }
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera source");
            // we have permission, so create the camerasource
            boolean autoFocus = true;
            boolean useFlash = false;
            createCameraSource(autoFocus, useFlash);
            return;
        }

        Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Multitracker sample")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();
    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() throws SecurityException {
        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    public void getProjectId(final String url){

        try{
            String deviceId = Utility.getDeviceId(getApplicationContext());
            String auth_code = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                    .getString(API.SharedPreferenceKeys.auth_code, "");
            JSONObject headerData = new JSONObject();
            headerData.put(API.SharedPreferenceKeys.device_key,deviceId);
            headerData.put(API.SharedPreferenceKeys.auth_code,auth_code);
            HttpUtility.getData(url,HttpUtility
                    .getHeaders(headerData),QRCodeScannerActivity.this,new HttpUtility.HttpCallback(){

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    QRCodeScannerActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(QRCodeScannerActivity.this)
                                    .setTitle("")
                                    .setMessage(R.string.error_unable_to_get_response)
                                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            getProjectId(url);
                                        }
                                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();
                        }
                    });
                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    // <-------- Do some view manipulation here
                    final String result;  // 4
                    try {
                        result = response.body().string();
                        Log.d(TAG, response.code() + " " +result);
                        if (response.isSuccessful()){
                            try{
                                JSONObject jsonResponse = new JSONObject(result);
                                if (jsonResponse.has("payload")){
                                    Log.d(TAG + " payload: ",jsonResponse.toString());
                                        JSONObject jsonPayload = jsonResponse.getJSONObject("payload");
                                        if (jsonPayload.has("list")){
                                            JSONArray jsonList = jsonPayload.getJSONArray("list");
                                            if (jsonList.length() > 0){
                                                JSONObject jsonProject  = jsonList.getJSONObject(0);
                                                if (jsonProject.has("id")){
                                                    String projectId = jsonProject.getString("id");
                                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                                                            .edit().putString(API.SharedPreferenceKeys.project_id, projectId).apply();
                                                    Intent intent = new Intent(QRCodeScannerActivity.this, DrawerMainActivity.class);
                                                    startActivity(intent);
                                                    killActivity();
                                                }
                                            }
                                        }
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                            }

                        }else {

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            });

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void killActivity(){
        QRCodeScannerActivity.this.finishAffinity();
    }
}
