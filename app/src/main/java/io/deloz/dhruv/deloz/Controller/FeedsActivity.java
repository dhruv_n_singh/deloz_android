package io.deloz.dhruv.deloz.Controller;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.timqi.sectorprogressview.ColorfulRingProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.deloz.dhruv.deloz.Helper.API;
import io.deloz.dhruv.deloz.Helper.HttpUtility;
import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.Helper.charts.BarData;
import io.deloz.dhruv.deloz.Helper.charts.ChartProgressBar;
import io.deloz.dhruv.deloz.Helper.charts.OnBarClickedListener;
import io.deloz.dhruv.deloz.Modal.DashboardBarChart;
import io.deloz.dhruv.deloz.Modal.DashboardInsights;
import io.deloz.dhruv.deloz.R;
import okhttp3.Response;

public class FeedsActivity extends Fragment implements OnBarClickedListener {

    //Variables
    DashboardInsights dashboardInsightsModal;

    //UI Components
    private ProgressDialog mProgressDialog;
    private ChartProgressBar mChart;
    private static final String TAG = FeedsActivity.class.getName();
    ColorfulRingProgressView progressViewDeals;
    TextView textViewTotalDealValue;
    TextView textViewNumberOfDeals;
    TextView textViewMyDealsWorth;
    TextView textViewMyDealsNumber;
    TextView textViewSalesInsight;
    TextView textViewLast30DaysNoOfDeals;
    TextView textViewTodayWonDeals;
    TextView textViewTodayDealWorth;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.activity_feeds, container, false);
        setupUIComponents(view);
        setupCircularProgressView(view);
        setupChartProgressBarChart(view);
        if (Utility.isOnline(getContext())){
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            setupDashboardCircularChart();
        }else {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_no_internet_connection)
                    .setMessage(getString(R.string.error_internet_connection_message))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
        }
        return view;
    }

    public void setupUIComponents(View view){
        textViewTotalDealValue = (TextView)view.findViewById(R.id.textViewTotalDealValue);
        textViewNumberOfDeals = (TextView)view.findViewById(R.id.textViewNumberOfDeals);
        textViewMyDealsWorth = (TextView)view.findViewById(R.id.textViewMyDealsWorth);
        textViewMyDealsNumber = (TextView)view.findViewById(R.id.textViewMyDealsNumber);
        textViewSalesInsight = (TextView)view.findViewById(R.id.textViewSalesInsight);
        textViewLast30DaysNoOfDeals = (TextView)view.findViewById(R.id.textViewLast30DaysNoOfDeals);
        textViewTodayWonDeals = (TextView)view.findViewById(R.id.textViewTodayWonDeals);
        textViewTodayDealWorth = (TextView)view.findViewById(R.id.textViewTodayDealWorth);

    }

    public void setupDashboardCircularChart(){
        dashboardInsightsModal = new DashboardInsights();
        final String accountKey = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.account_key, "");;
        String url = API.KBaseUrlDeloz + "/" + accountKey + API.kDashboardInsights;
        JSONArray jsonArrayGroups = new JSONArray();
        jsonArrayGroups.put("total_deal_value_num_deals");
        jsonArrayGroups.put("todays_deal_value_and_num_deals");
        jsonArrayGroups.put("next_30_days_deal_value_and_num_deals");
        jsonArrayGroups.put("todays_won_deal_value_and_num_deals");
        jsonArrayGroups.put("my_total_deal_value_num_deals");
        jsonArrayGroups.put("current_month_won_deal_value_and_num_deals");
        jsonArrayGroups.put("lastweeks_won_deal_value_and_num_deals");
        JSONObject postdata = new JSONObject();
        try {
            postdata.put("groups", jsonArrayGroups);
        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        getSalesInsights(url,postdata);

    }

    public  void setupCircularProgressView(View view){
        progressViewDeals = (ColorfulRingProgressView) view.findViewById(R.id.dealProgressView);
        progressViewDeals.setPercent(0);
        progressViewDeals.setStartAngle(0);
        progressViewDeals.setStrokeWidthDp(10);
        ObjectAnimator anim = ObjectAnimator.ofFloat(progressViewDeals, "percent",
                0, ((ColorfulRingProgressView) progressViewDeals).getPercent());
        anim.setInterpolator(new LinearInterpolator());
        anim.setDuration(1000);
        anim.start();

    }

    public void setupChartProgressBarChart(View view){
        mChart = (ChartProgressBar) view.findViewById(R.id.dealsHistoryBarChart);
    }


    public void getSalesInsights(final String url, final JSONObject jsonObjectPostData){
        String deviceId = Utility.getDeviceId(getContext());
        String auth_code = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.auth_code, "");
        Log.d(TAG ,jsonObjectPostData.toString());
        try {
            JSONObject headerData = new JSONObject();
            headerData.put("Device-Key",deviceId);
            headerData.put("Authorization-Key",auth_code);
            Log.d(TAG+ " Header: ",headerData.toString());
            HttpUtility.postData(url,jsonObjectPostData,HttpUtility
                    .getHeaders(headerData),getContext(), new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("")
                                    .setMessage("Error: Unable to get response from server!")
                                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            getSalesInsights(url,jsonObjectPostData);
                                        }
                                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();

                        }
                    });
                }

                @Override
                public void onSuccess(Response response) {

                    // final String mMessage = null;
                    try {
                        final String mMessage = response.body().string();
                        Log.d(TAG + " onSuccess: ",mMessage);
                        Log.d(TAG,mMessage);

                        if (response.isSuccessful()){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (Utility.isRedirectFound(mMessage)){
                                        Utility.sessionLogout(mMessage,getActivity(),getActivity());
                                    }else {
                                        try {
                                            JSONObject json = new JSONObject(mMessage);
                                            if (json.has("payload")){
                                                JSONObject jsonObjectPayload = json.getJSONObject("payload");
                                                getDashboardInsightsDeals(jsonObjectPayload);
                                            }
                                        } catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });




                        }else {
                            Toast.makeText(getContext(), R.string.error_unable_to_get_response, Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void getDashboardInsightsDeals(JSONObject jsonObjectDealsDetails){

        if (jsonObjectDealsDetails.has("total_deal_value_num_deals")){
            try {
                JSONObject dict_total_deal_value_num_deals = jsonObjectDealsDetails.getJSONObject("total_deal_value_num_deals");
                if (dict_total_deal_value_num_deals.has("sum_value")){
                    dashboardInsightsModal.setTotalDealValue(dict_total_deal_value_num_deals.getDouble("sum_value"));
                }
                if (dict_total_deal_value_num_deals.has("number_of_deals")){
                    dashboardInsightsModal.setTotalDeals(dict_total_deal_value_num_deals.getLong("number_of_deals"));
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        if (jsonObjectDealsDetails.has("my_total_deal_value_num_deals")){
            try{
                JSONObject dict_my_deal_value_num_deals = jsonObjectDealsDetails.getJSONObject("my_total_deal_value_num_deals");
                if (dict_my_deal_value_num_deals.has("number_of_deals")){
                    dashboardInsightsModal.setMyDeals(dict_my_deal_value_num_deals.getLong("number_of_deals"));
                }
                if (dict_my_deal_value_num_deals.has("sum_value")){
                    dashboardInsightsModal.setMyDealValue(dict_my_deal_value_num_deals.getDouble("sum_value"));
                }

            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        if (jsonObjectDealsDetails.has("current_month_won_deal_value_and_num_deals")){
            try {
                 JSONObject dict_current_month_won_deal_value_and_num_deals = jsonObjectDealsDetails
                         .getJSONObject("current_month_won_deal_value_and_num_deals");
                 if (dict_current_month_won_deal_value_and_num_deals.has("number_of_deals")){
                     dashboardInsightsModal.setMonthlyDeals(dict_current_month_won_deal_value_and_num_deals.getLong("number_of_deals"));
                 }

                 if (dict_current_month_won_deal_value_and_num_deals.has("sum_value")){
                     dashboardInsightsModal
                             .setMonthlyDealValue(dict_current_month_won_deal_value_and_num_deals.getDouble("sum_value"));
                 }

            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        if (jsonObjectDealsDetails.has("todays_deal_value_and_num_deals")){
            try {
                JSONObject dict_todays_deal_value_and_num_deals = jsonObjectDealsDetails
                        .getJSONObject("todays_deal_value_and_num_deals");
                if (dict_todays_deal_value_and_num_deals.has("number_of_deals")){
                    dashboardInsightsModal.setCurrent_month_number_of_deals(dict_todays_deal_value_and_num_deals.getLong("number_of_deals"));
                }
                if (dict_todays_deal_value_and_num_deals.has("sum_value")){
                    dashboardInsightsModal.setCurrent_month_won_deal(dict_todays_deal_value_and_num_deals.getDouble("sum_value"));
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        if (dashboardInsightsModal.getTotalDealValue() != null && dashboardInsightsModal.getMyDealValue() != null){
            float dealPercentage = (float) (dashboardInsightsModal.getMyDealValue()/dashboardInsightsModal.getTotalDealValue())*100;
            progressViewDeals.setPercent(dealPercentage);
            ObjectAnimator anim = ObjectAnimator.ofFloat(progressViewDeals, "percent",
                    0, ((ColorfulRingProgressView) progressViewDeals).getPercent());
            anim.setInterpolator(new LinearInterpolator());
            anim.setDuration(1000);
            anim.start();
        }

        if (dashboardInsightsModal.getTotalDealValue() != null){
            textViewTotalDealValue.setText(String.format("$ %1.2f", dashboardInsightsModal.getTotalDealValue()));
        }

        if (dashboardInsightsModal.getTotalDeals() > 1){
            textViewNumberOfDeals.setText(dashboardInsightsModal.getTotalDeals() + " Deals");
        }else {
            textViewNumberOfDeals.setText(dashboardInsightsModal.getTotalDeals() + " Deal");
        }

        if (dashboardInsightsModal.getMyDealValue() != null){
            textViewMyDealsWorth.setText(String.format("$ %1.2f", dashboardInsightsModal.getMyDealValue()));
        }

        if (dashboardInsightsModal.getMyDeals() > 1){
            textViewMyDealsNumber.setText(dashboardInsightsModal.getMyDeals() + " Deals");
        }else {
            textViewMyDealsNumber.setText(dashboardInsightsModal.getMyDeals() + " Deal");
        }

        if (dashboardInsightsModal.getTodayDeals() > 1){
            textViewTodayWonDeals.setText(dashboardInsightsModal.getTotalDeals() + " Deals");
        }else {
            textViewTodayWonDeals.setText(dashboardInsightsModal.getTotalDeals() + " Deal");
        }

        if (dashboardInsightsModal.getTodayDealWorth() != null){
            textViewTodayDealWorth.setText("$ "+ dashboardInsightsModal.getTodayDealWorth());
        }

        Log.d(TAG," Last 30 days " + Utility.getLast30Days().toString());
        final String accountKey = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.account_key, "");;
        String url = API.KBaseUrlDeloz + "/" + accountKey + API.kBarChartDetails;
        JSONObject postdata = new JSONObject();
        getBarCharData(url,jsonObjectDealsDetails);

    }


    public void getBarCharData(final String url , final JSONObject jsonObjectPostData){
        String deviceId = Utility.getDeviceId(getContext());
        String auth_code = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.auth_code, "");
        Log.d(TAG ,jsonObjectPostData.toString());
        try {
            JSONObject headerData = new JSONObject();
            headerData.put("Device-Key",deviceId);
            headerData.put("Authorization-Key",auth_code);
            Log.d(TAG+ " Header: ",headerData.toString());
            HttpUtility.postData(url,jsonObjectPostData,HttpUtility
                    .getHeaders(headerData),getContext(), new  HttpUtility.HttpCallback()
            {

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                    new AlertDialog.Builder(getActivity())
                            .setTitle("")
                            .setMessage(R.string.error_unable_to_get_response)
                            .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    getBarCharData(url,jsonObjectPostData);
                                }
                            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();

                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    // final String mMessage = null;
                    try {
                        final String mMessage = response.body().string();
                        Log.d(TAG + " onSuccess: ",mMessage);
                        Log.d(TAG,mMessage);
                        if (response.isSuccessful()){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        JSONObject json = new JSONObject(mMessage);
                                        if (json.has("payload")){
                                            JSONArray jsonArrayPayload = json.getJSONArray("payload");
                                            if (jsonArrayPayload.length() > 0){
                                                parseBarCharData(jsonArrayPayload.getJSONObject(0));
                                            }
                                        }
                                    } catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }else {
                            Toast.makeText(getContext(), R.string.error_unable_to_get_response, Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void parseBarCharData(JSONObject jsonObjectData){
        ArrayList<BarData> dataList = new ArrayList<>();
        List<DashboardBarChart> listDashboardBarChart = new ArrayList<>();
        List<String> arrLast30Days = new ArrayList<>();
        arrLast30Days = Utility.getLast30Days();
        long totalNumberOfDeals = 0;
        Double totalDealValue = 0.0;
        long maxTotalNumberOfDeals = 0;
        Double maxTotalDealValue = 0.0;
        for (int i = 0; i < arrLast30Days.size();i++){
            DashboardBarChart modal = new DashboardBarChart();
            modal.setDate(arrLast30Days.get(i));
            if (jsonObjectData.has(arrLast30Days.get(i))){
                try {
                    JSONObject jsonObjectDateData = jsonObjectData.getJSONObject(arrLast30Days.get(i));
                    modal.setNumberOfDeals(jsonObjectDateData.getLong("num_deals"));
                    modal.setAmountValue(jsonObjectDateData.getDouble("value"));
                    totalNumberOfDeals += jsonObjectDateData.getLong("num_deals");
                    totalDealValue += jsonObjectDateData.getDouble("value");
                    if (maxTotalNumberOfDeals < jsonObjectDateData.getLong("num_deals")){
                        maxTotalNumberOfDeals = jsonObjectDateData.getLong("num_deals");
                    }
                    if (maxTotalDealValue < jsonObjectDateData.getDouble("value")){
                        maxTotalDealValue = jsonObjectDateData.getDouble("value");
                    }
                }catch (JSONException e){
                    modal.setAmountValue(0.0);
                    modal.setNumberOfDeals(0);
                    e.printStackTrace();
                }
            }else {
               modal.setAmountValue(0.0);
               modal.setNumberOfDeals(0);
            }
            listDashboardBarChart.add(modal);
        }

        for (int i = 0;i < listDashboardBarChart.size();i++){
            DashboardBarChart modal = listDashboardBarChart.get(i);
            float convertedValue = (float) (((modal.getAmountValue()/maxTotalDealValue)*10));
            if (convertedValue == 0){
                convertedValue = 1.7f;
            }
            BarData data = new BarData("  ", convertedValue, "  "+modal.getNumberOfDeals());
            dataList.add(data);
        }
        Log.d(TAG," count " + dataList.size());
        mChart.setDataList(dataList);
        mChart.build();
        mChart.setOnBarClickedListener(this);

        textViewSalesInsight.setText("$ " + totalDealValue + " / " + totalNumberOfDeals);
        if (totalNumberOfDeals > 1){
            textViewLast30DaysNoOfDeals.setText("Deals");
        }else {
            textViewLast30DaysNoOfDeals.setText("Deal");
        }
    }


    @Override public void onBarClicked(int index) {

    }


}
