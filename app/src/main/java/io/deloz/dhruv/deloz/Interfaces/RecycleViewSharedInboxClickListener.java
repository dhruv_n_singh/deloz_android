package io.deloz.dhruv.deloz.Interfaces;

import io.deloz.dhruv.deloz.Modal.SharedInboxList;

/**
 * Created by Dhruv on 2/15/18.
 */

public interface RecycleViewSharedInboxClickListener {

    void onClickSharedInbox(SharedInboxList sharedInboxList);
}
