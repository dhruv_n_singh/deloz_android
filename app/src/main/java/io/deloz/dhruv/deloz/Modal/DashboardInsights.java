package io.deloz.dhruv.deloz.Modal;

/**
 * Created by Dhruv on 1/23/18.
 */

public class DashboardInsights {

    private Double totalDealValue;
    private long totalDeals;
    private Double myDealValue;
    private long myDeals;
    private Double current_month_won_deal;
    private long current_month_number_of_deals;
    private Double monthlyDealValue;
    private long monthlyDeals;
    private Double todayDealWorth;
    private long todayDeals;

    //Getter
    public Double getTotalDealValue() {
        return totalDealValue;
    }

    public long getTotalDeals() {
        return totalDeals;
    }

    public Double getMyDealValue() {
        return myDealValue;
    }

    public long getMyDeals() {
        return myDeals;
    }

    public Double getCurrent_month_won_deal() {
        return current_month_won_deal;
    }

    public long getCurrent_month_number_of_deals() {
        return current_month_number_of_deals;
    }

    public Double getMonthlyDealValue() {
        return monthlyDealValue;
    }

    public long getMonthlyDeals() {
        return monthlyDeals;
    }

    public Double getTodayDealWorth() {
        return todayDealWorth;
    }

    public long getTodayDeals() {
        return todayDeals;
    }

    //Setter

    public void setTotalDealValue(Double totalDealValue) {
        this.totalDealValue = totalDealValue;
    }

    public void setTotalDeals(long totalDeals) {
        this.totalDeals = totalDeals;
    }

    public void setMyDealValue(Double myDealValue) {
        this.myDealValue = myDealValue;
    }

    public void setMyDeals(long myDeals) {
        this.myDeals = myDeals;
    }

    public void setCurrent_month_won_deal(Double current_month_won_deal) {
        this.current_month_won_deal = current_month_won_deal;
    }

    public void setCurrent_month_number_of_deals(long current_month_number_of_deals) {
        this.current_month_number_of_deals = current_month_number_of_deals;
    }

    public void setMonthlyDealValue(Double monthlyDealValue) {
        this.monthlyDealValue = monthlyDealValue;
    }

    public void setMonthlyDeals(long monthlyDeals) {
        this.monthlyDeals = monthlyDeals;
    }

    public void setTodayDealWorth(Double todayDealWorth) {
        this.todayDealWorth = todayDealWorth;
    }

    public void setTodayDeals(long todayDeals) {
        this.todayDeals = todayDeals;
    }
}
