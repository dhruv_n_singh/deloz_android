package io.deloz.dhruv.deloz.Modal;

/**
 * Created by Dhruv on 2/14/18.
 */

public class SharedInboxList {

 private Boolean isStrict;
 private String created_by;
 private String default_from_mail;
 private Boolean isPrivate;
 private int received_count;
 private int status;
 private String account_id;
 private long unreadlength;
 private Boolean register_for_calls;
 private String colour;
 private Double call_credits;
 private String _key;
 private String created_at;
 private String to_email_address;
 private Boolean inbox_enabled;
 private String inbox_name;
 private String _id;
 private String inbox_type;

    //Getter

    public Boolean getStrict() {
        return isStrict;
    }

    public String getCreated_by() {
        return created_by;
    }

    public String getDefault_from_mail() {
        return default_from_mail;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public int getReceived_count() {
        return received_count;
    }

    public int getStatus() {
        return status;
    }

    public String getAccount_id() {
        return account_id;
    }

    public long getUnreadlength() {
        return unreadlength;
    }

    public Boolean getRegister_for_calls() {
        return register_for_calls;
    }

    public String getColour() {
        return colour;
    }

    public Double getCall_credits() {
        return call_credits;
    }

    public String get_key() {
        return _key;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getTo_email_address() {
        return to_email_address;
    }

    public Boolean getInbox_enabled() {
        return inbox_enabled;
    }

    public String getInbox_name() {
        return inbox_name;
    }

    public String get_id() {
        return _id;
    }

    public String getInbox_type() {
        return inbox_type;
    }

    //Setter


    public void setStrict(Boolean strict) {
        isStrict = strict;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public void setDefault_from_mail(String default_from_mail) {
        this.default_from_mail = default_from_mail;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public void setReceived_count(int received_count) {
        this.received_count = received_count;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public void setUnreadlength(long unreadlength) {
        this.unreadlength = unreadlength;
    }

    public void setRegister_for_calls(Boolean register_for_calls) {
        this.register_for_calls = register_for_calls;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setCall_credits(Double call_credits) {
        this.call_credits = call_credits;
    }

    public void set_key(String _key) {
        this._key = _key;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setTo_email_address(String to_email_address) {
        this.to_email_address = to_email_address;
    }

    public void setInbox_enabled(Boolean inbox_enabled) {
        this.inbox_enabled = inbox_enabled;
    }

    public void setInbox_name(String inbox_name) {
        this.inbox_name = inbox_name;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setInbox_type(String inbox_type) {
        this.inbox_type = inbox_type;
    }
}
