package io.deloz.dhruv.deloz.Helper.swipelayout.adapters.Implements.Interfaces;

/**
 * Created by Dhruv on 1/19/18.
 */

public interface SwipeAdapterInterface {

    int getSwipeLayoutResourceId(int position);

    void notifyDatasetChanged();

}