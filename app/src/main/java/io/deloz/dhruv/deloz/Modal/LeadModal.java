package io.deloz.dhruv.deloz.Modal;

import java.io.Serializable;

/**
 * Created by Dhruv on 12/20/17.
 */

public class LeadModal implements Serializable {

    private String leadChannel;
    private Integer status;
    private String project_id ;
    private String firstName ;
    private String lastName;
    private String key;
    private String rev;
    private String createdUserProfilePic;
    private String createdUserName;
    private String createdFirstName;
    private String createdLastName;
    private String createdId;
    private String createdEmail;
    private String createdByColor;
    private String leadPrimaryEmail;
    private String leadSecondaryEmail;
    private String leadWorkPhone;
    private String leadPrimaryPhone;
    private Integer value;
    private String createdTime;
    private String leadId;
    private String leadColor;
    private String leadAccountId;
    private String leadUserImageURL;
    private String userImageThumbURL;
    private Boolean isForSpinner;
    private String companyName;
    private String address;
    private String contactsPeople;
    private String assignedToUsername;
    private String assignedToFirstName;
    private String assignedToLastName;
    private String assignedToColor;
    private String assignedToEmail;
    private String assignedToId;

    //Getters

    public String getAssignedToUsername() {
        return assignedToUsername;
    }

    public String getAssignedToFirstName() {
        return assignedToFirstName;
    }

    public String getAssignedToLastName() {
        return assignedToLastName;
    }

    public String getAssignedToColor() {
        return assignedToColor;
    }

    public String getAssignedToEmail() {
        return assignedToEmail;
    }

    public String getAssignedToId() {
        return assignedToId;
    }

    public String getLeadChannel(){ return leadChannel;}
    public Integer getStatus(){return status;}
    public String getProjectId(){ return project_id;}
    public String getCreatedFirstName() {
        return createdFirstName;
    }
    public String getFirstName(){return firstName;}
    public String getLastName(){return lastName;}
    public String getKey(){return key;}
    public String getRev(){return rev;}

    public String getCreatedByColor() {
        return createdByColor;
    }

    public String getCreatedUserProfilePic(){return createdUserProfilePic;}
    public String getCreatedUserName(){return createdUserName;}
    public String getCreatedLastName(){return createdLastName;}
    public String getCreatedId(){return createdId;}
    public String getCreatedEmail(){return createdEmail;}
    public String getLeadPrimaryEmail(){return leadPrimaryEmail;}
    public String getLeadSecondaryEmail(){return leadSecondaryEmail;}
    public String getLeadWorkPhone(){return leadWorkPhone;}
    public String getLeadPrimaryPhone(){return leadPrimaryPhone;}
    public Integer getValue(){return value;}
    public String getCreatedTime(){return createdTime;}
    public String getLeadId(){return leadId;}
    public String getLeadColor(){return leadColor;}
    public String getLeadAccountId(){return leadAccountId;}
    public String getLeadUserImageURL(){return leadUserImageURL;}
    public String getUserImageThumbURL(){return userImageThumbURL;}
    public Boolean getIsForSpinner(){return isForSpinner;}
    public String getCompanyName() {
        return companyName;
    }
    public String getAddress() {
        return address;
    }
    public String getContactsPeople(){ return contactsPeople; }


    //Setters


    public void setAssignedToUsername(String assignedToUsername) {
        this.assignedToUsername = assignedToUsername;
    }

    public void setAssignedToFirstName(String assignedToFirstName) {
        this.assignedToFirstName = assignedToFirstName;
    }

    public void setAssignedToLastName(String assignedToLastName) {
        this.assignedToLastName = assignedToLastName;
    }

    public void setAssignedToColor(String assignedToColor) {
        this.assignedToColor = assignedToColor;
    }

    public void setAssignedToEmail(String assignedToEmail) {
        this.assignedToEmail = assignedToEmail;
    }

    public void setAssignedToId(String assignedToId) {
        this.assignedToId = assignedToId;
    }

    public void setCreatedByColor(String createdByColor) {
        this.createdByColor = createdByColor;
    }

    public void setLeadChannel(String leadChannel) {
        this.leadChannel = leadChannel;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setProject_id(String project_id){
        this.project_id = project_id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setRev(String rev) {
        this.rev = rev;
    }

    public void setCreatedUserProfilePic(String createdUserProfilePic) {
        this.createdUserProfilePic = createdUserProfilePic;
    }

    public void setCreatedUserName(String createdUserName) {
        this.createdUserName = createdUserName;
    }
    public void setCreatedFirstName(String createdFirstName) {
        this.createdFirstName = createdFirstName;
    }

    public void setCreatedLastName(String createdLastName) {
        this.createdLastName = createdLastName;
    }

    public void setCreatedId(String createdId) {
        this.createdId = createdId;
    }

    public void setCreatedEmail(String createdEmail) {
        this.createdEmail = createdEmail;
    }

    public void setLeadPrimaryEmail(String leadPrimaryEmail) {
        this.leadPrimaryEmail = leadPrimaryEmail;
    }

    public void setLeadSecondaryEmail(String leadSecondaryEmail) {
        this.leadSecondaryEmail = leadSecondaryEmail;
    }

    public void setLeadWorkPhone(String leadWorkPhone) {
        this.leadWorkPhone = leadWorkPhone;
    }

    public void setLeadPrimaryPhone(String leadPrimaryPhone) {
        this.leadPrimaryPhone = leadPrimaryPhone;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public void setLeadColor(String leadColor) {
        this.leadColor = leadColor;
    }

    public void setLeadAccountId(String leadAccountId) {
        this.leadAccountId = leadAccountId;
    }

    public void setLeadUserImageURL(String leadUserImageURL) {
        this.leadUserImageURL = leadUserImageURL;
    }

    public void setUserImageThumbURL(String userImageThumbURL) {
        this.userImageThumbURL = userImageThumbURL;
    }

    public void setForSpinner(Boolean forSpinner) {
        isForSpinner = forSpinner;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setContactsPeople(String contactsPeople) {
        this.contactsPeople = contactsPeople;
    }
}
