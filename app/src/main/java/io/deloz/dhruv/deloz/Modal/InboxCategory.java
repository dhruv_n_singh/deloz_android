package io.deloz.dhruv.deloz.Modal;

/**
 * Created by Dhruv on 2/6/18.
 */

public class InboxCategory {

    String name;
    public InboxCategory( String name) {
        this.name = name;
    }

    //Getter

    public String getName() {
        return name;
    }

    //Setter
    public void setName(String name) {
        this.name = name;
    }

}
