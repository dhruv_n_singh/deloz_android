package io.deloz.dhruv.deloz.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;
import io.deloz.dhruv.deloz.Modal.IntroductionModal;
import io.deloz.dhruv.deloz.R;

/**
 * Created by Dhruv on 12/26/17.
 */

public class IntroductionAdapter extends RecyclerView.Adapter<IntroductionAdapter.ViewHolder> {
    private static final String TAG = "IntroductionAdapter";
    private Context mContext;
    private List<IntroductionModal> introListData;

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView
                textViewIntroDescription;
        private ImageView
                imageViewIntroduction;


        public ViewHolder(View v) {
            super(v);
            imageViewIntroduction = (ImageView) v.findViewById(R.id.imageViewIntroduction);
            textViewIntroDescription = (TextView) v.findViewById(R.id.textViewIntroDescription);
        }

    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param introListData ArrayList<LeadModal> containing the data to populate views to be used by RecyclerView.
     */
    public IntroductionAdapter( Context mContext
            , List<IntroductionModal> introListData) {
        this.introListData = introListData;
        this.mContext = mContext;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public IntroductionAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_item_introduction_view, viewGroup, false);

        return new IntroductionAdapter.ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(IntroductionAdapter.ViewHolder viewHolder, final int position) {
           IntroductionModal modal = introListData.get(position);
           viewHolder.textViewIntroDescription.setText(modal.getStrDescription());

        Picasso
                .with(mContext)
                .load(modal.getImage())
                .into(viewHolder.imageViewIntroduction);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return introListData.size();
    }
}
