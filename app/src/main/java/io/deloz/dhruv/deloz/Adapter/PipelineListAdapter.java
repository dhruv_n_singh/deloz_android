package io.deloz.dhruv.deloz.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import io.deloz.dhruv.deloz.Interfaces.RecycleViewPipelineListClickListener;
import io.deloz.dhruv.deloz.Modal.PipelineList;
import io.deloz.dhruv.deloz.R;

/**
 * Created by Dhruv on 1/29/18.
 */

public class PipelineListAdapter  extends RecyclerView.Adapter<PipelineListAdapter.ViewHolder> {

    private static final String TAG = "PipelineListAdapter";
    private Context mContext;
    private List<PipelineList> pipelineListData;
    private RecycleViewPipelineListClickListener mListener;

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView
                textViewPipelineListName;

        public ViewHolder(View v) {
            super(v);
            textViewPipelineListName = (TextView) v.findViewById(R.id.textViewPipelineListName);
        }

    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param pipelineListData ArrayList<PipelineList> containing the data to populate views to be used by RecyclerView.
     */
    public PipelineListAdapter( Context mContext
            , List<PipelineList> pipelineListData,RecycleViewPipelineListClickListener mListener) {
        this.pipelineListData = pipelineListData;
        this.mContext = mContext;
        this.mListener = mListener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PipelineListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_item_pipeline_list, viewGroup, false);

        return new PipelineListAdapter.ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(PipelineListAdapter.ViewHolder viewHolder, final int position) {
            PipelineList modal = pipelineListData.get(position);
            viewHolder.textViewPipelineListName.setText(modal.getPipeline_name());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onClickView(position);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return pipelineListData.size();
    }
}
