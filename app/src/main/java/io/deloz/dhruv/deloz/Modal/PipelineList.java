package io.deloz.dhruv.deloz.Modal;

/**
 * Created by Dhruv on 1/29/18.
 */

public class PipelineList {

    String account_id;
    String _key;
    String created_at;
    String description;
    String updated_at;
    String created_by;
    String pipeline_name;
    String project_id;
    String colour;
    String updated_by;

    //Getter
    public String getAccount_id() {
        return account_id;
    }

    public String get_key() {
        return _key;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getDescription() {
        return description;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCreated_by() {
        return created_by;
    }

    public String getPipeline_name() {
        return pipeline_name;
    }

    public String getProject_id() {
        return project_id;
    }

    public String getColour() {
        return colour;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    //Setter


    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public void set_key(String _key) {
        this._key = _key;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public void setPipeline_name(String pipeline_name) {
        this.pipeline_name = pipeline_name;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }
}
