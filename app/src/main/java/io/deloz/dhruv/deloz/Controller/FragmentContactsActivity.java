package io.deloz.dhruv.deloz.Controller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import io.deloz.dhruv.deloz.Adapter.ContactAdapterSectionRecycler;
import io.deloz.dhruv.deloz.Adapter.ContactSectionHeader;
import io.deloz.dhruv.deloz.Helper.API;
import io.deloz.dhruv.deloz.Helper.HttpUtility;
import io.deloz.dhruv.deloz.Helper.Utility;
import io.deloz.dhruv.deloz.Modal.Contact;
import io.deloz.dhruv.deloz.R;
import okhttp3.Response;

/**
 * Created by Dhruv on 12/19/17.
 */

public class FragmentContactsActivity  extends Fragment {


    private FragmentDashboardActivity.OnFragmentInteractionListener mListener;
    public FragmentContactsActivity() {}
    //UI Components
    Toolbar mToolbar;
    Spinner mSpinner;
    RecyclerView recyclerViewContact;

    //Variables
    ContactAdapterSectionRecycler adapterRecycler;
    List<ContactSectionHeader> sectionHeaders;
    HashMap<Character,Integer> alphabetIndexMap;
    private static final String TAG = AccountValidationActivity.class.getName();
    private ProgressDialog mProgressDialog;
    private List<Contact> contactList;
    private boolean isPeople = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_contacts, container, false);
        if (mListener != null) {
            mListener.onFragmentInteraction("Contacts");
        }
        setRetainInstance(true);
        alphabetIndexMap = new HashMap<>();
        contactList = new ArrayList<>();
        //initialize RecyclerView
        recyclerViewContact = (RecyclerView) view.findViewById(R.id.recycleViewContact);
       final String project_id = PreferenceManager.getDefaultSharedPreferences(getContext())
                .getString(API.SharedPreferenceKeys.project_id, "");
       final String accountKey = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.account_key, "");
        //setLayout Manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewContact.setLayoutManager(linearLayoutManager);
        recyclerViewContact.setHasFixedSize(true);
        adapterRecycler.isPeople = true;
        if (Utility.isOnline(getContext())){
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            String url = API.KBaseUrlDeloz + "/" + accountKey + "/" + project_id +  API.kContactPeople + "?sort=name&order=asc";
            getContactsData(url);
        }else {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_no_internet_connection)
                    .setMessage(getString(R.string.error_internet_connection_message))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
        }
        final Button btnPeople = (Button) view.findViewById(R.id.btnQuickInsight);
        final Button btnOrganization = (Button) view.findViewById(R.id.btnOrganizationContact);

        btnPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapterRecycler.isPeople = true;
                isPeople = true;
                sectionHeaders = new ArrayList<>();
                adapterRecycler.notifyDataChanged(sectionHeaders);
                btnOrganization.setTextColor(getResources().getColor(R.color.colorDeselected));
                btnPeople.setTextColor(getResources().getColor(R.color.colorGettingStartedButton));
                String url = API.KBaseUrlDeloz + "/" + accountKey + "/" + project_id +  API.kContactPeople + "?sort=name&order=asc";
                if (Utility.isOnline(getContext())){
                    mProgressDialog = new ProgressDialog(getContext());
                    mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();
                    getContactsData(url);
                }else {
                    new AlertDialog.Builder(getContext())
                            .setTitle(R.string.error_no_internet_connection)
                            .setMessage(getString(R.string.error_internet_connection_message))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();
                }
            }
        });

        btnOrganization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapterRecycler.isPeople = false;
                isPeople = false;
                sectionHeaders = new ArrayList<>();
                adapterRecycler.notifyDataChanged(sectionHeaders);
                btnPeople.setTextColor(getResources().getColor(R.color.colorDeselected));
                btnOrganization.setTextColor(getResources().getColor(R.color.colorGettingStartedButton));
                String url = API.KBaseUrlDeloz + "/" + accountKey + "/" + project_id +  API.kContactOrganizations + "?sort=name&order=asc";
                if (Utility.isOnline(getContext())){
                    mProgressDialog = new ProgressDialog(getContext());
                    mProgressDialog.setMessage(getString(R.string.indicator_please_wait));
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();
                    getContactsData(url);
                }else {
                    new AlertDialog.Builder(getContext())
                            .setTitle(R.string.error_no_internet_connection)
                            .setMessage(getString(R.string.error_internet_connection_message))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();
                }
            }
        });
        setDropDownToolBar();
        return view;
    }

    /**
     * This function sort ArrayList<Contact> alphabetically
     * @param contactArrayList
     */
    public void sortArrayList(List<Contact> contactArrayList){
        Collections.sort(contactArrayList, new Comparator<Contact>() {
            @Override
            public int compare(Contact item, Contact t1) {
                String s1 = item.getFirstName();
                String s2 = t1.getFirstName();
                return s1.compareToIgnoreCase(s2);
            }

        });
    }

    public void getContactsData(final String url){

        Log.d(TAG + " url: ",url);
        String deviceId = Utility.getDeviceId(getContext());
        String auth_code = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(API.SharedPreferenceKeys.auth_code, "");
        try {
            JSONObject headerData = new JSONObject();
            headerData.put(API.SharedPreferenceKeys.device_key,deviceId);
            headerData.put(API.SharedPreferenceKeys.auth_code,auth_code);
            HttpUtility.getData(url,HttpUtility.getHeaders(headerData),getContext(),new HttpUtility.HttpCallback(){

                @Override
                public void onFailure(Response response, IOException e) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("")
                                    .setMessage(R.string.error_unable_to_get_response)
                                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            getContactsData(url);
                                        }
                                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert).show();

                        }
                    });

                }

                @Override
                public void onSuccess(Response response) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }

                    final String result;
                    try {
                        result = response.body().string();
                        if (response.isSuccessful()){
                            if (Utility.isRedirectFound(result)){
                                Utility.sessionLogout(result,getActivity(),getActivity());
                            }else {
                                try{
                                    JSONObject jsonResponse = new JSONObject(result);
                                    if (jsonResponse.has("payload")){
                                        Log.d(TAG + " payload: ",jsonResponse.toString());
                                            JSONObject jsonPayload = jsonResponse.getJSONObject("payload");
                                            if (jsonPayload.has("list")){
                                                final JSONArray jsonList = jsonPayload.getJSONArray("list");
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        setContactData(jsonList);
                                                    }
                                                });
                                            }
                                    }
                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }

                        }else {

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            });
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void setContactData(JSONArray jsonList) {
        contactList = new ArrayList<>();
        for (int i = 0; i < jsonList.length(); i++) {
            try {
                JSONObject jsonContact = jsonList.getJSONObject(i);
                Contact modal = new Contact();
                if (jsonContact.has("status")){
                    modal.setStatus(jsonContact.getInt("status"));
                }

                if (jsonContact.has("colour")){
                   modal.setColor(jsonContact.getString("colour"));
                }

                if (jsonContact.has("project_id")){
                    modal.setProjectId(jsonContact.getString("project_id"));
                }

                if (isPeople){
                    if (jsonContact.has("name")){
                        JSONObject jsonName = jsonContact.getJSONObject("name");
                        if (jsonName.has("FirstName")){
                            modal.setFirstName(jsonName.getString("FirstName"));
                        }
                        if (jsonName.has("LastName")){
                            modal.setLastName(jsonName.getString("LastName"));
                        }
                        if (jsonName.has("MiddleName")){
                            modal.setMiddleName(jsonName.getString("MiddleName"));
                        }
                    }
                }else{
                    if (jsonContact.has("name")){
                        modal.setOrganizationName(jsonContact.getString("name"));
                    }
                }

                if (jsonContact.has("_key")){
                    modal.setKey(jsonContact.getString("_key"));
                }

                if (jsonContact.has("_rev")){
                    modal.setRev(jsonContact.getString("_rev"));
                }

                if (jsonContact.has("profile_image")){
                    modal.setProfileImage(jsonContact.getString("profile_image"));
                }

                if (jsonContact.has("created_by")){
                    modal.setCreatedBy(jsonContact.getString("created_by"));
                }

                if (jsonContact.has("email")){
                    if (isPeople){
                        JSONObject jsonObjectEmail = jsonContact.getJSONObject("email");
                        if (jsonObjectEmail.has("primaryemail")){
                            modal.setPrimaryEmail(jsonObjectEmail.getString("primaryemail"));
                        }
                    }else {
                        modal.setOrganizationEmail(jsonContact.getString("email"));
                    }
                }
                if (jsonContact.has("phone")){
                    JSONObject jsonObjectPhone = jsonContact.getJSONObject("phone");
                    if (jsonObjectPhone.has("primary")){
                      modal.setPrimaryPhone(jsonObjectPhone.getString("primary"));
                    }
                    if (jsonObjectPhone.has("work")){
                        modal.setWorkPhone(jsonObjectPhone.getString("work"));
                    }
                }

                if (jsonContact.has("_id")){
                    modal.setId(jsonContact.getString("_id"));
                }

                if (jsonContact.has("created_at")){
                    modal.setCreatedAt(jsonContact.getString("created_at"));
                }

                if (jsonContact.has("account_id")){
                    modal.setAccountId(jsonContact.getString("account_id"));
                }
                contactList.add(modal);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (contactList.size() > 0){
            arrangeContactsAlphabetically(contactList);
        }
    }

    public void arrangeContactsAlphabetically(List<Contact> contactArrayList){
        sectionHeaders = new ArrayList<>();
        //Add 26 buckets
        int current_index = 0;
        char letter = 'A';
        for (; letter <= 'Z'; letter++) {
            /**
             ** Creating Filter Logic
             **/
            Filter<Contact,String> filter = new Filter<Contact,String>() {
                public boolean isMatched(Contact object, String text) {
                    if (isPeople){
                        if (object.getFirstName() != null && object.getFirstName() != "null"){
                            return object.getFirstName().toUpperCase().startsWith(String.valueOf(text));
                        }else {
                            return false;
                        }
                    }else{
                        if (object.getOrganizationName() != null && object.getOrganizationName() != "null"){
                            return (object.getOrganizationName().toUpperCase().startsWith(String.valueOf(text)));
                        }else {
                            return false;
                        }
                    }
                }
            };
            /**
             * Pass filter to filter the original list
             */
            List<Contact> sortContactList = new FilterList().filterList(contactArrayList, filter, String.format("%c",letter));
            if (sortContactList.size() > 0){
                sectionHeaders.add(new ContactSectionHeader(sortContactList, String.format("%c",letter), current_index));
            }
            current_index++;
        }
        List<Contact> contactListWithoutAlphabet = new ArrayList<>();
        for (int i = 0; i < contactArrayList.size(); i++){
            Contact modal = contactArrayList.get(i);
            if (modal.getFirstName() != null && modal.getFirstName() != "null"){
                if (modal.getFirstName().length() > 0){
                    String startsWith = String.format("%c",modal.getFirstName().charAt(0));
                    if (!Utility.isAlpha(startsWith)){
                        contactListWithoutAlphabet.add(modal);
                    }
                }
            }
        }
        if (contactListWithoutAlphabet.size() > 0){
            sectionHeaders.add(new ContactSectionHeader(contactListWithoutAlphabet, "#", current_index));
        }
        adapterRecycler = new ContactAdapterSectionRecycler(getContext(), sectionHeaders);
        if (contactArrayList.size() > 0){
            recyclerViewContact.setAdapter(adapterRecycler);
            adapterRecycler.notifyDataChanged(sectionHeaders);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentDashboardActivity.OnFragmentInteractionListener) {
            mListener = (FragmentDashboardActivity.OnFragmentInteractionListener) context;
        } else {
            // NOTE: This is the part that usually gives you the error
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // NOTE : We changed the Uri to String.
        void onFragmentInteraction(String title);
        void onFragmentSwitch(String selectedFragment);
    }

    /**
     * This function is used to match the object
     * @param <T>
     * @param <E>
     */
    interface Filter<T,E> {
        public boolean isMatched(T object, E text);
    }

    /**
     * This function is used to filter the ArrayList based on Starts with chracter
     * @param <E>
     */

    public class FilterList<E> {
        public  <T> List filterList(List<T> originalList, Filter filter, E text) {
            List<T> filterList = new ArrayList<T>();
            for (T object : originalList) {
                if (filter.isMatched(object, text)) {
                    filterList.add(object);
                } else {
                    continue;
                }
            }
            return filterList;
        }
    }


    public void setDropDownToolBar(){
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        //you can also set the style with the constructor
        mSpinner = new Spinner(getActivity());
        mSpinner.getBackground().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);

        mSpinner.setGravity(Gravity.LEFT);
       final String[] frags = new String[]{
                "Contacts",
                "Leads"
        };
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,frags);
        mSpinner.setAdapter(arrayAdapter);
        mSpinner.setSelection(0, true);
        View v = mSpinner.getSelectedView();
        ((TextView)v).setTextSize(Utility.SPINNER_TEXT_SIZE);
        ((TextView)v).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
        ((TextView)v).setTextColor(getResources().getColor(R.color.colorWhite));
        mToolbar.addView(mSpinner);

        //Set the listener for when each option is clicked.
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                //Change the selected item's text color
                ((TextView) view).setTextColor(getResources().getColor(R.color.colorWhite));
                ((TextView)view).setTextSize(Utility.SPINNER_TEXT_SIZE);
                if (mListener != null) {
                    mListener.onFragmentSwitch(frags[position]);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mToolbar != null && mSpinner != null) {
            mToolbar.removeView(mSpinner);
        }
    }
}


