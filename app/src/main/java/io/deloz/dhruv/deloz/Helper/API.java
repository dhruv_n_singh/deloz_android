package io.deloz.dhruv.deloz.Helper;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by Dhruv on 12/14/17.
 */

public class API {

    private static final String TAG = API.class.getName();
    public static final String kLocalBaseURL = "http://192.168.1.17:5000";
    public static final String kBaseUrl = "http://138.68.245.229:5000";
    public static final String KBaseUrlDeloz = "http://5.9.84.39:8000";
    public static final String kAccountValidation = "/accountnameverify";
    public static final String kGenerateMagicLink = "/generatemagiclink";
    public static final String kDefaultLogin = "/applogin";
    public static final String  kLogout = "/logout";
    public static final String kGetLeads = "/leads";
    public static final String kGetProjectList = "/projects/list";
    public static final String kGConnect = "/appgconnect";
    public static final String kVerifyQrCode = "/verifyqrcode";
    public static final String kForgotPassword = "/forgot-password";
    public static final String kContactPeople = "/contact/people";
    public static final String  kContactOrganizations = "/contact/organisation";
    public static final String kDeleteLead = "/delete";
    public static final String kConvertLeadToContact = "/lead/people";
    public static final String kDeleteOrArchiveLeads = "/edit";
    public static final String kDashboardInsights = "/v1/insights/deals/quickinsight";
    public static final String kBarChartDetails = "/v1/insights/deals/wondeals";
    public static final String kGetPipeLineList = "/pipeline/list";
    public static final String kStageList = "/stage/list";
    public static final String  kGetPipeLineDeals = "/deals";
    public static final String kDealStageUpdate = "/update";
    public static final String kDeal = "/deal";
    public static final String kGetMails = "/inbox";
    public static final String  kGetFavourites = "/inbox/starred";
    public static final String kGetAssignedToMe = "/inbox/assigned";
    public static final String  kGetSendMails = "/inbox/sentitems";
    public static final String kGetDrafts = "/inbox/drafts";
    public static final String kGetTrash = "/inbox/trash";
    public static final String  kGetSharedInbox = "/inbox/sharedinboxes";
    public static final String kGetSharedInboxMessages = "/inbox/shared";

    //GooglePlus client ID
    public static String outh_client_id = "851102419273-3ect0nu8aijpsn8vls5ehht0u1791c59.apps.googleusercontent.com";

    public class SharedPreferenceKeys {
        public static final String account_key = "account_key";
        public static final String auth_code = "Authorization-Key";
        public static final String project_id = "project_id";
        public static final String device_key = "Device-Key";
    }

    public static boolean isLogIn(@NonNull Context context) {
        Boolean isLoginFound = false;
        String auth_code = PreferenceManager.getDefaultSharedPreferences(context).getString(SharedPreferenceKeys.auth_code, "");
        Log.d(TAG + " auth_code: ",auth_code);
        if (!auth_code.isEmpty()){
          return true;
        }
        return isLoginFound;
   }


}


