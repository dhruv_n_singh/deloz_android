package io.deloz.dhruv.deloz.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.deloz.dhruv.deloz.Controller.AccountValidationActivity;
import io.deloz.dhruv.deloz.R;

/**
 * Created by Dhruv on 12/15/17.
 */

public class Utility {

    public class RequestResultConst {
       public static final int PICK_PIPELINE_REQUEST = 1;
       public static final int PICK_INBOX_REQUEST = 2;
    }

    private static final String TAG = Utility.class.getName();
    public static final int SPINNER_TEXT_SIZE = 19;

    public static boolean isOnline(@NonNull Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        // if running on emulator return true always.
        return android.os.Build.MODEL.equals("google_sdk");
    }

    public static  String getDeviceId(@NonNull Context context){
        return Secure.getString(
                context.getContentResolver(),
                Secure.ANDROID_ID);
    }

    public static void sessionLogout(String response,@NonNull Context context, Activity activity) {
        if (response != null){
            try {
                JSONObject json = new JSONObject(response);
                if (json.has("redirect_url")){
                    Toast.makeText(activity, R.string.error_session_expired, Toast.LENGTH_SHORT).show();
                    PreferenceManager.getDefaultSharedPreferences(context).edit().putString(API.SharedPreferenceKeys.account_key, "").apply();
                    PreferenceManager.getDefaultSharedPreferences(context).edit().putString(API.SharedPreferenceKeys.auth_code, "").apply();
                    context.startActivity(new Intent(context, AccountValidationActivity.class));
                    activity.finish();;
                }

            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    public static boolean isRedirectFound(String response){
        if (response != null){
            try {
                JSONObject json = new JSONObject(response);
                if (json.has("redirect_url")){
                    return true;
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
        return  false;
    }


    public static final String EMAIL_PATTERN =
            "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
    public static Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    public static Matcher matcher;

    public static boolean validateEmail(String email) {
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static int daysBtwDates(Date inputDate){
        int diffInDays = 0;
        Date currentDate = new Date();
        //  diffInDays = (int) ((currentDate.getTime() - date.getTime()) / (1000 * 60 * 60 * 24));
       // diffInDays =(int) TimeUnit.MILLISECONDS.toDays(currentDate.getTime() - inputDate.getTime());
        long startDay = currentDate.getTime() / 1000 / 60 / 60 / 24;
        long endDay = inputDate.getTime() / 1000 / 60 / 60 / 24;

        // Find the difference, duh
        diffInDays = (int)(startDay - endDay  );
        return diffInDays;
    }

    public static String getDayOfWeek(Date date){
            DateFormat dateFormatNew = new SimpleDateFormat("EEEE");
            //to convert Date to String, use format method of SimpleDateFormat class.
            String strDate = dateFormatNew.format(date);
            return strDate;
    }

    public static String getFormattedDate(Date date, String stringDateFormat){
        DateFormat dateFormatNew = new SimpleDateFormat(stringDateFormat);
        String strDate = dateFormatNew.format(date);
        return strDate;
    }


    public static String getFormattedDate(String dateStr){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = format.parse(dateStr);
            DateFormat dateFormatNew = new SimpleDateFormat("dd-MM-yyyy");
            //to convert Date to String, use format method of SimpleDateFormat class.
            String strDate = dateFormatNew.format(date);
            return strDate;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "No due date!";
    }

    public static String getDateString(String dateStr){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = format.parse(dateStr);
            DateFormat dateFormatNew = new SimpleDateFormat("dd-MM-yyyy");
            //to convert Date to String, use format method of SimpleDateFormat class.
            String strDate = dateFormatNew.format(date);
            return strDate;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFormattedTime(String dateStr){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(dateStr);
            DateFormat dateFormatNew = new SimpleDateFormat("h:mm a");
            //to convert Date to String, use format method of SimpleDateFormat class.
            String strDate = dateFormatNew.format(date);
            return strDate.toUpperCase();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date getDate(String dateStr){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(dateStr);
            return date;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDaysAgo(String date)  {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d1 = null;
            Date d2 = null;
            try {
                d1 = format.parse(date);
                d2 = format.parse(getToday());
                long diff = d2.getTime() - d1.getTime();
                long seconds = diff / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                long days = hours / 24;
                long months = days / 30;
                long years = months /12;
              //  Log.d(TAG," seconds: " + seconds + " minutes: " + minutes
                 //       + " hours: " + hours + " days: " + days + " months: " + months + " years: " + years);

                if ((seconds < 60) && (seconds == 1)){
                    return seconds + " second ago";
                }else if ((seconds < 60) && (seconds >= 1)){
                    return seconds + " seconds ago";
                }

                if ((minutes < 60) && (minutes == 1)){
                    return minutes + " minute ago";
                }else if ((minutes < 60) && (minutes >= 1)){
                    return minutes + " minutes ago";
                }

                if ((hours < 24) && (hours == 1)){
                    return hours + " hour ago";
                }else if ((hours < 24) && (hours >= 1)){
                    return hours + " hours ago";
                }


                if ((days < 30) && (days == 1)){
                    return days + " day ago";
                }else if ((days < 30) && (days >= 1)){
                    return days + " days ago";
                }

                if ((months < 12) && (months == 1)){
                    return months + " month ago";
                }else if ((months < 12) && (months >= 1)){
                    return months + " months ago";
                }

                if ((years < Long.MAX_VALUE) && (years == 1)){
                    return years + " year ago";
                }else if ((years < Long.MAX_VALUE) && (years >= 1)){
                    return years + " years ago";
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        return "";
    }

    public static String getToday(){
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);
    }

    public static void hideKeyboardFrom(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    private int getRandomMaterialColor(String typeColor,Activity av) {
        int returnColor = Color.GRAY;
        int arrayId = av.getResources().getIdentifier("mdcolor_" + typeColor, "array", av.getPackageName());

        if (arrayId != 0) {
            TypedArray colors = av.getResources().obtainTypedArray(arrayId);
            int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.GRAY);
            colors.recycle();
        }
        return returnColor;
    }

    public static boolean isAlpha(String name) {
        return name.matches("[a-zA-Z]+");
    }

    public static List<String> getLast30Days(){
        List<String> arrLast30Days = new ArrayList<>();
        Date date = new Date();
        Log.d(TAG, "Todays date: "+date.toString());
        int i=30;
        while(i>=1){
            Date newDate = subtractDays(date, i);
           // Log.d(TAG,"Java Date after subtracting "+i+" days: "+newDate.toString());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            arrLast30Days.add(dateFormat.format(newDate));
            i = i - 1;
        }

        return arrLast30Days;
    }

    /**
     * add days to date in java
     * @param date
     * @param days
     * @return
     */
    public static Date addDays(Date date, int days) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);

        return cal.getTime();
    }

    /**
     * subtract days to date in java
     * @param date
     * @param days
     * @return
     */
    public static Date subtractDays(Date date, int days) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, -days);

        return cal.getTime();
    }


}
